angular.module('hm', [
    'ui.router',
    'ngTouch',
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'hm.config',
    'hm.utils',
    'hm.signup',
    'hm.camera',
    'hm.login',
    'hm.home',
    'hm.roomDetails'
])

    .run(function ($state, $rootScope, CookiesService) {
        console.log("app run");
        $rootScope.$state = $state;
        //if (CookiesService.get('CurrentUser') != undefined) {
        //    $rootScope.currentUser = JSON.parse(CookiesService.get('CurrentUser'));
        //}
        if (window.localStorage.getItem('CurrentUser') != undefined) {
            $rootScope.currentUser = JSON.parse(window.localStorage.getItem('CurrentUser'));
        }
    })

    .service('CookiesService', ['$cookieStore', function ($cookieStore) {
        console.log('Service');
        this.set = function (key, value) {
            $cookieStore.put(key, value);
        }
        this.get = function (key) {
            return $cookieStore.get(key);
        }
        this.remove = function (key) {
            $cookieStore.remove(key);
        }
    }])

    .directive("year", function() {
        return {
            restrict: "A",
         
            require: "ngModel",
         
            link: function(scope, element, attributes, ngModel) {
                ngModel.$validators.year = function(modelValue) {  
                    return (modelValue < 1900 || modelValue > 2100? false: true);
                }
            }
        };
    })

    .config(function ($stateProvider, $urlRouterProvider) {
        console.log("app config");

        //$urlRouterProvider.when("", "/login");
        $urlRouterProvider.when("", "/camera");
        $stateProvider
            //.state('home', {
            //    url: "/home",
            //    template: "templates/home.html",
            //    controller: 'AppCtrl'
            //})

        .state('home', {
            url: "/home",
            views: {
                'menuContent': {
                    templateUrl: "templates/home.html",
                    controller: "AppCtrl"
                }
            }
        })

        .state('signup', {
            url: "/signup",
            views: {
                'menuContent': {
                    templateUrl: "templates/signup.html",
                    controller: "SignupCtrl"
                }
            }
        })
        .state('camera', {
            url: "/camera",
            views: {
                'menuContent': {
                    templateUrl: "templates/camera.html",
                    controller: "CameraCtrl"
                }
            }
        })
         .state('login', {
             url: "/login",
             views: {
                 'menuContent': {
                     templateUrl: "templates/login.html",
                     controller: "LoginCtrl"
                 }
             }
         })
        .state('roomDetails', {
            url: "/roomDetails",
            views: {
                'menuContent': {
                    templateUrl: "templates/roomDetails.html",
                    controller: "RoomDetailsCtrl"
                }
            }
        })
    })

    .controller('AppCtrl', function ($scope, $rootScope, $cacheFactory, $state, $timeout) {
        console.log("app controller");
        $rootScope.showHeader = true;
        $rootScope.showMessageflag = false;

        //Create Cache object
        if ($rootScope.cache == undefined) {
            $rootScope.cache = $cacheFactory('hmCache');
        }

        $rootScope.showMessage = function () {
            $rootScope.showMessageflag = true;
            $timeout(function () { $rootScope.showMessageflag = false; }, 5000);
        }
        //$state.transitionTo('camera');

        /*
        if ($rootScope.currentUser == undefined) {
            event.preventDefault();
            $state.transitionTo('login');
            return;
        }*/

    });

﻿angular.module('hm.camera', [])
    .factory('CameraService', ['$q', function ($q) {
        return {
            getPicture: function (options) {
                debugger;
                var q = $q.defer();

                navigator.camera.getPicture(function (result) {
                    // Do any magic you need
                    q.resolve(result);
                }, function (err) {
                    q.reject(err);
                }, options);

                return q.promise;
            }
        }
    }])
    .factory('FileUploadService', function () {
        'use strict';

        function getFileUploadOptions(fileURI) {
            var options = new FileUploadOptions();
            options.fileName = fileURI.substr(fileURI.lastIndexOf('/') + 1);
            options.mimeType = "multipart/form-data";
            options.chunkedMode = false;
            options.headers = {
                Connection: "close"
            }
            return options;
        }
        return {
            upload: function (files, onSuccess, onError) {
                var ft = new FileTransfer();
                for (var i = 0; i < files.length; i++) {
                    var file = files[i].Src;
                    navigator.notification.alert(
                                 file.toString() + " upload started.",  // message
                                 function () { },         // callback
                                 'Exception',            // title
                                 'Done'                  // buttonName
                             );
                    try {
                        //ft.upload(file, encodeURI("http://104.155.205.157/HMDocs/default.aspx"), onSuccess, onError, getFileUploadOptions(file));
                        ft.upload(file, encodeURI("http://localhost:5495/HMRest/hm/Document/postdocument"), onSuccess, onError, getFileUploadOptions(file));
                    } catch (e) {
                        navigator.notification.alert(
                                  e.toString(),  // message
                                  function () { },         // callback
                                  'Exception',            // title
                                  'Done'                  // buttonName
                              );
                    }
                }
            }
        };
    })
    .controller('CameraCtrl', function ($scope, CameraService, FileUploadService) {
        console.log('Camera Controller');
        $scope.images = [];
        $scope.imagesToUpload = [];
        $scope.uploadStatus = "none";
        $scope.getLibraryPhoto = function () {
            debugger;
            console.log('getLibraryPhoto()');
            var options = {
                quality: 50,
                destinationType: Camera.DestinationType.FILE_URL,
                sourceType: Camera.PictureSourceType.Camera,
                //allowEdit: true,
                //encodingType: Camera.EncodingType.JPEG,
                targetWidth: 150,
                targetHeight: 150
            };
            getPhoto(options);
        }

        $scope.getCameraPhoto = function () {
            debugger;
            console.log('getPhoto()');

            var options = {
                quality: 50,
                destinationType: Camera.DestinationType.NATIVE_URI,
                sourceType: Camera.PictureSourceType.CAMERA,
                //allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 50,
                targetHeight: 50,
                //saveToPhotoAlbum: true
            };


            getPhoto(options);
        }

        $scope.getSavedPhoto = function () {
            debugger;
            console.log('getSavedPhoto()');

            var options = {
                quality: 50,
                destinationType: Camera.DestinationType.FILE_URL,
                sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
                //allowEdit: true,
                //encodingType: Camera.EncodingType.JPEG,
                targetWidth: 150,
                targetHeight: 150
            };
            getPhoto(cameraOptions);
        }

        $scope.uploadFiles = function () {
            navigator.notification.alert(
                                   "uploadFiles()",  // message
                                   function () { },         // callback
                                   'Game Over',            // title
                                   'Done'                  // buttonName
                               );
            FileUploadService.upload($scope.images, function onSuccess(r) {
                $scope.uploadStatus = "Success";
                navigator.notification.alert(
                                   "File uploaded",  // message
                                   function () { },         // callback
                                   'Game Over',            // title
                                   'Done'                  // buttonName
                               );
                console.log("Code = " + r.responseCode);
                console.log("Response = " + r.response);
                console.log("Sent = " + r.bytesSent);
            }, function onFailure(error) {
                $scope.uploadStatus = "An error has occurred: Code = " + error.code + "upload error source " + error.source + "upload error target " + error.target;
                navigator.notification.alert(
                                   $scope.uploadStatus,  // message
                                   function () { },         // callback
                                   'Game Over',            // title
                                   'Done'                  // buttonName
                               );
                console.log("upload error source " + error.source);
                console.log("upload error target " + error.target);
            });
        }

        function getPhoto(options) {
            CameraService.getPicture().then(function (imageURI) {
                var image = new Object();
                image.Src = imageURI;
                $scope.images.push(image);//"data:image/jpeg;base64," + imageURI;
                $scope.$apply();
                movePic(image.toURI())
                //$scope.imageSrc = imageURI;
                //navigator.notification.alert('photo taken');
            }, function (err) {
                navigator.notification.alert(
                                   err.toString(),  // message
                                   function () { },         // callback
                                   'Game Over',            // title
                                   'Done'                  // buttonName
                               );
            });
        }

        function movePic(file) {
            try {
                navigator.notification.alert(
                                "resolveLocalFileSystemURI()",  // message
                                 function () { },         // callback
                                 'Game Over',            // title
                                 'Done'                  // buttonName
                             );
                window.resolveLocalFileSystemURI(file.toString(), resolveOnSuccess, resOnError);
            } catch (e) {
                navigator.notification.alert(
                                 "movePic " + e.toString(),  // message
                                  function () { },         // callback
                                  'Game Over',            // title
                                  'Done'                  // buttonName
                              );
            }

        }

        //Callback function when the file system uri has been resolved
        function resolveOnSuccess(entry) {
            var d = new Date();
            var n = d.getTime();
            //new file name
            var newFileName = n + ".jpg";
            var myFolderApp = "HMDocs";

            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
                navigator.notification.alert(
                                "requestFileSystem()",  // message
                                 function () { },         // callback
                                 'Game Over',            // title
                                 'Done'                  // buttonName
                             );
                //The folder is created if doesn't exist
                fileSys.root.getDirectory(myFolderApp,
                                { create: true, exclusive: false },
                                function (directory) {
                                    navigator.notification.alert(
                                "fileSys.root.getDirectory",  // message
                                 function () { },         // callback
                                 'Game Over',            // title
                                 'Done'                  // buttonName
                             );
                                    entry.moveTo(directory, newFileName, successMove, resOnError);
                                },
                                resOnError);
            },
            resOnError);
        }

        //Callback function when the file has been moved successfully - inserting the complete path
        function successMove(entry) {
            //I do my insert with "entry.fullPath" as for the path
            $scope.imagesToUpload.push(entry.fullPath);
            navigator.notification.alert(
                                   entry.toString() + " successMove moved succ...",  // message
                                   function () { },         // callback
                                   'Game Over',            // title
                                   'Done'                  // buttonName
                               );
        }

        function resOnError(error) {
            navigator.notification.alert(
                                   error.code + "resOnError" + error.toString(),  // message
                                   function () { },         // callback
                                   'Game Over',            // title
                                   'Done'                  // buttonName
                               );
        }

        // Called when capture operation is finished
        //
        function captureSuccess(mediaFiles) {
            var i, len;
            for (i = 0, len = mediaFiles.length; i < len; i += 1) {
                navigator.notification.alert(mediaFiles[i].fullPath, null, 'Uh oh!');
                $scope.images.push(mediaFiles[i]);
                uploadFile(mediaFiles[i]);
            }
        }

        // Called if something bad happens.
        //
        function captureError(error) {
            var msg = 'An error occurred during capture: ' + error.code;
            navigator.notification.alert(msg, null, 'Uh oh!');
        }

        // A button will call this function
        //
        $scope.captureImage = function () {
            // Launch device camera application,
            // allowing user to capture up to 2 images
            navigator.device.capture.captureImage(captureSuccess, captureError, { limit: 1 });
        }

        // Upload files to server
        function uploadFile(mediaFile) {
            var ft = new FileTransfer(),
                path = mediaFile.fullPath,
                name = mediaFile.name;
            navigator.notification.alert("uploadFile-" + path, null, 'Uh oh!');
            ft.upload(path,
                "http://104.155.205.157/HMDocs",
                function (result) {
                    console.log('Upload success: ' + result.responseCode);
                    console.log(result.bytesSent + ' bytes sent');

                    navigator.notification.alert('Upload success: ' + result.responseCode + result.bytesSent + ' bytes sent', null, 'Uh oh!');
                },
                function (error) {
                    console.log('Error uploading file ' + path + ': ' + error.code);
                    navigator.notification.alert('Error uploading file ' + path + ': ' + error.code, null, 'Uh oh!');
                },
                { fileName: name });
        }
    });

/*
DestinationType:{
    DATA_URL: 0,         // Return base64 encoded string
    FILE_URI: 1,         // Return file uri (content://media/external/images/media/2 for Android)
    NATIVE_URI: 2        // Return native uri (eg. asset-library://... for iOS)
  },
  EncodingType:{
    JPEG: 0,             // Return JPEG encoded image
    PNG: 1               // Return PNG encoded image
  },
  MediaType:{
    PICTURE: 0,          // allow selection of still pictures only. DEFAULT. Will return format specified via DestinationType
    VIDEO: 1,            // allow selection of video only, ONLY RETURNS URL
    ALLMEDIA : 2         // allow selection from all media types
  },
  PictureSourceType:{
    PHOTOLIBRARY : 0,    // Choose image from picture library (same as SAVEDPHOTOALBUM for Android)
    CAMERA : 1,          // Take picture from camera
    SAVEDPHOTOALBUM : 2  // Choose image from picture library (same as PHOTOLIBRARY for Android)
  },
  PopoverArrowDirection:{
      ARROW_UP : 1,        // matches iOS UIPopoverArrowDirection constants to specify arrow location on popover
      ARROW_DOWN : 2,
      ARROW_LEFT : 4,
      ARROW_RIGHT : 8,
      ARROW_ANY : 15
  },
  Direction:{
      BACK: 0,
      FRONT: 1
  }
*/

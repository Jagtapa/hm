﻿angular.module('hm.utils', [])

.factory('$localstorage', ['$window', function ($window) {
    return {
        set: function (key, value) {
            $window.localStorage[key] = value;
        },
        get: function (key, defaultValue) {
            return $window.localStorage[key] || defaultValue;
        },
        setObject: function (key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function (key) {
            return JSON.parse($window.localStorage[key] || '{}');
        }
    }
}])

.factory('util', function () {

    var util = {};

    util.getDate = function (t) {
        var dateRE = /(\d+)-(\d+)-(\d+)T(\d+):(\d+):(\d+)(?:.(\d+))([+\-]\d+):(\d+)/;
        var match = t.match(dateRE);
        var nums = [], item, date;
        if (match) {
            for (var i = 1; i < match.length; i++) {
                nums.push(parseInt(match[i], 10));
            }
            if (nums[7] < 0) {
                nums[8] *= -1;
            }
            return (new Date(nums[0], nums[1] - 1, nums[2]));
        }
        return;
    }

    util.getTime = function (t) {
        var dateRE = /(\d+)-(\d+)-(\d+)T(\d+):(\d+):(\d+)(?:.(\d+))([+\-]\d+):(\d+)/;
        var match = t.match(dateRE);
        var nums = [], item, date;
        if (match) {
            for (var i = 1; i < match.length; i++) {
                nums.push(parseInt(match[i], 10));
            }
            if (nums[7] < 0) {
                nums[8] *= -1;
            }
            //return (new Date(nums[0], nums[1] - 1, nums[2], nums[3] - nums[6], nums[4] - nums[7]));
            return (new Date(nums[0], nums[1] - 1, nums[2], nums[3], nums[4]));
        }
        return;
    }

    util.getCurrnetDate = function () {
        var d = new Date();
        return (new Date(
            d.getFullYear(),
            d.getMonth(),
            d.getDate()
            )
        );
    }

    util.getCurrnetTime = function () {
        var d = new Date();
        return (new Date(
            d.getFullYear(),
            d.getMonth(),
            d.getDate(),
            d.getHours(),
            d.getMinutes()
            )
        );
    }


    util.getDateTime = function (t) {
        var d = new Date(t);
        //d.setSeconds(0);
        //d.setMilliseconds(0);
        return (new Date(
            d.getUTCFullYear(),
            d.getUTCMonth(),
            d.getUTCDate(),
            d.getUTCHours(),
            d.getUTCMinutes()
            )
        );
    }

    return util;
});
;
﻿angular.module('hm.signup', ['ngResource', 'hm.config'])
     //.run(function ($rootScope) {
     //    $rootScope.showFirstNameError = false;
     //    $rootScope.showLastNameError = false;
     //    $rootScope.showEmailError = false;
     //    $rootScope.showPasswordError = false;
     //})

    .factory('Signup', function ($resource, SERVER_PATH, $rootScope) {
        return {
            registerUser: $resource(SERVER_PATH + '/user/PostRegisterUser', {}, {
                'post': { method: 'POST', cache: $rootScope.cache, isArray: false }
            }),
        }
    })

    .controller('SignupCtrl', function ($scope, $state, $stateParams, Signup, $rootScope, CookiesService, $location, $timeout) {
        console.log('Signup Controller');

        $scope.signupUser = { FirstName: '', LastName: '', EmailId: '', Password: '' };

        $scope.signup = function () {
            console.time('signup');
            $scope.showFirstNameError = false;
            $scope.showLastNameError = false;
            $scope.showEmailError = false;
            $scope.showPasswordError = false;

            var NAME_REGEXP = /^[a-zA-Z]*$/;

            if ($scope.signupUser.FirstName == "" || !NAME_REGEXP.test($scope.signupUser.FirstName))
            {
                $scope.showFirstNameError = true;
                return;
            }

            if ($scope.signupUser.LastName == "" || !NAME_REGEXP.test($scope.signupUser.LastName)) {
                $scope.showLastNameError = true;
                return;
            }
            var EMAIL_REGEXP = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;

            if ($scope.signupUser.EmailId == "" || !EMAIL_REGEXP.test($scope.signupUser.EmailId)) {
                $scope.showEmailError = true;
                return;
            }
            if ($scope.signupUser.Password == "") {
                $scope.showPasswordError = true;
                return;
            }
            $scope.user = Signup.registerUser.post($scope.signupUser);

            $scope.user.$promise.then(function (result) {
                if (result != undefined) {
                    console.timeEnd('signup');
                    $timeout(function () { $state.go('login'); });
                }                
            })            
        }

    });

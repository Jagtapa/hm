﻿angular.module('hm.login', ['ngResource', 'hm.config'])
    .factory('User', function ($resource, SERVER_PATH, $rootScope) {
        return {
            login: $resource(SERVER_PATH + '/user/authenticate', {}, {
                'post': { method: 'POST', cache: $rootScope.cache, isArray: false }
            }),
            forgotPassword: $resource(SERVER_PATH + '/user/forgotpassword', {}, {
                'post': { method: 'POST', cache: $rootScope.cache, isArray: false }
            }),
        }
    })

    .controller('LoginCtrl', function ($scope, $state, $stateParams, User, $rootScope, CookiesService, $location, $timeout) {
        console.log('Login Controller');

        $scope.loginUser = { EmailId: '', Password: '' };

        $scope.login = function () {
            console.time('login');
            $scope.user = User.login.post($scope.loginUser);

            $scope.user.$promise.then(function (result) {
                if (result.FirstName != null) {
                    //window.localStorage.setItem('CurrentUser', JSON.stringify(result));
                    $rootScope.currentUser = result;
                    console.timeEnd('login');
                    $timeout(function () { $state.go('home'); });
                }

                

            })
        }

        $scope.forgotPassword = function () {
            console.time('forgotPassword');
            $scope.result = User.forgotPassword.post($scope.loginUser);

            //$scope.result.$promise.then(function (result) {
            //    if (result.FirstName != null) {

            //        console.timeEnd('forgotPassword');
            //    }
            //})
        }
    });

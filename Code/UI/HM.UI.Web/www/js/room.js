﻿angular.module('hm.room', ['ngResource', 'hm.config'])
    .factory('RM', function ($resource, SERVER_PATH, $rootScope) {
        return {
            getrooms: $resource(SERVER_PATH + '/room/GetRooms', {}, {
                'query': { method: 'GET', isArray: true }
            }),
            postaddroom: $resource(SERVER_PATH + '/room/postaddroom', {}, {
                'post': { method: 'POST', isArray: false }
            }),
            postupdateroom: $resource(SERVER_PATH + '/room/postupdateroom', {}, {
                'post': { method: 'POST', isArray: false }
            }),
            postdeleteroom: $resource(SERVER_PATH + '/room/postdeleteroom', {}, {
                'post': { method: 'POST', isArray: false }
            }),
        }
    })

    .controller('RoomCtrl', function ($scope, $stateParams, RM, $rootScope, $timeout) {
        console.log('Room Controller');
        ///* Test Setting Start - Remove Later*/
        ////$rootScope.currentUser = { Id: 3, HomeId: 1 };
        ////$rootScope.showMessage = function () {
        ////    $rootScope.showMessageflag = true;
        ////    $timeout(function () { $rootScope.showMessageflag = false; }, 5000);
        ////}
        ///* Test Setting End*/

        $scope.resultMessage = "";
        $scope.resultSuccess = false;
        $scope.editFlag = false;

        $scope.rooms = RM.getrooms.query({ homeId: $rootScope.currentUser.HomeId });

        $scope.rooms.$promise.then(function (result) {
            console.log('Rooms');
        })

        $scope.validate = function () {
            //$scope.isValid = false;
            $scope.resultMessage = "";
            if ($scope.roomItem == undefined) {
                $scope.resultMessage = "Please enter room";
            }
            else if ($scope.roomItem.Description == undefined) {
                $scope.resultMessage = "Please enter room";
            }

            if ($scope.resultMessage != "") {
                $scope.resultSuccess = false;
                $rootScope.showMessage();
                return false;
            }
            return true;
        }

        //$scope.getRooms = function () {
        //    $scope.rooms = Room.getrooms.query({ roomId: $scope.roomItem.DescriptionId });
        //}

        $scope.save = function () {
            $scope.roomItem.TypeId = 1;
            if (!$scope.validate()) { return; }
            
            if ($scope.editFlag) {
                $scope.roomItem.UpdatedBy = $rootScope.currentUser.Id;
                $scope.roomAddResult = RM.postupdateroom.post($scope.roomItem);
            }
            else {
                $scope.roomItem.CreatedBy = $rootScope.currentUser.Id;
                $scope.roomItem.HomeId = $rootScope.currentUser.HomeId;
                $scope.roomAddResult = RM.postaddroom.post($scope.roomItem);
            }

            $scope.roomAddResult.$promise.then(function (result) {
                if (result.Success && !$scope.editFlag) {
                    $scope.roomItem.Id = result.Id;
                    $scope.rooms.push($scope.roomItem);
                }

                $scope.resultMessage = result.Message;
                $scope.resultSuccess = result.Success;
                $rootScope.showMessage();
                $scope.new();
            })
        }

        $scope.editItem = function (index) {
            $scope.editFlag = true;
            $scope.roomItem = $scope.rooms[index];
        }

        $scope.new = function () {
            $scope.editFlag = false;
            $scope.roomItem = { "Id": 0, "Description": "" };
        }

        $scope.remove = function (index) {
            $scope.roomItem = $scope.rooms[index];
            $scope.roomItem.UpdatedBy = $rootScope.currentUser.Id;
            $scope.indexRooms = index;
            $scope.roomAddResult = RM.postdeleteroom.post($scope.roomItem);
            $scope.new();
            $scope.roomAddResult.$promise.then(function (result) {
                if (result.Success) {
                    $scope.rooms.splice($scope.indexRooms, 1);
                }
                $scope.resultMessage = result.Message;
                $scope.resultSuccess = result.Success;
                $rootScope.showMessage();
            })
        }

    });

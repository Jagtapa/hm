﻿angular.module('hm.login', ['ngResource', 'hm.config'])
    .factory('User', function ($resource, SERVER_PATH, $rootScope) {
        return {
            login: $resource(SERVER_PATH + '/user/authenticate', {}, {
                'post': { method: 'POST', cache: $rootScope.cache, isArray: false }
            }),
            forgotPassword: $resource(SERVER_PATH + '/user/forgotpassword', {}, {
                'post': { method: 'POST', cache: $rootScope.cache, isArray: false }
            }),
        }
    })

    .controller('LoginCtrl', function ($scope, $state, $stateParams, User, $rootScope, CookiesService, $location, $timeout) {
        console.log('Login Controller');

        $scope.loginUser = { EmailId: '', Password: '' };

        $rootScope.showMessage = function () {
            $rootScope.showMessageflag = true;
            $timeout(function () { $rootScope.showMessageflag = false; }, 5000);
        }

        $scope.login = function () {
            console.time('login');
            $scope.resultMessage = "";
            if ($scope.loginUser.EmailId == "" ) {
                $scope.resultMessage = "Please Enter Valid Username";
            }
            else if ($scope.loginUser.Password == "") {
                $scope.resultMessage = "Please Enter Valid Password";
            }

            if ($scope.resultMessage != "") {
                $scope.resultSuccess = false;
                $rootScope.showMessage();
                return;
            }

            $scope.user = User.login.post($scope.loginUser);

            $scope.user.$promise.then(function (result) {
                if (result.FirstName != null) {
                    //window.localStorage.setItem('CurrentUser', JSON.stringify(result));
                    $rootScope.currentUser = result;                    
                    $timeout(function () { $state.go('home'); });
                }
                else {
                    $scope.resultMessage = "Please Enter Valid Username/Password";
                    $scope.resultSuccess = false;
                    $rootScope.showMessage();
                }
                console.timeEnd('login');
            })
        }

        $scope.forgotPassword = function () {
            console.time('forgotPassword');
            $scope.result = User.forgotPassword.post($scope.loginUser);
            $scope.result.$promise.then(function (result) {
                if ($scope.result.errorMessage != "") {
                    $scope.resultSuccess = false;
                    $scope.resultMessage = $scope.result.errorMessage;
                    $rootScope.showMessage();
                }
            })
            //$scope.result.$promise.then(function (result) {
            //    if (result.FirstName != null) {

            //        console.timeEnd('forgotPassword');
            //    }
            //})
        }
    });

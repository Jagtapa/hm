﻿angular.module('hm.roomDetails', ['ngResource', 'hm.config'])
    .factory('Room', function ($resource, SERVER_PATH, $rootScope) {
        return {
            getrooms: $resource(SERVER_PATH + '/room/GetRooms', {}, {
                'query': { method: 'GET', isArray: true }
            }),
            //getroomtypes: $resource(SERVER_PATH + '/room/GetRoomTypes', {}, {
            //    'query': { method: 'GET', cache: $rootScope.cache, isArray: true }
            //}),
            getroomitemtypes: $resource(SERVER_PATH + '/room/GetRoomItemTypes', {}, {
                'query': { method: 'GET', cache: $rootScope.cache, isArray: true }
            }),
            postaddroomdetails: $resource(SERVER_PATH + '/room/postaddroomdetails', {}, {
                'post': { method: 'POST', isArray: false }
            }),
            postupdateroomdetails: $resource(SERVER_PATH + '/room/postupdateroomdetails', {}, {
                'post': { method: 'POST', isArray: false }
            }),
            postdeleteroomdetails: $resource(SERVER_PATH + '/room/postdeleteroomdetails', {}, {
                'post': { method: 'POST', isArray: false }
            }),
            getroomdetails: $resource(SERVER_PATH + '/room/GetRoomDetails', {}, {
                'query': { method: 'GET', isArray: true }
            }),
        }
    })

    .controller('RoomDetailsCtrl', function ($scope, $stateParams, Room, $rootScope, $timeout) {
        console.log('Room Details Controller');
        ///* Test Setting Start - Remove Later*/
        ////$rootScope.currentUser = { Id: 3, HomeId: 1 };
        ////$rootScope.showMessage = function () {
        ////    $rootScope.showMessageflag = true;
        ////    $timeout(function () { $rootScope.showMessageflag = false; }, 5000);
        ////}
        ///* Test Setting End*/

        $scope.resultMessage = "";
        $scope.resultSuccess = false;
        $scope.editFlag = false;

        $scope.rooms = Room.getrooms.query({ homeId: $rootScope.currentUser.HomeId });
        $scope.roomitemtypes = Room.getroomitemtypes.query();

        $scope.rooms.$promise.then(function (result) {
            console.log('Rooms');
        })

        $scope.validate = function () {
            //$scope.isValid = false;
            $scope.resultMessage = "";
            if ($scope.roomDetailItem == undefined) {
                $scope.resultMessage = "Please select valid room";
            }
            else if ($scope.roomDetailItem.RoomId == undefined) {
                $scope.resultMessage = "Please select valid room";
            }

            else if ($scope.roomDetailItem.RoomItemTypeId == undefined) {
                $scope.resultMessage = "Please select valid room type";
            }
            else if ($scope.roomDetailItem.YearOfInstalled == undefined) {
                $scope.resultMessage = "Please select valid year of installation";
            }
            if ($scope.resultMessage != "") {
                $scope.resultSuccess = false;
                $rootScope.showMessage();
                return false;
            }
            return true;
        }

        //$scope.getRoomDetails = function () {
        //    $scope.roomDetails = Room.getroomdetails.query({ roomId: $scope.roomDetailItem.RoomId });
        //}

        $scope.save = function () {
            if (!$scope.validate()) { return;}
            $scope.roomDetailItem.CreatedBy = $rootScope.currentUser.Id;

            if ($scope.editFlag) {
                $scope.roomAddResult = Room.postupdateroomdetails.post($scope.roomDetailItem);
            }
            else {
                $scope.roomAddResult = Room.postaddroomdetails.post($scope.roomDetailItem);
            }

            $scope.roomAddResult.$promise.then(function (result) {
                if (result.Success && !$scope.editFlag) {
                    $scope.roomDetailItem.Id = result.Id;
                    $scope.roomDetailItem.Item = $.grep($scope.roomitemtypes, function (roomType) { return roomType.Id == $scope.roomDetailItem.RoomItemTypeId; })[0].Item;
                    //$scope.roomDetailItem.Item = itemName;
                    $scope.roomDetails.push($scope.roomDetailItem);
                }
                else if (result.Success && $scope.editFlag) {
                    $scope.roomDetailItem.Item = $.grep($scope.roomitemtypes, function (roomType) { return roomType.Id == $scope.roomDetailItem.RoomItemTypeId; })[0].Item;
                }

                $scope.resultMessage = result.Message;
                $scope.resultSuccess = result.Success;
                $rootScope.showMessage();
                $scope.new();
            })
        }



        $scope.roomChanged = function () {
            //$scope.selectedRoomId = $scope.roomDetailItem.RoomId;
            $scope.roomDetails = Room.getroomdetails.query({ roomId: $scope.roomDetailItem.RoomId });
            $scope.roomDetails.$promise.then(function (result) {
                console.log('get roomDetails called');
            })
        }

        $scope.editItem = function (index) {
            $scope.editFlag = true;
            $scope.roomDetailItem = $scope.roomDetails[index];
            $scope.roomDetailItem.UpdatedBy = $rootScope.currentUser.Id;
            //$scope.roomDetailItem.Id = item.Id;
            //$scope.roomDetailItem.RoomItemTypeId = item.RoomItemTypeId;
            //$scope.roomDetailItem.YearOfInstalled = item.YearOfInstalled;
            //$scope.roomDetailItem.Brand = item.Brand;
        }

        $scope.new = function () {
            $scope.editFlag = false;
            $scope.roomDetailItem = {"Id": 0, "RoomId": $scope.roomDetailItem.RoomId , "RoomItemTypeId": 0, "YearOfInstalled" : "", "Brand": ""};
            //$scope.roomDetailItem.YearOfInstalled = "";
            //$scope.roomDetailItem.Brand = "";
        }

        $scope.remove = function (index) {
            $scope.roomDetailItem = $scope.roomDetails[index];
            $scope.roomDetailItem.UpdatedBy = $rootScope.currentUser.Id;
            $scope.indexRoomDetails = index;
            $scope.roomAddResult = Room.postdeleteroomdetails.post($scope.roomDetailItem);
            $scope.new();
            $scope.roomAddResult.$promise.then(function (result) {
                if (result.Success) {
                    $scope.roomDetails.splice($scope.indexRoomDetails, 1);
                }
                $scope.resultMessage = result.Message;
                $scope.resultSuccess = result.Success;
                $rootScope.showMessage();
            })
        }

    });

﻿// <copyright file="RoomBusinessManager.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>23-07-2015</date>
// <summary> This class contains all the business logic for room.
// </summary>

namespace HM.Application.BusinessManagers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Transactions;
    using HM.Application.DataManagers;
    using HM.Entities;
    using HM.Framework;

    /// <summary>
    /// This class contains all the business logic for room and details.
    /// </summary>
    public class RoomBusinessManager
    {
        #region Public Methods
        /// <summary>
        /// create new room.
        /// </summary>
        /// <param name="room">The room entity.</param>
        /// <returns>The room entity with id.</returns>
        public Room AddRoom(Room room)
        {
            try
            {
                Logger.Write("Start RoomBusinessManager AddRoom", LogType.Information);
                RoomDataManager roomDataManager = new RoomDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    room = roomDataManager.AddRoom(room);
                    transactionScope.Complete();
                }
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End RoomBusinessManager AddRoom", LogType.Information);
            }

            return room;
        }

        /// <summary>
        /// add room details.
        /// </summary>
        /// <param name="roomDetails">The room details entity.</param>
        /// <returns>The room details entity with id.</returns>
        public RoomDetails AddRoomDetails(RoomDetails roomDetails)
        {
            try
            {
                Logger.Write("Start RoomBusinessManager AddRoomDetails", LogType.Information);
                RoomDataManager roomDataManager = new RoomDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    roomDetails = roomDataManager.AddRoomDetails(roomDetails);
                    transactionScope.Complete();
                }
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End RoomBusinessManager AddRoomDetails", LogType.Information);
            }

            return roomDetails;
        }

        /// <summary>
        /// add room item type.
        /// </summary>
        /// <param name="roomItemType">The room item type entity.</param>
        /// <returns>The room item type entity with id.</returns>
        public RoomItemType AddRoomItemType(RoomItemType roomItemType)
        {
            try
            {
                Logger.Write("Start RoomBusinessManager AddRoomItemType", LogType.Information);
                RoomDataManager roomDataManager = new RoomDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    roomItemType = roomDataManager.AddRoomItemType(roomItemType);
                    transactionScope.Complete();
                }
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End RoomBusinessManager AddRoomItemType", LogType.Information);
            }

            return roomItemType;
        }

        /// <summary>
        /// add room type.
        /// </summary>
        /// <param name="roomType">The room type entity.</param>
        /// <returns>The room type entity with id.</returns>
        public RoomType AddRoomType(RoomType roomType)
        {
            try
            {
                Logger.Write("Start RoomBusinessManager AddRoomType", LogType.Information);
                RoomDataManager roomDataManager = new RoomDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    roomType = roomDataManager.AddRoomType(roomType);
                    transactionScope.Complete();
                }
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End RoomBusinessManager AddRoomType", LogType.Information);
            }

            return roomType;
        }

        /// <summary>
        /// update room.
        /// </summary>
        /// <param name="room">The room entity.</param>
        /// <returns>The boolean value.</returns>
        public bool UpdateRoom(Room room)
        {
            bool isSuccess = true;
            try
            {
                Logger.Write("Start RoomBusinessManager UpdateRoom", LogType.Information);
                RoomDataManager roomDataManager = new RoomDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    isSuccess = roomDataManager.UpdateRoom(room);
                    transactionScope.Complete();
                }
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End RoomBusinessManager UpdateRoom", LogType.Information);
            }

            return isSuccess;
        }

        /// <summary>
        /// update room details.
        /// </summary>
        /// <param name="roomDetails">The room details entity.</param>
        /// <returns>The boolean value.</returns>
        public bool UpdateRoomDetails(RoomDetails roomDetails)
        {
            bool isSuccess = true;
            try
            {
                Logger.Write("Start RoomBusinessManager UpdateRoomDetails", LogType.Information);
                RoomDataManager roomDataManager = new RoomDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    isSuccess = roomDataManager.UpdateRoomDetails(roomDetails);
                    transactionScope.Complete();
                }
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End RoomBusinessManager UpdateRoomDetails", LogType.Information);
            }

            return isSuccess;
        }

        /// <summary>
        /// update room item type.
        /// </summary>
        /// <param name="roomItemType">The room item type entity.</param>
        /// <returns>The boolean value.</returns>
        public bool UpdateRoomItemType(RoomItemType roomItemType)
        {
            bool isSuccess = true;
            try
            {
                Logger.Write("Start RoomBusinessManager UpdateRoomItemType", LogType.Information);
                RoomDataManager roomDataManager = new RoomDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    isSuccess = roomDataManager.UpdateRoomItemType(roomItemType);
                    transactionScope.Complete();
                }
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End RoomBusinessManager UpdateRoomItemType", LogType.Information);
            }

            return isSuccess;
        }

        /// <summary>
        /// update room type.
        /// </summary>
        /// <param name="roomType">The room item entity.</param>
        /// <returns>The boolean value.</returns>
        public bool UpdateRoomType(RoomType roomType)
        {
            bool isSuccess = true;
            try
            {
                Logger.Write("Start RoomBusinessManager UpdateRoomType", LogType.Information);
                RoomDataManager roomDataManager = new RoomDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    isSuccess = roomDataManager.UpdateRoomType(roomType);
                    transactionScope.Complete();
                }
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End RoomBusinessManager UpdateRoomType", LogType.Information);
            }

            return isSuccess;
        }

        /// <summary>
        /// delete room.
        /// </summary>
        /// <param name="room">The room entity.</param>
        /// <returns>The boolean value.</returns>
        public bool DeleteRoom(Room room)
        {
            bool isSuccess = true;
            try
            {
                Logger.Write("Start RoomBusinessManager DeleteRoom", LogType.Information);
                RoomDataManager roomDataManager = new RoomDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    isSuccess = roomDataManager.DeleteRoom(room);
                    transactionScope.Complete();
                }
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End RoomBusinessManager DeleteRoom", LogType.Information);
            }

            return isSuccess;
        }

        /// <summary>
        /// delete room details.
        /// </summary>
        /// <param name="roomDetails">The room details entity.</param>
        /// <returns>The boolean value.</returns>
        public bool DeleteRoomDetails(RoomDetails roomDetails)
        {
            bool isSuccess = true;
            try
            {
                Logger.Write("Start RoomBusinessManager DeleteRoomDetails", LogType.Information);
                RoomDataManager roomDataManager = new RoomDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    isSuccess = roomDataManager.DeleteRoomDetails(roomDetails);
                    transactionScope.Complete();
                }
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End RoomBusinessManager DeleteRoomDetails", LogType.Information);
            }

            return isSuccess;
        }

        /// <summary>
        /// delete room type.
        /// </summary>
        /// <param name="roomType">The room type entity.</param>
        /// <returns>The boolean value.</returns>
        public bool DeleteRoomType(RoomType roomType)
        {
            bool isSuccess = true;
            try
            {
                Logger.Write("Start RoomBusinessManager DeleteRoomType", LogType.Information);
                RoomDataManager roomDataManager = new RoomDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    isSuccess = roomDataManager.DeleteRoomType(roomType);
                    transactionScope.Complete();
                }
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End RoomBusinessManager DeleteRoomType", LogType.Information);
            }

            return isSuccess;
        }

        /// <summary>
        /// delete room item type.
        /// </summary>
        /// <param name="roomItemType">The room item type entity.</param>
        /// <returns>The boolean value.</returns>
        public bool DeleteRoomItemType(RoomItemType roomItemType)
        {
            bool isSuccess = true;
            try
            {
                Logger.Write("Start RoomBusinessManager DeleteRoomItemType", LogType.Information);
                RoomDataManager roomDataManager = new RoomDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    isSuccess = roomDataManager.DeleteRoomItemType(roomItemType);
                    transactionScope.Complete();
                }
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End RoomBusinessManager DeleteRoomItemType", LogType.Information);
            }

            return isSuccess;
        }

        /// <summary>
        /// Get list of rooms for home.
        /// </summary>
        /// <param name="homeId">The home id.</param>
        /// <returns>The list of records</returns>
        public List<Room> GetRooms(long homeId)
        {
            List<Room> roomList = null;
            try
            {
                Logger.Write("Start RoomBusinessManager GetRooms", LogType.Information);
                RoomDataManager roomDataManager = new RoomDataManager();
                roomList = roomDataManager.GetRooms(homeId);
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End RoomBusinessManager GetRooms", LogType.Information);
            }

            return roomList;
        }

        /// <summary>
        /// Get list of room item details.
        /// </summary>
        /// <param name="roomId">The room id.</param>
        /// <returns>The list of records</returns>
        public List<RoomDetails> GetRoomDetails(long roomId)
        {
            List<RoomDetails> roomDetailsList = null;
            try
            {
                Logger.Write("Start RoomBusinessManager GetRoomDetails", LogType.Information);
                RoomDataManager roomDataManager = new RoomDataManager();
                roomDetailsList = roomDataManager.GetRoomDetails(roomId);
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End RoomBusinessManager GetRoomDetails", LogType.Information);
            }

            return roomDetailsList;
        }

        /// <summary>
        /// Get list of room types.
        /// </summary>
        /// <returns>The list of records</returns>
        public List<RoomType> GetRoomTypes()
        {
            List<RoomType> roomTypeList = null;
            try
            {
                Logger.Write("Start RoomBusinessManager GetRoomTypes", LogType.Information);
                RoomDataManager roomDataManager = new RoomDataManager();
                roomTypeList = roomDataManager.GetRoomTypes();
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End RoomBusinessManager GetRoomTypes", LogType.Information);
            }

            return roomTypeList;
        }

        /// <summary>
        /// Get list of room item types.
        /// </summary>
        /// <returns>The list of records</returns>
        public List<RoomItemType> GetRoomItemTypes()
        {
            List<RoomItemType> roomItemTypeList = null;
            try
            {
                Logger.Write("Start RoomBusinessManager GetRoomItemTypes", LogType.Information);
                RoomDataManager roomDataManager = new RoomDataManager();
                roomItemTypeList = roomDataManager.GetRoomItemTypes();
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End RoomBusinessManager GetRoomItemTypes", LogType.Information);
            }

            return roomItemTypeList;
        }
        #endregion
    }
}

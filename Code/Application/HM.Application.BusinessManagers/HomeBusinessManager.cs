﻿// <copyright file="HomeBusinessManager.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>21-07-2015</date>
// <summary> This class contains all the business logic for house.
// </summary>

namespace HM.Application.BusinessManagers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Transactions;
    using HM.Application.DataManagers;
    using HM.Entities;
    using HM.Framework;

    /// <summary>
    /// This class contains all the business logic for house.
    /// </summary>
    public class HomeBusinessManager
    {
        #region Public Methods
        /// <summary>
        /// Create new home.
        /// </summary>
        /// <param name="home">The home entity.</param>
        /// <returns>The result of update.</returns>
        public Home AddHome(Home home)
        {
            try
            {
                Logger.Write("Start HomeBusinessManager AddHome", LogType.Information);
                HomeDataManager homeDataManager = new HomeDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    home = homeDataManager.AddHome(home);
                    transactionScope.Complete();
                }
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End HomeBusinessManager AddHome", LogType.Information);
            }

            return home;
        }

        /// <summary>
        /// update home details
        /// </summary>
        /// <param name="home">The home entity.</param>
        /// <returns>The result of update.</returns>
        public bool UpdateHome(Home home)
        {
            bool isSuccess = true;
            try
            {
                Logger.Write("Start HomeBusinessManager UpdateHome", LogType.Information);
                HomeDataManager homeDataManager = new HomeDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    isSuccess = homeDataManager.UpdateHome(home);
                    transactionScope.Complete();
                }
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End HomeBusinessManager UpdateHome", LogType.Information);
            }

            return isSuccess;
        }

        /// <summary>
        /// delete home details
        /// </summary>
        /// <param name="home">The home entity.</param>
        /// <returns>The result of update.</returns>
        public bool DeleteHome(Home home)
        {
            bool isSuccess = true;
            try
            {
                Logger.Write("Start HomeBusinessManager DeleteHome", LogType.Information);
                HomeDataManager homeDataManager = new HomeDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    isSuccess = homeDataManager.DeleteHome(home);
                    transactionScope.Complete();
                }
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End HomeBusinessManager DeleteHome", LogType.Information);
            }

            return isSuccess;
        }

        /// <summary>
        /// Get list of homes for user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns>The list of records</returns>
        public List<Home> GetHomes(long userId)
        {
            List<Home> homeList = null;
            try
            {
                Logger.Write("Start HomeBusinessManager GetHomes", LogType.Information);
                HomeDataManager homeDataManager = new HomeDataManager();
                homeList = homeDataManager.GetHomes(userId);
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
            finally
            {
                Logger.Write("End HomeBusinessManager GetHomes", LogType.Information);
            }

            return homeList;
        }
        #endregion
    }
}

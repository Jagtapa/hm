﻿// <copyright file="UserBusinessManager.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>14-05-2014</date>
// <summary>This class is used to perform business operations related to user.
// </summary>

namespace HM.Application.UserManagement.BusinessManagers
{
    using HM.Application.UserManagement.DataManagers;
    using HM.Entities.UserManagement;
    using HM.Framework;
    using System;
    using System.Collections.Generic;
    using System.Transactions;

    /// <summary>
    /// This class is used to perform business operations related to user.
    /// </summary>
    public class UserBusinessManager
    {

        /// <summary>
        /// Get the details of user.
        /// </summary>
        /// <param name="userEntity">The user to get details.</param>
        /// <returns>The details of user</returns>
        public UserEntity GetDetails(UserEntity userEntity)
        {
            try
            {
                Logger.Write("Start UserBusinessManager GetDetails", LogType.Information);
                UserDataManager userDataManager = new UserDataManager();
                userEntity = userDataManager.GetDetails(userEntity);
                Logger.Write("End UserBusinessManager GetDetails", LogType.Information);
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }

            return userEntity;
        }

        /// <summary>
        /// Authenticate the user.
        /// </summary>
        /// <param name="userEntity">The user to Authenticate.</param>
        /// <returns>The details of user</returns>
        public UserEntity Authenticate(UserEntity userEntity)
        {
            try
            {
                Logger.Write("Start UserBusinessManager Authenticate", LogType.Information);
                UserDataManager userDataManager = new UserDataManager();
                userEntity = userDataManager.Authenticate(userEntity);
                Logger.Write("End UserBusinessManager Authenticate", LogType.Information);
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }

            return userEntity;
        }

        /// <summary>
        /// Register user with first,last name and emailid. 
        /// </summary>
        /// <param name="userEntity">The User entity.</param>
        /// <returns>Returns user Entity</returns>
        public UserEntity RegisterUser(UserEntity userEntity)
        {
            try
            {
                Logger.Write("Start UserBusinessManager RegisterUser", LogType.Information);
                UserDataManager userDataManager = new UserDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    userEntity = userDataManager.RegisterUser(userEntity);
                    transactionScope.Complete();
                }

                Logger.Write("End UserBusinessManager RegisterUser", LogType.Information);
            }
            catch (Exception ex)
            {
                ex.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }

            return userEntity;
        }

        /// <summary>
        /// Update user details. 
        /// </summary>
        /// <param name="userEntity">The User entity.</param>
        /// <returns>Returns boolean value</returns>
        public bool UpdateUser(UserEntity userEntity)
        {
            bool isUpdated = false;
            try
            {
                Logger.Write("Start UserBusinessManager UpdateUser", LogType.Information);
                UserDataManager userDataManager = new UserDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    isUpdated = userDataManager.UpdateUser(userEntity);
                    transactionScope.Complete();
                }

                Logger.Write("End UserBusinessManager UpdateUser", LogType.Information);
            }
            catch (Exception ex)
            {
                ex.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }

            return isUpdated;
        }

        /// <summary>
        /// Update user details. 
        /// </summary>
        /// <param name="userEntity">The User entity.</param>
        /// <returns>Returns boolean value</returns>
        public bool DeleteUser(UserEntity userEntity)
        {
            bool isUpdated = false;
            try
            {
                Logger.Write("Start UserBusinessManager DeleteUser", LogType.Information);
                UserDataManager userDataManager = new UserDataManager();
                using (TransactionScope transactionScope = new TransactionScope(TransactionScopeOption.Required, CommonUtility.GetRequireTransactionOptions()))
                {
                    isUpdated = userDataManager.DeleteUser(userEntity);
                    transactionScope.Complete();
                }

                Logger.Write("End UserBusinessManager DeleteUser", LogType.Information);
            }
            catch (Exception ex)
            {
                ex.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }

            return isUpdated;
        }

        /// <summary>
        /// Get the details of user.
        /// </summary>
        /// <param name="userEntity">The user to get details.</param>
        /// <returns>The details of user</returns>
        public UserEntity ForgotPassword(UserEntity userEntity)
        {
            try
            {
                Logger.Write("Start UserBusinessManager ForgotPassword", LogType.Information);
                UserDataManager userDataManager = new UserDataManager();
                userEntity = userDataManager.GetDetails(userEntity);

                if(userEntity.IsValid)
                {
                    MailEntity mailEntity = new MailEntity();
                    mailEntity.Host = ConfigManager.ReadAppSettings("SMTPSERVER");
                    mailEntity.From = ConfigManager.ReadAppSettings("ADMINISTRATOREMAIL");
                    List<string> toemails = new List<string>();
                    toemails.Add(userEntity.EmailId);
                    mailEntity.ToAddressList = toemails;
                    mailEntity.Subject = "Home Manager Admin";
                    mailEntity.Body = "Your Home Manager password is " + userEntity.Password;

                    MailManager.SendMail(mailEntity);
                }
                Logger.Write("End UserBusinessManager ForgotPassword", LogType.Information);
            }
            catch (Exception exception)
            {
                exception.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }

            return userEntity;
        }
    }
}

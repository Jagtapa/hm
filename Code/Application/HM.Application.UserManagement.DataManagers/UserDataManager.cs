﻿// <copyright file="UserDataManager.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>14-05-2014</date>
// <summary>UserDataManager class used to perform DB operations related to USER accounts.
// </summary>

namespace HM.Application.UserManagement.DataManagers
{
    using System;
    using System.Data;
    using System.Text;
    using HM.Entities.UserManagement;
    using HM.Framework;
    using HM.Framework.Application;

    /// <summary>
    /// UserDataManager class used to perform DB operations related to USER.
    /// </summary>
    public class UserDataManager
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the UserDataManager class.
        /// </summary>
        public UserDataManager()
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets the user details based on user email id. 
        /// </summary>
        /// <param name="userEntity">The User entity.</param>
        /// <returns>Returns user Entity</returns>
        public UserEntity GetDetails(UserEntity userEntity)
        {
            if (userEntity != null)
            {
                IDataReader dataReader = null;
                try
                {
                    CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPGETUSERDETAILS);
                    CommonDataAccess.AddInParameter(Constants.EMAILID.ToMySqlParameter(), DbType.String, userEntity.EmailId);
                    dataReader = CommonDataAccess.ExecuteReader();
                    userEntity.IsValid = false;
                    if (dataReader.Read())
                    {
                        userEntity.IsValid = true;
                        userEntity.Id = dataReader[Constants.ID].ObjectToInt32();
                        userEntity.FirstName = dataReader[Constants.FIRSTNAME].ObjectToString();
                        userEntity.LastName = dataReader[Constants.LASTNAME].ObjectToString();
                        userEntity.Password = dataReader[Constants.USERPASSWORD].ObjectToString();
                        userEntity.HomeId = dataReader[Constants.HOMEID].ObjectToInt64();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    dataReader.CloseDataReader();
                }
            }

            return userEntity;
        }

        /// <summary>
        /// Authenticate user details based on user email id. 
        /// </summary>
        /// <param name="userEntity">The User entity.</param>
        /// <returns>Returns user Entity</returns>
        public UserEntity Authenticate(UserEntity userEntity)
        {
            if (userEntity != null)
            {
                IDataReader dataReader = null;
                try
                {
                    CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPAUTHENTICATE);
                    CommonDataAccess.AddInParameter(Constants.EMAILID.ToMySqlParameter(), DbType.String, userEntity.EmailId);
                    CommonDataAccess.AddInParameter(Constants.USERPASSWORD.ToMySqlParameter(), DbType.String, userEntity.Password);
                    dataReader = CommonDataAccess.ExecuteReader();
                    userEntity.IsValid = false;
                    if (dataReader.Read())
                    {
                        userEntity.IsValid = true;
                        userEntity.Id = dataReader[Constants.ID].ObjectToInt32();
                        userEntity.FirstName = dataReader[Constants.FIRSTNAME].ObjectToString();
                        userEntity.LastName = dataReader[Constants.LASTNAME].ObjectToString();
                        userEntity.Password = String.Empty;
                        userEntity.HomeId = dataReader[Constants.HOMEID].ObjectToInt64();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    dataReader.CloseDataReader();
                }
            }

            return userEntity;
        }

        /// <summary>
        /// Register user with first,last name and emailid. 
        /// </summary>
        /// <param name="userEntity">The User entity.</param>
        /// <returns>Returns user Entity</returns>
        public UserEntity RegisterUser(UserEntity userEntity)
        {
            try
            {
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPREGISTERUSER);
                CommonDataAccess.AddInParameter(Constants.FIRSTNAME.ToMySqlParameter(), DbType.String, userEntity.FirstName);
                CommonDataAccess.AddInParameter(Constants.LASTNAME.ToMySqlParameter(), DbType.String, userEntity.LastName);
                CommonDataAccess.AddInParameter(Constants.EMAILID.ToMySqlParameter(), DbType.String, userEntity.EmailId);
                CommonDataAccess.AddInParameter(Constants.USERPASSWORD.ToMySqlParameter(), DbType.String, userEntity.Password);
                CommonDataAccess.AddOutParameter(Constants.ID.ToMySqlParameter(), DbType.Int64, Int32.MaxValue, false, 0, 0);
                CommonDataAccess.AddInParameter(Constants.CREATEDON.ToMySqlParameter(), DbType.DateTime, DateTime.Now.ObjectToDBDateTime());
                CommonDataAccess.ExceuteNonQuery();
                userEntity.Id = CommonDataAccess.GetParameterValue(Constants.ID.ToMySqlParameter()).ObjectToInt64();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return userEntity;
        }

        /// <summary>
        /// Update user details. 
        /// </summary>
        /// <param name="userEntity">The User entity.</param>
        /// <returns>Returns boolean value</returns>
        public bool UpdateUser(UserEntity userEntity)
        {
            try
            {
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPUPDATEUSER);
                CommonDataAccess.AddInParameter(Constants.FIRSTNAME.ToMySqlParameter(), DbType.String, userEntity.FirstName);
                CommonDataAccess.AddInParameter(Constants.LASTNAME.ToMySqlParameter(), DbType.String, userEntity.LastName);
                CommonDataAccess.AddInParameter(Constants.EMAILID.ToMySqlParameter(), DbType.String, userEntity.EmailId);
                CommonDataAccess.AddInParameter(Constants.ID.ToMySqlParameter(), DbType.Int64, userEntity.Id);
                return CommonDataAccess.ExceuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete user details. 
        /// </summary>
        /// <param name="userEntity">The User entity.</param>
        /// <returns>Returns boolean value</returns>
        public bool DeleteUser(UserEntity userEntity)
        {
            try
            {
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPDELETEUSER);
                CommonDataAccess.AddInParameter(Constants.ID.ToMySqlParameter(), DbType.Int64, userEntity.Id);
                return CommonDataAccess.ExceuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}

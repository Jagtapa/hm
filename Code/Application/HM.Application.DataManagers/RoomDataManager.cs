﻿// <copyright file="RoomDataManager.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2015 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>22-07-2015</date>
// <summary> This class is used to deal with room related table operations.
// </summary>
namespace HM.Application.DataManagers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using HM.Entities;
    using HM.Framework;
    using HM.Framework.Application;

    /// <summary>
    /// This class is used to deal with room related table operations.
    /// </summary>
    public class RoomDataManager
    {
        /// <summary>
        /// Get list of room item details.
        /// </summary>
        /// <param name="roomId">The room id.</param>
        /// <returns>The list of records</returns>
        public List<RoomDetails> GetRoomDetails(long roomId)
        {
            IDataReader dataReader = null;
            List<RoomDetails> roomDetailsList = new List<RoomDetails>();
            try
            {
                Logger.Write("Start RoomDataManager GetRoomDetails", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPGETROOMDETAILS);
                CommonDataAccess.AddInParameter(Constants.ROOMID.ToMySqlParameter(), DbType.Int64, roomId);
                dataReader = CommonDataAccess.ExecuteReader();
                RoomDetails roomDetails = null;
                while (dataReader.Read())
                {
                    roomDetails = new RoomDetails();
                    roomDetails.RoomId = roomId;
                    roomDetails.Id = dataReader[Constants.ID].ObjectToInt64();
                    roomDetails.RoomItemTypeId = dataReader[Constants.ROOMITEMTYPEID].ObjectToInt32();
                    roomDetails.Brand = dataReader[Constants.BRAND].ObjectToString();
                    roomDetails.YearOfInstalled = dataReader[Constants.YEAROFINSTALLED].ObjectToInt32();
                    roomDetails.Quantity = dataReader[Constants.QUANTITY].ObjectToInt32();
                    roomDetails.Type = dataReader[Constants.TYPE].ObjectToString();
                    roomDetails.Item = dataReader[Constants.ITEM].ObjectToString();
                    roomDetails.CreatedBy = dataReader[Constants.CREATEDBY].ObjectToInt64();
                    roomDetails.UpdatedBy = dataReader[Constants.UPDATEDBY].ObjectToInt64();
                    roomDetails.CreatedOn = dataReader[Constants.CREATEDON].ObjectToDateTime();
                    roomDetails.UpdatedOn = dataReader[Constants.UPDATEDON].ObjectToDateTime();
                    roomDetailsList.Add(roomDetails);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dataReader.CloseDataReader();
                Logger.Write("End RoomDataManager GetRoomDetails", LogType.Information);
            }

            return roomDetailsList;
        }

        /// <summary>
        /// Get list of room types.
        /// </summary>
        /// <returns>The list of records</returns>
        public List<RoomType> GetRoomTypes()
        {
            IDataReader dataReader = null;
            List<RoomType> roomTypeList = new List<RoomType>();
            try
            {
                Logger.Write("Start RoomDataManager GetRoomTypes", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPGETROOMTYPES);
                dataReader = CommonDataAccess.ExecuteReader();
                RoomType roomType = null;
                while (dataReader.Read())
                {
                    roomType = new RoomType();
                    roomType.Id = dataReader[Constants.ID].ObjectToInt32();                    
                    roomType.Type = dataReader[Constants.TYPE].ObjectToString();
                    roomType.CreatedBy = dataReader[Constants.CREATEDBY].ObjectToInt64();
                    roomType.UpdatedBy = dataReader[Constants.UPDATEDBY].ObjectToInt64();
                    roomType.CreatedOn = dataReader[Constants.CREATEDON].ObjectToDateTime();
                    roomType.UpdatedOn = dataReader[Constants.UPDATEDON].ObjectToDateTime();
                    roomTypeList.Add(roomType);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dataReader.CloseDataReader();
                Logger.Write("End RoomDataManager GetRoomTypes", LogType.Information);
            }

            return roomTypeList;
        }

        /// <summary>
        /// Get list of room item types.
        /// </summary>
        /// <returns>The list of records</returns>
        public List<RoomItemType> GetRoomItemTypes()
        {
            IDataReader dataReader = null;
            List<RoomItemType> roomItemTypeList = new List<RoomItemType>();
            try
            {
                Logger.Write("Start RoomDataManager GetRoomItemTypes", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPGETROOMITEMTYPES);
                dataReader = CommonDataAccess.ExecuteReader();
                RoomItemType roomItemType = null;
                while (dataReader.Read())
                {
                    roomItemType = new RoomItemType();
                    roomItemType.Id = dataReader[Constants.ID].ObjectToInt32();
                    roomItemType.Item = dataReader[Constants.ITEM].ObjectToString();
                    roomItemType.CreatedBy = dataReader[Constants.CREATEDBY].ObjectToInt64();
                    roomItemType.UpdatedBy = dataReader[Constants.UPDATEDBY].ObjectToInt64();
                    roomItemType.CreatedOn = dataReader[Constants.CREATEDON].ObjectToDateTime();
                    roomItemType.UpdatedOn = dataReader[Constants.UPDATEDON].ObjectToDateTime();
                    roomItemTypeList.Add(roomItemType);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dataReader.CloseDataReader();
                Logger.Write("End RoomDataManager GetRoomItemTypes", LogType.Information);
            }

            return roomItemTypeList;
        }

        /// <summary>
        /// Get list of rooms for home.
        /// </summary>
        /// <param name="homeId">The home id.</param>
        /// <returns>The list of records</returns>
        public List<Room> GetRooms(long homeId)
        {
            IDataReader dataReader = null;
            List<Room> roomList = new List<Room>();
            try
            {
                Logger.Write("Start RoomDataManager GetRooms", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPGETROOMS);
                CommonDataAccess.AddInParameter(Constants.HOMEID.ToMySqlParameter(), DbType.Int64, homeId);
                dataReader = CommonDataAccess.ExecuteReader();
                Room room = null;
                while (dataReader.Read())
                {
                    room = new Room();
                    room.Id = dataReader[Constants.ID].ObjectToInt64();
                    room.Description = dataReader[Constants.DESCRIPTION].ObjectToString();
                    room.TypeId = dataReader[Constants.TYPEID].ObjectToInt32();
                    room.Type = dataReader[Constants.TYPE].ObjectToString();
                    room.CreatedBy = dataReader[Constants.CREATEDBY].ObjectToInt64();
                    room.UpdatedBy = dataReader[Constants.UPDATEDBY].ObjectToInt64();
                    room.CreatedOn = dataReader[Constants.CREATEDON].ObjectToDateTime();
                    room.UpdatedOn = dataReader[Constants.UPDATEDON].ObjectToDateTime();
                    roomList.Add(room);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dataReader.CloseDataReader();
                Logger.Write("End RoomDataManager GetRooms", LogType.Information);
            }

            return roomList;
        }

        /// <summary>
        /// create new room.
        /// </summary>
        /// <param name="room">The room entity.</param>
        /// <returns>The room entity with id.</returns>
        public Room AddRoom(Room room)
        {
            try
            {
                Logger.Write("Start RoomDataManager AddRoom", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPADDROOM);
                CommonDataAccess.AddInParameter(Constants.HOMEID.ToMySqlParameter(), DbType.Int64, room.HomeId);
                CommonDataAccess.AddInParameter(Constants.TYPEID.ToMySqlParameter(), DbType.Int32, room.TypeId);
                CommonDataAccess.AddInParameter(Constants.DESCRIPTION.ToMySqlParameter(), DbType.String, room.Description);
                CommonDataAccess.AddInParameter(Constants.CREATEDBY.ToMySqlParameter(), DbType.Int64, room.CreatedBy);
                CommonDataAccess.AddInParameter(Constants.CREATEDON.ToMySqlParameter(), DbType.DateTime, DateTime.Now.ObjectToDBDateTime());
                CommonDataAccess.AddOutParameter(Constants.ID.ToMySqlParameter(), DbType.Int64, Int32.MaxValue, false, 0, 0);
                CommonDataAccess.ExceuteNonQuery();
                room.Id = CommonDataAccess.GetParameterValue(Constants.ID.ToMySqlParameter()).ObjectToInt64();
            }
            catch
            {
                throw;
            }
            finally
            {
                Logger.Write("End RoomDataManager AddRoom", LogType.Information);
            }

            return room;
        }

        /// <summary>
        /// add room details.
        /// </summary>
        /// <param name="roomDetails">The room details entity.</param>
        /// <returns>The room details entity with id.</returns>
        public RoomDetails AddRoomDetails(RoomDetails roomDetails)
        {
            try
            {
                Logger.Write("Start RoomDataManager AddRoomDetails", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPADDROOMDETAILS);
                CommonDataAccess.AddInParameter(Constants.ROOMID.ToMySqlParameter(), DbType.Int64, roomDetails.RoomId);
                CommonDataAccess.AddInParameter(Constants.ROOMITEMTYPEID.ToMySqlParameter(), DbType.Int32, roomDetails.RoomItemTypeId);
                CommonDataAccess.AddInParameter(Constants.BRAND.ToMySqlParameter(), DbType.String, roomDetails.Brand);
                CommonDataAccess.AddInParameter(Constants.YEAROFINSTALLED.ToMySqlParameter(), DbType.Int32, roomDetails.YearOfInstalled);
                CommonDataAccess.AddInParameter(Constants.QUANTITY.ToMySqlParameter(), DbType.Int32, roomDetails.Quantity);
                CommonDataAccess.AddInParameter(Constants.CREATEDBY.ToMySqlParameter(), DbType.Int64, roomDetails.CreatedBy);
                CommonDataAccess.AddInParameter(Constants.CREATEDON.ToMySqlParameter(), DbType.DateTime, DateTime.Now.ObjectToDBDateTime());
                CommonDataAccess.AddOutParameter(Constants.ID.ToMySqlParameter(), DbType.Int64, Int32.MaxValue, false, 0, 0);
                CommonDataAccess.ExceuteNonQuery();
                roomDetails.Id = CommonDataAccess.GetParameterValue(Constants.ID.ToMySqlParameter()).ObjectToInt64();
            }
            catch
            {
                throw;
            }
            finally
            {
                Logger.Write("End RoomDataManager AddRoomDetails", LogType.Information);
            }

            return roomDetails;
        }

        /// <summary>
        /// add room item type.
        /// </summary>
        /// <param name="roomItemType">The room item type entity.</param>
        /// <returns>The room item type entity with id.</returns>
        public RoomItemType AddRoomItemType(RoomItemType roomItemType)
        {
            try
            {
                Logger.Write("Start RoomDataManager AddRoomItemType", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPADDROOMITEMTYPE);                
                CommonDataAccess.AddInParameter(Constants.ITEM.ToMySqlParameter(), DbType.String, roomItemType.Item);
                CommonDataAccess.AddInParameter(Constants.CREATEDBY.ToMySqlParameter(), DbType.Int64, roomItemType.CreatedBy);
                CommonDataAccess.AddInParameter(Constants.CREATEDON.ToMySqlParameter(), DbType.DateTime, DateTime.Now.ObjectToDBDateTime());
                CommonDataAccess.AddOutParameter(Constants.ID.ToMySqlParameter(), DbType.Int64, Int32.MaxValue, false, 0, 0);
                CommonDataAccess.ExceuteNonQuery();
                roomItemType.Id = CommonDataAccess.GetParameterValue(Constants.ID.ToMySqlParameter()).ObjectToInt32();
            }
            catch
            {
                throw;
            }
            finally
            {
                Logger.Write("End RoomDataManager AddRoomItemType", LogType.Information);
            }

            return roomItemType;
        }

        /// <summary>
        /// add room type.
        /// </summary>
        /// <param name="roomType">The room type entity.</param>
        /// <returns>The room type entity with id.</returns>
        public RoomType AddRoomType(RoomType roomType)
        {
            try
            {
                Logger.Write("Start RoomDataManager AddRoomType", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPADDROOMTYPE);
                CommonDataAccess.AddInParameter(Constants.TYPE.ToMySqlParameter(), DbType.String, roomType.Type);
                CommonDataAccess.AddInParameter(Constants.CREATEDBY.ToMySqlParameter(), DbType.Int64, roomType.CreatedBy);
                CommonDataAccess.AddInParameter(Constants.CREATEDON.ToMySqlParameter(), DbType.DateTime, DateTime.Now.ObjectToDBDateTime());
                CommonDataAccess.AddOutParameter(Constants.ID.ToMySqlParameter(), DbType.Int64, Int32.MaxValue, false, 0, 0);
                CommonDataAccess.ExceuteNonQuery();
                roomType.Id = CommonDataAccess.GetParameterValue(Constants.ID.ToMySqlParameter()).ObjectToInt32();
            }
            catch
            {
                throw;
            }
            finally
            {
                Logger.Write("End RoomDataManager AddRoomType", LogType.Information);
            }

            return roomType;
        }

        /// <summary>
        /// update room.
        /// </summary>
        /// <param name="room">The room entity.</param>
        /// <returns>The boolean value.</returns>
        public bool UpdateRoom(Room room)
        {
            try
            {
                Logger.Write("Start RoomDataManager UpdateRoom", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPUPDATEROOM);
                CommonDataAccess.AddInParameter(Constants.ID.ToMySqlParameter(), DbType.Int64, room.Id);
                CommonDataAccess.AddInParameter(Constants.TYPEID.ToMySqlParameter(), DbType.Int32, room.TypeId);
                CommonDataAccess.AddInParameter(Constants.DESCRIPTION.ToMySqlParameter(), DbType.String, room.Description);
                CommonDataAccess.AddInParameter(Constants.UPDATEDBY.ToMySqlParameter(), DbType.Int64, room.UpdatedBy);
                CommonDataAccess.AddInParameter(Constants.UPDATEDON.ToMySqlParameter(), DbType.DateTime, DateTime.Now.ObjectToDBDateTime());
                return CommonDataAccess.ExceuteNonQuery() > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                Logger.Write("End RoomDataManager UpdateRoom", LogType.Information);
            }
        }

        /// <summary>
        /// update room details.
        /// </summary>
        /// <param name="roomDetails">The room details entity.</param>
        /// <returns>The boolean value.</returns>
        public bool UpdateRoomDetails(RoomDetails roomDetails)
        {
            try
            {
                Logger.Write("Start RoomDataManager UpdateRoomDetails", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPUPDATEROOMDETAILS);
                CommonDataAccess.AddInParameter(Constants.ID.ToMySqlParameter(), DbType.Int64, roomDetails.Id);
                CommonDataAccess.AddInParameter(Constants.ROOMITEMTYPEID.ToMySqlParameter(), DbType.Int64, roomDetails.RoomItemTypeId);
                CommonDataAccess.AddInParameter(Constants.BRAND.ToMySqlParameter(), DbType.String, roomDetails.Brand);
                CommonDataAccess.AddInParameter(Constants.YEAROFINSTALLED.ToMySqlParameter(), DbType.Int32, roomDetails.YearOfInstalled);
                CommonDataAccess.AddInParameter(Constants.QUANTITY.ToMySqlParameter(), DbType.Int32, roomDetails.Quantity);
                CommonDataAccess.AddInParameter(Constants.UPDATEDBY.ToMySqlParameter(), DbType.Int64, roomDetails.UpdatedBy);
                CommonDataAccess.AddInParameter(Constants.UPDATEDON.ToMySqlParameter(), DbType.DateTime, DateTime.Now.ObjectToDBDateTime());
                return CommonDataAccess.ExceuteNonQuery() > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                Logger.Write("End RoomDataManager UpdateRoomDetails", LogType.Information);
            }
        }

        /// <summary>
        /// update room item type.
        /// </summary>
        /// <param name="roomItemType">The room item type entity.</param>
        /// <returns>The boolean value.</returns>
        public bool UpdateRoomItemType(RoomItemType roomItemType)
        {
            try
            {
                Logger.Write("Start RoomDataManager UpdateRoomItemType", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPUPDATEROOMITEMTYPE);
                CommonDataAccess.AddInParameter(Constants.ID.ToMySqlParameter(), DbType.Int32, roomItemType.Id);
                CommonDataAccess.AddInParameter(Constants.ITEM.ToMySqlParameter(), DbType.String, roomItemType.Item);
                CommonDataAccess.AddInParameter(Constants.UPDATEDBY.ToMySqlParameter(), DbType.Int64, roomItemType.UpdatedBy);
                CommonDataAccess.AddInParameter(Constants.UPDATEDON.ToMySqlParameter(), DbType.DateTime, DateTime.Now.ObjectToDBDateTime());
                return CommonDataAccess.ExceuteNonQuery() > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                Logger.Write("End RoomDataManager UpdateRoomItemType", LogType.Information);
            }
        }

        /// <summary>
        /// update room type.
        /// </summary>
        /// <param name="roomType">The room item entity.</param>
        /// <returns>The boolean value.</returns>
        public bool UpdateRoomType(RoomType roomType)
        {
            try
            {
                Logger.Write("Start RoomDataManager UpdateRoomItemType", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPUPDATEROOMTYPE);
                CommonDataAccess.AddInParameter(Constants.ID.ToMySqlParameter(), DbType.Int32, roomType.Id);
                CommonDataAccess.AddInParameter(Constants.TYPE.ToMySqlParameter(), DbType.String, roomType.Type);
                CommonDataAccess.AddInParameter(Constants.UPDATEDBY.ToMySqlParameter(), DbType.Int64, roomType.UpdatedBy);
                CommonDataAccess.AddInParameter(Constants.UPDATEDON.ToMySqlParameter(), DbType.DateTime, DateTime.Now.ObjectToDBDateTime());
                return CommonDataAccess.ExceuteNonQuery() > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                Logger.Write("End RoomDataManager UpdateRoomItemType", LogType.Information);
            }
        }

        /// <summary>
        /// delete room.
        /// </summary>
        /// <param name="room">The room entity.</param>
        /// <returns>The boolean value.</returns>
        public bool DeleteRoom(Room room)
        {
            try
            {
                Logger.Write("Start RoomDataManager DeleteRoom", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPDELETEROOM);
                CommonDataAccess.AddInParameter(Constants.ID.ToMySqlParameter(), DbType.Int64, room.Id);
                CommonDataAccess.AddInParameter(Constants.UPDATEDBY.ToMySqlParameter(), DbType.Int64, room.UpdatedBy);
                CommonDataAccess.AddInParameter(Constants.UPDATEDON.ToMySqlParameter(), DbType.DateTime, DateTime.Now.ObjectToDBDateTime());
                return CommonDataAccess.ExceuteNonQuery() > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                Logger.Write("End RoomDataManager DeleteRoom", LogType.Information);
            }
        }

        /// <summary>
        /// delete room details.
        /// </summary>
        /// <param name="roomDetails">The room details entity.</param>
        /// <returns>The boolean value.</returns>
        public bool DeleteRoomDetails(RoomDetails roomDetails)
        {
            try
            {
                Logger.Write("Start RoomDataManager DeleteRoomDetails", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPDELETEROOMDETAILS);
                CommonDataAccess.AddInParameter(Constants.ID.ToMySqlParameter(), DbType.Int64, roomDetails.Id);
                CommonDataAccess.AddInParameter(Constants.UPDATEDBY.ToMySqlParameter(), DbType.Int64, roomDetails.UpdatedBy);
                CommonDataAccess.AddInParameter(Constants.UPDATEDON.ToMySqlParameter(), DbType.DateTime, DateTime.Now.ObjectToDBDateTime());
                return CommonDataAccess.ExceuteNonQuery() > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                Logger.Write("End RoomDataManager DeleteRoomDetails", LogType.Information);
            }
        }

        /// <summary>
        /// delete room type.
        /// </summary>
        /// <param name="roomType">The room type entity.</param>
        /// <returns>The boolean value.</returns>
        public bool DeleteRoomType(RoomType roomType)
        {
            try
            {
                Logger.Write("Start RoomDataManager DeleteRoomType", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPDELETEROOMTYPE);
                CommonDataAccess.AddInParameter(Constants.ID.ToMySqlParameter(), DbType.Int32, roomType.Id);
                CommonDataAccess.AddInParameter(Constants.UPDATEDBY.ToMySqlParameter(), DbType.Int64, roomType.UpdatedBy);
                CommonDataAccess.AddInParameter(Constants.UPDATEDON.ToMySqlParameter(), DbType.DateTime, DateTime.Now.ObjectToDBDateTime());
                return CommonDataAccess.ExceuteNonQuery() > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                Logger.Write("End RoomDataManager DeleteRoomType", LogType.Information);
            }
        }

        /// <summary>
        /// delete room item type.
        /// </summary>
        /// <param name="roomItemType">The room item type entity.</param>
        /// <returns>The boolean value.</returns>
        public bool DeleteRoomItemType(RoomItemType roomItemType)
        {
            try
            {
                Logger.Write("Start RoomDataManager DeleteRoomItemType", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPDELETEROOMITEMTYPE);
                CommonDataAccess.AddInParameter(Constants.ID.ToMySqlParameter(), DbType.Int32, roomItemType.Id);
                CommonDataAccess.AddInParameter(Constants.UPDATEDBY.ToMySqlParameter(), DbType.Int64, roomItemType.UpdatedBy);
                CommonDataAccess.AddInParameter(Constants.UPDATEDON.ToMySqlParameter(), DbType.DateTime, DateTime.Now.ObjectToDBDateTime());
                return CommonDataAccess.ExceuteNonQuery() > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                Logger.Write("End RoomDataManager DeleteRoomItemType", LogType.Information);
            }
        }
    }
}

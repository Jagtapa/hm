﻿// <copyright file="HomeDataManager.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2015 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>21-07-2015</date>
// <summary> This class is used to deal with HOME table operations.
// </summary>
namespace HM.Application.DataManagers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using HM.Entities;
    using HM.Framework;
    using HM.Framework.Application;

    /// <summary>
    /// This class is used to deal with HOME table operations.
    /// </summary>
    public class HomeDataManager
    {
        /// <summary>
        /// Get list of homes for user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns>The list of records</returns>
        public List<Home> GetHomes(long userId)
        {
            IDataReader dataReader = null;
            List<Home> homeList = new List<Home>();
            try
            {
                Logger.Write("Start HomeDataManager GetHomes", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPGETHOMES);
                CommonDataAccess.AddInParameter(Constants.USERID.ToMySqlParameter(), DbType.Int64, userId);
                dataReader = CommonDataAccess.ExecuteReader();
                Home home = null;
                while (dataReader.Read())
                {
                    home = new Home();
                    home.Id = dataReader[Constants.ID].ObjectToInt64();
                    home.HouseName = dataReader[Constants.HOUSENAME].ObjectToString();
                    home.HouseNo = dataReader[Constants.HOUSENO].ObjectToString();
                    home.Street = dataReader[Constants.STREET].ObjectToString();
                    home.Country = dataReader[Constants.COUNTRY].ObjectToString();
                    home.City = dataReader[Constants.CITY].ObjectToString();
                    home.Town = dataReader[Constants.TOWN].ObjectToString();
                    home.Locality = dataReader[Constants.LOCALITY].ObjectToString();
                    home.PostCode = dataReader[Constants.POSTCODE].ObjectToInt32();
                    home.CreatedBy = dataReader[Constants.CREATEDBY].ObjectToInt64();
                    home.UpdatedBy = dataReader[Constants.UPDATEDBY].ObjectToInt64();
                    home.CreatedOn = dataReader[Constants.CREATEDON].ObjectToDateTime();
                    home.UpdatedOn = dataReader[Constants.UPDATEDON].ObjectToDateTime();
                    homeList.Add(home);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dataReader.CloseDataReader();
                Logger.Write("End HomeDataManager GetHomes", LogType.Information);
            }

            return homeList;
        }

        /// <summary>
        /// create new home.
        /// </summary>
        /// <param name="filter">The Home entity.</param>
        /// <returns>The boolean value.</returns>
        public Home AddHome(Home home)
        {
            try
            {
                Logger.Write("Start HomeDataManager AddHome", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPADDHOME);
                CommonDataAccess.AddInParameter(Constants.USERID.ToMySqlParameter(), DbType.Int64, home.UserId);
                CommonDataAccess.AddInParameter(Constants.HOUSENAME.ToMySqlParameter(), DbType.String, home.HouseName);
                CommonDataAccess.AddInParameter(Constants.HOUSENO.ToMySqlParameter(), DbType.String, home.HouseNo);
                CommonDataAccess.AddInParameter(Constants.STREET.ToMySqlParameter(), DbType.String, home.Street);
                CommonDataAccess.AddInParameter(Constants.COUNTRY.ToMySqlParameter(), DbType.String, home.Country);
                CommonDataAccess.AddInParameter(Constants.CITY.ToMySqlParameter(), DbType.String, home.City);
                CommonDataAccess.AddInParameter(Constants.TOWN.ToMySqlParameter(), DbType.String, home.Town);
                CommonDataAccess.AddInParameter(Constants.LOCALITY.ToMySqlParameter(), DbType.String, home.Locality);
                CommonDataAccess.AddInParameter(Constants.POSTCODE.ToMySqlParameter(), DbType.Int32, home.PostCode);
                CommonDataAccess.AddInParameter(Constants.CREATEDON.ToMySqlParameter(), DbType.DateTime, DateTime.Now.ObjectToDBDateTime());
                CommonDataAccess.AddOutParameter(Constants.ID.ToMySqlParameter(), DbType.Int64, Int32.MaxValue, false, 0, 0);
                CommonDataAccess.ExceuteNonQuery();
                home.Id = CommonDataAccess.GetParameterValue(Constants.ID.ToMySqlParameter()).ObjectToInt64();
            }
            catch
            {
                throw;
            }
            finally
            {
                Logger.Write("End HomeDataManager AddHome", LogType.Information);
            }

            return home;
        }

        /// <summary>
        /// update homede tails.
        /// </summary>
        /// <param name="filter">The Home entity.</param>
        /// <returns>The boolean value.</returns>
        public bool UpdateHome(Home home)
        {
            try
            {
                Logger.Write("Start HomeDataManager UpdateHome", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPUPDATEHOME);
                CommonDataAccess.AddInParameter(Constants.ID.ToMySqlParameter(), DbType.Int32, home.Id);
                CommonDataAccess.AddInParameter(Constants.HOUSENAME.ToMySqlParameter(), DbType.String, home.HouseName);
                CommonDataAccess.AddInParameter(Constants.HOUSENO.ToMySqlParameter(), DbType.String, home.HouseNo);
                CommonDataAccess.AddInParameter(Constants.STREET.ToMySqlParameter(), DbType.String, home.Street);
                CommonDataAccess.AddInParameter(Constants.COUNTRY.ToMySqlParameter(), DbType.String, home.Country);
                CommonDataAccess.AddInParameter(Constants.CITY.ToMySqlParameter(), DbType.String, home.City);
                CommonDataAccess.AddInParameter(Constants.TOWN.ToMySqlParameter(), DbType.String, home.Town);
                CommonDataAccess.AddInParameter(Constants.LOCALITY.ToMySqlParameter(), DbType.String, home.Locality);
                CommonDataAccess.AddInParameter(Constants.POSTCODE.ToMySqlParameter(), DbType.Int32, home.PostCode);
                CommonDataAccess.AddInParameter(Constants.UPDATEDBY.ToMySqlParameter(), DbType.Int64, home.UpdatedBy);
                CommonDataAccess.AddInParameter(Constants.UPDATEDON.ToMySqlParameter(), DbType.DateTime, DateTime.Now.ObjectToDBDateTime());
                return CommonDataAccess.ExceuteNonQuery() > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                Logger.Write("End HomeDataManager UpdateHome", LogType.Information);
            }
        }

        /// <summary>
        /// delete homede tails.
        /// </summary>
        /// <param name="filter">The Home entity.</param>
        /// <returns>The boolean value.</returns>
        public bool DeleteHome(Home home)
        {
            try
            {
                Logger.Write("Start HomeDataManager DeleteHome", LogType.Information);
                CommonDataAccess.CreateStoredProcCommandWrapper(Constants.SPDELETEHOME);
                CommonDataAccess.AddInParameter(Constants.ID.ToMySqlParameter(), DbType.Int32, home.Id);
                CommonDataAccess.AddInParameter(Constants.UPDATEDBY.ToMySqlParameter(), DbType.Int64, home.UpdatedBy);
                CommonDataAccess.AddInParameter(Constants.UPDATEDON.ToMySqlParameter(), DbType.DateTime, DateTime.Now.ObjectToDBDateTime());
                return CommonDataAccess.ExceuteNonQuery() > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                Logger.Write("End HomeDataManager DeleteHome", LogType.Information);
            }
        }
    }
}

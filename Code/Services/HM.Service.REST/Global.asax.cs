﻿// <copyright file="Global.asax.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2015 All Right Reserved
// </copyright>
// <author>Sandip Salunkhe</author>
// <email></email>
// <date>05-04-2015</date>
// <summary> This class is used to handle application level events.
// </summary>

namespace HM.Service.REST
{
    using System.Web.Http;

    /// <summary>
    /// This class is used to handle application level events.
    /// </summary>
    public class WebApiApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// The application start event.
        /// </summary>
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}

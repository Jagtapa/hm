﻿// <copyright file="DocumentController.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2015 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>14-05-2015</date>
// <summary> This class is used to perform document related operations.
// </summary>
namespace HM.Service.REST
{
    using HM.Application.UserManagement.BusinessManagers;
    using HM.Entities.UserManagement;
    using System.IO;
    using System.Web.Http;

    /// <summary>
    /// This class is used to perform user related operations.
    /// </summary>
    public class DocumentController : ApiController
    {
        /// <summary>
        /// Get the user details from emailid.
        /// </summary>
        [HttpPost]
        [ActionName("postdocument")]
        public void PostDocument(object fileStream)
        {
            int i = 0;
            i += 1;
            var item = fileStream.ToString();
        }
    }
}
﻿// <copyright file="UserController.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2015 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>14-05-2015</date>
// <summary> This class is used to perform user related operations.
// </summary>
namespace HM.Service.REST
{
    using HM.Application.UserManagement.BusinessManagers;
    using HM.Entities.UserManagement;
    using System.Web.Http;

    /// <summary>
    /// This class is used to perform user related operations.
    /// </summary>
    public class UserController : ApiController
    {
        /// <summary>
        /// Get the user details from emailid.
        /// </summary>
        /// <returns>The user details.</returns>
        [HttpPost]
        [ActionName("authenticate")]
        public UserEntity Authenticate([FromBody]UserEntity userEntity)
        {
            UserBusinessManager userBusinessManager = new UserBusinessManager();
            return userBusinessManager.Authenticate(userEntity);
        }

        /// <summary>
        /// Get the user details from emailid.
        /// </summary>
        /// <returns>The user details.</returns>
        [HttpPost]
        [ActionName("postuserdetails")]
        public UserEntity GetUserDetails([FromBody]UserEntity userEntity)
        {
            UserBusinessManager userBusinessManager = new UserBusinessManager();
            return userBusinessManager.GetDetails(userEntity);
        }

        /// <summary>
        /// Register user with first,last name and emailid. 
        /// </summary>
        /// <param name="userEntity">The User entity.</param>
        /// <returns>Returns user identity.</returns>
        [ActionName("postregisteruser")]
        public UserEntity PostRegisterUser([FromBody]UserEntity userEntity)
        {
            UserBusinessManager userBusinessManager = new UserBusinessManager();
            userEntity = userBusinessManager.RegisterUser(userEntity);
            return userEntity;
        }

        /// <summary>
        /// update user details. 
        /// </summary>
        /// <param name="userEntity">The User entity.</param>
        /// <returns>The boolean value.</returns>
        [ActionName("postupdateuser")]
        public bool PostUpdateUser([FromBody]UserEntity userEntity)
        {
            UserBusinessManager userBusinessManager = new UserBusinessManager();
            return userBusinessManager.UpdateUser(userEntity);
        }

        /// <summary>
        /// Delete user details. 
        /// </summary>
        /// <param name="userEntity">The User entity.</param>
        /// <returns>The boolean value.</returns>
        [ActionName("postdeleteuser")]
        public bool PostDeleteUser([FromBody]UserEntity userEntity)
        {
            UserBusinessManager userBusinessManager = new UserBusinessManager();
            return userBusinessManager.DeleteUser(userEntity);
        }

        /// <summary>
        /// Get the user details from emailid.
        /// </summary>
        /// <returns>The user details.</returns>
        [HttpPost]
        [ActionName("forgotpassword")]
        public object ForgotPassword([FromBody]UserEntity userEntity)
        {
            UserBusinessManager userBusinessManager = new UserBusinessManager();
            UserEntity user = userBusinessManager.ForgotPassword(userEntity);
            var result = new { errorMessage = "Invalid user" };

            if (user.IsValid)
            {
                result = new { errorMessage = "Please check your email" };
            }
            return result;
        }
    }
}
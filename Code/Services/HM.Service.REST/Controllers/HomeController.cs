﻿// <copyright file="HomeController.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2015 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>21-07-2015</date>
// <summary> This class is used to perform home related operations.
// </summary>
namespace HM.Service.REST
{
    using HM.Application.BusinessManagers;
    using HM.Application.UserManagement.BusinessManagers;
    using HM.Entities;
    using HM.Entities.UserManagement;
    using System.Collections.Generic;
    using System.Web.Http;

    /// <summary>
    /// This class is used to perform home related operations.
    /// </summary>
    public class HomeController : ApiController
    {
        /// <summary>
        /// Get list of homes for user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns>The list of homes.</returns>
        [HttpGet]
        public List<Home> GetHomes(long userId)
        {
            HomeBusinessManager homeBusinessManager = new HomeBusinessManager();
            List<Home> homeList = homeBusinessManager.GetHomes(userId);
            return homeList;
        }

        /// <summary>
        /// Create new home. 
        /// </summary>
        /// <param name="home">The Home entity.</param>
        /// <returns>Returns user home id.</returns>
        [ActionName("postaddhome")]
        public long PostAddHome([FromBody]Home home)
        {
            HomeBusinessManager homeBusinessManager = new HomeBusinessManager();
            home = homeBusinessManager.AddHome(home);
            return home.Id;
        }

        /// <summary>
        /// Update home details. 
        /// </summary>
        /// <param name="home">The Home entity.</param>
        /// <returns>Returns boolean value.</returns>
        [ActionName("postupdatehome")]
        public bool PostUpdateHome([FromBody]Home home)
        {
            HomeBusinessManager homeBusinessManager = new HomeBusinessManager();
            return homeBusinessManager.UpdateHome(home);
        }

        /// <summary>
        /// Delete home details. 
        /// </summary>
        /// <param name="home">The Home entity.</param>
        /// <returns>Returns boolean value.</returns>
        [ActionName("postdeletehome")]
        public bool PostDeleteHome([FromBody]Home home)
        {
            HomeBusinessManager homeBusinessManager = new HomeBusinessManager();
            return homeBusinessManager.DeleteHome(home);
        }
    }
}
﻿// <copyright file="RoomController.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2015 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>23-07-2015</date>
// <summary> This class is used to perform room related operations.
// </summary>
namespace HM.Service.REST
{
    using HM.Application.BusinessManagers;
    using HM.Application.UserManagement.BusinessManagers;
    using HM.Entities;
    using HM.Entities.UserManagement;
    using System.Collections.Generic;
    using System.Web.Http;
    using System.Linq;

    /// <summary>
    /// This class is used to perform room related operations.
    /// </summary>
    public class RoomController : ApiController
    {
        /// <summary>
        /// Get list of rooms for home.
        /// </summary>
        /// <param name="homeId">The home id.</param>
        /// <returns>The list of records</returns>
        public IEnumerable<object> GetRooms(long homeId)
        {
            RoomBusinessManager roomBusinessManager = new RoomBusinessManager();
            List<Room> roomList = roomBusinessManager.GetRooms(homeId);
            var result = from item in roomList select new { Id = item.Id, Description = item.Description };
            return result;
        }

        /// <summary>
        /// Get list of room item details.
        /// </summary>
        /// <param name="roomId">The room id.</param>
        /// <returns>The list of records</returns>
        public List<RoomDetails> GetRoomDetails(long roomId)
        {
            RoomBusinessManager roomBusinessManager = new RoomBusinessManager();
            List<RoomDetails> roomDetailsList = roomBusinessManager.GetRoomDetails(roomId);
            return roomDetailsList;
        }

        /// <summary>
        /// Get list of room types.
        /// </summary>
        /// <returns>The list of records</returns>
        public List<RoomType> GetRoomTypes()
        {
            RoomBusinessManager roomBusinessManager = new RoomBusinessManager();
            List<RoomType> roomDetailsList = roomBusinessManager.GetRoomTypes();
            return roomDetailsList;
        }

        /// <summary>
        /// Get list of room item types.
        /// </summary>
        /// <returns>The list of records</returns>
        public List<RoomItemType> GetRoomItemTypes()
        {
            RoomBusinessManager roomBusinessManager = new RoomBusinessManager();
            List<RoomItemType> roomItemTypeList = roomBusinessManager.GetRoomItemTypes();
            return roomItemTypeList;
        }

        /// <summary>
        /// create new room.
        /// </summary>
        /// <param name="room">The room entity.</param>
        /// <returns>The room id.</returns>
        [ActionName("postaddroom")]
        public object PostAddRoom([FromBody]Room room)
        {
            RoomBusinessManager roomBusinessManager = new RoomBusinessManager();
            room = roomBusinessManager.AddRoom(room);
            var result = new { Success = false, Message = "Server Error", Id = room.Id };

            if (room.Id != 0)
            {
                result = new { Success = true, Message = "Record added successfully", Id = room.Id };
            }
            return result;
        }

        /// <summary>
        /// add room details.
        /// </summary>
        /// <param name="roomDetails">The room details entity.</param>
        /// <returns>The room details id.</returns>
        [ActionName("postaddroomdetails")]
        public object PostAddRoomDetails([FromBody]RoomDetails roomDetails)
        {
            RoomBusinessManager roomBusinessManager = new RoomBusinessManager();
            roomDetails = roomBusinessManager.AddRoomDetails(roomDetails);
            var result = new { Success = false, Message = "Server Error", Id = roomDetails.Id };

            if (roomDetails.Id != 0)
            {
                result = new { Success = true, Message = "Record added successfully", Id = roomDetails.Id };
            }
            return result;
            //return roomDetails.Id;
        }

        /// <summary>
        /// add room item type.
        /// </summary>
        /// <param name="roomItemType">The room item type entity.</param>
        /// <returns>The room item type id.</returns>
        [ActionName("postaddroomitemtype")]
        public long PostAddRoomItemType([FromBody]RoomItemType roomItemType)
        {
            RoomBusinessManager roomBusinessManager = new RoomBusinessManager();
            roomItemType = roomBusinessManager.AddRoomItemType(roomItemType);
            return roomItemType.Id;
        }

        /// <summary>
        /// add room type.
        /// </summary>
        /// <param name="roomType">The room item entity.</param>
        /// <returns>The room type id.</returns>
        [ActionName("postaddroomtype")]
        public long PostAddRoomType([FromBody]RoomType roomType)
        {
            RoomBusinessManager roomBusinessManager = new RoomBusinessManager();
            roomType = roomBusinessManager.AddRoomType(roomType);
            return roomType.Id;
        }

        /// <summary>
        /// update room.
        /// </summary>
        /// <param name="room">The room entity.</param>
        /// <returns>The boolean value.</returns>
        [ActionName("postupdateroom")]
        public object PostUpdateRoom([FromBody]Room room)
        {
            RoomBusinessManager roomBusinessManager = new RoomBusinessManager();
            bool updateResult = roomBusinessManager.UpdateRoom(room);
            var result = new { Success = false, Message = "Server Error" };

            if (updateResult)
            {
                result = new { Success = true, Message = "Record updated successfully" };
            }
            return result;
        }

        /// <summary>
        /// update room details.
        /// </summary>
        /// <param name="roomDetails">The room details entity.</param>
        /// <returns>The boolean value.</returns>
        [ActionName("postupdateroomdetails")]
        public object PostUpdateRoomDetails([FromBody]RoomDetails roomDetails)
        {
            RoomBusinessManager roomBusinessManager = new RoomBusinessManager();
            bool updateResult = roomBusinessManager.UpdateRoomDetails(roomDetails);
            var result = new { Success = false, Message = "Server Error" };

            if (updateResult)
            {
                result = new { Success = true, Message = "Record updated successfully" };
            }
            return result;
        }

        /// <summary>
        /// update room item type.
        /// </summary>
        /// <param name="roomItemType">The room item type entity.</param>
        /// <returns>The boolean value.</returns>
        [ActionName("postupdateroomitemtype")]
        public bool PostUpdateRoomItemType([FromBody]RoomItemType roomItemType)
        {
            RoomBusinessManager roomBusinessManager = new RoomBusinessManager();
            return roomBusinessManager.UpdateRoomItemType(roomItemType);
        }

        /// <summary>
        /// update room type.
        /// </summary>
        /// <param name="roomType">The room type entity.</param>
        /// <returns>The boolean value.</returns>
        [ActionName("postupdateroomtype")]
        public bool PostUpdateRoomType([FromBody]RoomType roomType)
        {
            RoomBusinessManager roomBusinessManager = new RoomBusinessManager();
            return roomBusinessManager.UpdateRoomType(roomType);
        }

        /// <summary>
        /// Delete Room. 
        /// </summary>
        /// <param name="room">The Room entity.</param>
        /// <returns>Returns boolean value.</returns>
        [ActionName("postdeleteroom")]
        public object PostDeleteRoom([FromBody]Room room)
        {
            RoomBusinessManager roomBusinessManager = new RoomBusinessManager();
            bool deleteResult = roomBusinessManager.DeleteRoom(room);
            var result = new { Success = false, Message = "Server Error" };

            if (deleteResult)
            {
                result = new { Success = true, Message = "Record deleted successfully" };
            }
            return result;
        }

        /// <summary>
        /// Delete Room details. 
        /// </summary>
        /// <param name="roomDetails">The Room details entity.</param>
        /// <returns>Returns boolean value.</returns>
        [ActionName("postdeleteroomdetails")]
        public object PostDeleteRoomDetails([FromBody]RoomDetails roomDetails)
        {
            RoomBusinessManager roomBusinessManager = new RoomBusinessManager();
            bool deleteResult = roomBusinessManager.DeleteRoomDetails(roomDetails);
            var result = new { Success = false, Message = "Server Error" };

            if (deleteResult)
            {
                result = new { Success = true, Message = "Record deleted successfully" };
            }
            return result;
        }

        /// <summary>
        /// Delete Room item type. 
        /// </summary>
        /// <param name="roomType">The Room item type entity.</param>
        /// <returns>Returns boolean value.</returns>
        [ActionName("postdeleteroomitemtype")]
        public bool PostDeleteRoomItemType([FromBody]RoomItemType roomItemType)
        {
            RoomBusinessManager roomBusinessManager = new RoomBusinessManager();
            return roomBusinessManager.DeleteRoomItemType(roomItemType);
        }

        /// <summary>
        /// Delete Room type. 
        /// </summary>
        /// <param name="roomType">The Room type entity.</param>
        /// <returns>Returns boolean value.</returns>
        [ActionName("postdeleteroomtype")]
        public bool PostDeleteRoomType([FromBody]RoomType roomType)
        {
            RoomBusinessManager roomBusinessManager = new RoomBusinessManager();
            return roomBusinessManager.DeleteRoomType(roomType);
        }
    }
}
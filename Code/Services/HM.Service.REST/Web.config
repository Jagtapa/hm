﻿<?xml version="1.0"?>
<!--
  For more information on how to configure your ASP.NET application, please visit
  http://go.microsoft.com/fwlink/?LinkId=301879
  -->
<configuration>
  <configSections>
    <section name="loggingConfiguration" type="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.LoggingSettings, Microsoft.Practices.EnterpriseLibrary.Logging, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" requirePermission="true"/>
    <section name="exceptionHandling" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration.ExceptionHandlingSettings, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" requirePermission="true"/>
    <section name="dataConfiguration" type="Microsoft.Practices.EnterpriseLibrary.Data.Configuration.DatabaseSettings, Microsoft.Practices.EnterpriseLibrary.Data, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" requirePermission="true"/>
    <section name="cachingConfiguration" type="Microsoft.Practices.EnterpriseLibrary.Caching.Configuration.CacheManagerSettings, Microsoft.Practices.EnterpriseLibrary.Caching, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" requirePermission="true"/>
  </configSections>
  <loggingConfiguration name="" tracingEnabled="true" defaultCategory="ErrorCategory">
    <listeners>
      <add name="Info Listener" type="Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners.RollingFlatFileTraceListener, Microsoft.Practices.EnterpriseLibrary.Logging" listenerDataType="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.RollingFlatFileTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging" fileName="C:\Sigmo\RestInfo.txt" formatter="Info Formatter" rollFileExistsBehavior="Increment" rollInterval="Midnight" rollSizeKB="20480" traceOutputOptions="DateTime, Timestamp"/>
      <add name="Error Listener" type="Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners.RollingFlatFileTraceListener, Microsoft.Practices.EnterpriseLibrary.Logging" listenerDataType="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.RollingFlatFileTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging" fileName="C:\Sigmo\RestError.txt" formatter="Error Formatter" rollFileExistsBehavior="Increment" rollInterval="Midnight" rollSizeKB="20480" traceOutputOptions="DateTime, Timestamp"/>
    </listeners>
    <formatters>
      <add name="Info Formatter" type="Microsoft.Practices.EnterpriseLibrary.Logging.Formatters.TextFormatter, Microsoft.Practices.EnterpriseLibrary.Logging" template="Timestamp: {timestamp(local)}{newline} Message: {message}{newline}"/>
      <add name="Error Formatter" type="Microsoft.Practices.EnterpriseLibrary.Logging.Formatters.TextFormatter, Microsoft.Practices.EnterpriseLibrary.Logging" template="Timestamp: {timestamp(local)}{newline} Message: {message}{newline}"/>
    </formatters>
    <categorySources>
      <add switchValue="All" name="InformationCategory">
        <listeners>
          <add name="Info Listener"/>
        </listeners>
      </add>
      <add switchValue="All" name="ErrorCategory">
        <listeners>
          <add name="Error Listener"/>
        </listeners>
      </add>
      <add switchValue="All" name="WarningCategory">
        <listeners>
          <add name="Info Listener"/>
        </listeners>
      </add>
    </categorySources>
    <specialSources>
      <allEvents switchValue="All" name="All Events"/>
      <notProcessed switchValue="All" name="Unprocessed Category"/>
      <errors switchValue="All" name="Logging Errors &amp; Warnings">
        <listeners>
          <add name="Error Listener"/>
        </listeners>
      </errors>
    </specialSources>
  </loggingConfiguration>
  <exceptionHandling>
    <exceptionPolicies>
      <add name="Business Policy">
        <exceptionTypes>
          <add name="All Exceptions" type="System.Exception, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" postHandlingAction="None">
            <exceptionHandlers>
              <add name="Logging Exception Handler" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging.LoggingExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" logCategory="ErrorCategory" eventId="100" severity="Error" title="Enterprise Library Exception Handling" formatterType="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.TextExceptionFormatter, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling" priority="0"/>
            </exceptionHandlers>
          </add>
        </exceptionTypes>
      </add>
      <add name="UI Policy">
        <exceptionTypes>
          <add name="All Exceptions" type="System.Exception, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" postHandlingAction="None"/>
        </exceptionTypes>
      </add>
      <add name="Resume policy">
        <exceptionTypes>
          <add name="All Exceptions" type="System.Exception, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" postHandlingAction="None">
            <exceptionHandlers>
              <add name="Logging Exception Handler" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging.LoggingExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" logCategory="ErrorCategory" eventId="100" severity="Error" title="Enterprise Library Exception Handling" formatterType="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.TextExceptionFormatter, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling" priority="0"/>
            </exceptionHandlers>
          </add>
        </exceptionTypes>
      </add>
    </exceptionPolicies>
  </exceptionHandling>
  <cachingConfiguration defaultCacheManager="ShortInMemoryPersistence">
    <cacheManagers>
      <add name="ShortInMemoryPersistence" type="Microsoft.Practices.EnterpriseLibrary.Caching.CacheManager, Microsoft.Practices.EnterpriseLibrary.Caching" expirationPollFrequencyInSeconds="1" maximumElementsInCacheBeforeScavenging="10" numberToRemoveWhenScavenging="2" backingStoreName="inMemory"/>
    </cacheManagers>
    <backingStores>
      <add type="Microsoft.Practices.EnterpriseLibrary.Caching.BackingStoreImplementations.NullBackingStore, Microsoft.Practices.EnterpriseLibrary.Caching" name="inMemory"/>
    </backingStores>
  </cachingConfiguration>
  <dataConfiguration defaultDatabase="HM">
    <providerMappings>
      <add databaseType="EntLibContrib.Data.MySql.MySqlDatabase, EntLibContrib.Data.MySql" name="MySql.Data.MySqlClient"/>
    </providerMappings>
  </dataConfiguration>
  <connectionStrings>
    <add name="HM" connectionString="Server=morya;Database=hm;IntegratedSecurity=false;Uid=JagtapA;Password=quantum@123;Allow User Variables=True;" providerName="MySql.Data.MySqlClient" />
    <!--<add name="HM" connectionString="Server=sandippc;Database=hm;IntegratedSecurity=false;Uid=salunkhes;Password=Sandy@123;Allow User Variables=True;" providerName="MySql.Data.MySqlClient" />-->
    <!--<add name="HM" connectionString="Server=104.155.205.157;Database=hm;IntegratedSecurity=false;Uid=jagtapa;Password=Quantum@123;Allow User Variables=True;" providerName="MySql.Data.MySqlClient"/>-->
  </connectionStrings>
  <appSettings>
    <add key="CommandTimeout" value="1200"/>
    <add key="SmtpServer" value="192.168.0.2"/>
    <add key="FROMEMAIL" value="SYSTST@quantumsystems.co.uk"/>
    <add key="ADMINISTRATOREMAIL" value="anand.jagtap@quantumsystems.co.uk"/>
    <add key="LogEnabled" value="true"/>
    <!--PROXYTIMEOUT in Mili seconds End-->
    <add key="PROXYTIMEOUT" value="1800000"/>
    <!--REQUIREDTANSTIMEOUT in Minuts End-->
    <add key="REQUIREDTANSTIMEOUT" value="120"/>
    <!--SUPPORTEDTANSTIMEOUT in Minuts End-->
    <add key="SUPPORTEDTANSTIMEOUT" value="120"/>
    <!--Send Exception mail details Start-->
    <add key="EXCEPTIONMAILSUBJECT" value="Exception occured. Please investigate."/>
    <add key="EXCEPTIONMAILFROM" value="anand.jagtap@quantumsystems.co.uk"/>
    <add key="EXCEPTIONMAILTO" value="anand.jagtap@quantumsystems.co.uk"/>
    <add key="EXCEPTIONMAILCC" value="anand.jagtap@quantumsystems.co.uk"/>
    <add key="EXCEPTIONMAILBCC" value="anand.jagtap@quantumsystems.co.uk"/>
    <add key="EXCEPTIONMAILSMTPSERVER" value="QEDSVR04"/>
    <add key="DATETIMEFORMATINFILE" value="yyyy/MM/dd HH:mm:ss"/>
    <add key="DATEFORMATINFILENAME" value="yyyyMMdd"/>
    <add key="ClientSettingsProvider.ServiceUri" value=""/>
  </appSettings>
  <!--
    For a description of web.config changes see http://go.microsoft.com/fwlink/?LinkId=235367.

    The following attributes can be set on the <httpRuntime> tag.
      <system.Web>
        <httpRuntime targetFramework="4.5" />
      </system.Web>
  -->
  <system.web>
    <compilation debug="true" targetFramework="4.5"/>
    <httpRuntime/>
    <pages controlRenderingCompatibilityVersion="4.0"/>
  </system.web>
   <system.data>
    <DbProviderFactories>
      <clear />
      <remove invariant="MySql.Data.MySqlClient" />
      <add name="MySql.Data.MySqlClient" description="ADO.Net driver for MySQL" invariant="MySql.Data.MySqlClient" type="EntLibContrib.Data.MySql.MySqlDatabase, EntLibContrib.Data.MySql" />
    </DbProviderFactories>
  </system.data>
  <system.webServer>
    <httpProtocol>
      <customHeaders>
        <!-- Adding the following custom HttpHeader will help prevent CORS from stopping the Request-->
        <!--<add name="Access-Control-Allow-Origin" value="*" />
        <add name="Access-Control-Allow-Methods" value="GET, PUT, POST, DELETE, OPTIONS" />
        <add name="Access-Control-Allow-Headers" value="Origin, Accept, Content-Type,Content-Range, Content-Disposition, Content-Description,Origin, X-Requested-With" />
        <add name="Access" value="application/json" />-->
      </customHeaders>
    </httpProtocol>
    <handlers>
      <remove name="ExtensionlessUrlHandler-Integrated-4.0"/>
      <remove name="OPTIONSVerbHandler"/>
      <remove name="TRACEVerbHandler"/>
      <add name="ExtensionlessUrlHandler-Integrated-4.0" path="*." verb="*" type="System.Web.Handlers.TransferRequestHandler" preCondition="integratedMode,runtimeVersionv4.0"/>
    </handlers>
  </system.webServer>
  <system.transactions>
    <defaultSettings timeout="01:00:00"/>
  </system.transactions>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <!--Web API-->
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Helpers" publicKeyToken="31bf3856ad364e35"/>
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35"/>
        <bindingRedirect oldVersion="1.0.0.0-5.1.0.0" newVersion="5.1.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Optimization" publicKeyToken="31bf3856ad364e35"/>
        <bindingRedirect oldVersion="1.0.0.0-1.1.0.0" newVersion="1.1.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35"/>
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="WebGrease" publicKeyToken="31bf3856ad364e35"/>
        <bindingRedirect oldVersion="1.0.0.0-1.5.2.14234" newVersion="1.5.2.14234"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Http" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Net.Http.Formatting" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-6.0.0.0" newVersion="6.0.0.0"/>
      </dependentAssembly>
      <!--My SQL-->
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Practices.EnterpriseLibrary.Common" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.0.505.0" newVersion="5.0.505.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Practices.EnterpriseLibrary.Data" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.0.505.0" newVersion="5.0.505.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Practices.Unity" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-2.1.505.0" newVersion="2.1.505.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Practices.Unity.Interception" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-2.1.505.0" newVersion="2.1.505.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Practices.Unity.Configuration" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-2.1.505.0" newVersion="2.1.505.0"/>
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
</configuration>
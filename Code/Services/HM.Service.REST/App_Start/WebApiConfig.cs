﻿// <copyright file="WebApiConfig.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2015 All Right Reserved
// </copyright>
// <author>Sandip Salunkhe</author>
// <email></email>
// <date>05-04-2015</date>
// <summary> This class is used configure web API's.
// </summary>
namespace HM.Service.REST
{
    using System.Net.Http.Headers;
    using System.Web.Http;
    using System.Web.Http.Cors;

    /// <summary>
    /// This class is used configure web API's.
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Configure the http request routing.
        /// </summary>
        /// <param name="config">The http configuration element.</param>
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var enableCorsAttribute = new EnableCorsAttribute("*",
                                               "Origin, Accept, Content-Type,Content-Range, Content-Disposition, Content-Description,Origin, X-Requested-With",
                                               "GET, PUT, POST, DELETE, OPTIONS");
            config.EnableCors(enableCorsAttribute);
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
               name: "HMAction",
               routeTemplate: "hm/{controller}/{action}/{id}",
               defaults: new { id = RouteParameter.Optional });

            config.Routes.MapHttpRoute(
                name: "HM",
                routeTemplate: "hm/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }
    }
}

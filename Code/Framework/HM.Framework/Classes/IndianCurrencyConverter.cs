﻿// <copyright file="IndianCurrencyConverter.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>29-08-2013</date>
// <summary>The convertor used to convert from digit to words in indian currency.
// </summary>

namespace HM.Framework
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// The convertor used to convert from digit to words in indian currency.
    /// </summary>
    public class IndianCurrencyConverter
    {
        /// <summary>
        /// The htPunctuation dictionary.
        /// </summary>
        private Hashtable hashTablePunctuation;

        /// <summary>
        /// The listStaticSuffix dictionary.
        /// </summary>
        private List<DictionaryEntry> listStaticSuffix;

        /// <summary>
        /// The listStaticPrefix dictionary.
        /// </summary>
        private List<DictionaryEntry> listStaticPrefix;

        /// <summary>
        /// The listHelpNotation dictionary.
        /// </summary>
        private List<DictionaryEntry> listHelpNotation;

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the IndianCurrencyConverter class.
        /// </summary>
        public IndianCurrencyConverter()
        {
            this.hashTablePunctuation = new Hashtable();
            this.listStaticPrefix = new List<DictionaryEntry>();
            this.listStaticSuffix = new List<DictionaryEntry>();
            this.listHelpNotation = new List<DictionaryEntry>();
            this.LoadStaticPrefix();
            this.LoadStaticSuffix();
            this.LoadHelpofNotation();
        }
        #endregion

        #region public methods
        /// <summary>
        /// The conversion of string numeric value to words.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <returns>The word representation of number.</returns>
        public string ConvertToWord(string value)
        {
            string convertedString = string.Empty;
            if (!(value.ToString().Length > 40))
            {
                if (this.IsNumeric(value.ToString()))
                {
                    try
                    {
                        string strValue = this.Reverse(value);
                        switch (strValue.Length)
                        {
                            case 1:
                                if (int.Parse(strValue.ToString()) > 0)
                                {
                                    convertedString = this.GetWordConversion(value);
                                }
                                else
                                {
                                    convertedString = "Zero ";
                                }

                                break;
                            case 2:
                                convertedString = this.GetWordConversion(value);
                                break;
                            default:
                                this.InsertToPunctuationTable(strValue);
                                this.ReverseHashTable();
                                convertedString = this.ReturnHashtableValue();
                                break;
                        }
                    }
                    catch (Exception exception)
                    {
                        convertedString = "Unexpected Error Occured";
                        convertedString += exception.Message;
                    }
                }
                else
                {
                    convertedString = "Please Enter Numbers Only, Decimal Values Are not supported";
                }
            }
            else
            {
                convertedString = "Please Enter Value in Less Then or Equal to 40 Digit";
            }

            return convertedString;
        }
        #endregion

        #region private methods
        /// <summary>
        /// Validate the string is number.
        /// </summary>
        /// <param name="valueInNumeric">The value to validate.</param>
        /// <returns>true or false.</returns>
        private bool IsNumeric(string valueInNumeric)
        {
            bool isFine = true;
            foreach (char ch in valueInNumeric)
            {
                if (!(ch >= '0' && ch <= '9'))
                {
                    isFine = false;
                }
            }

            return isFine;
        }

        /// <summary>
        /// get the panctuation value.
        /// </summary>
        /// <returns>The panctuation value.</returns>
        private string ReturnHashtableValue()
        {
            string strFinalString = string.Empty;
            for (int i = this.hashTablePunctuation.Count; i > 0; i--)
            {
                if (this.GetWordConversion(this.hashTablePunctuation[i].ToString()) != string.Empty)
                {
                    strFinalString = strFinalString + this.GetWordConversion(this.hashTablePunctuation[i].ToString()) + this.StaticPrefixFind(i.ToString());
                }
            }

            return strFinalString;
        }

        /// <summary>
        /// Fill static suffix dictionary.
        /// </summary>
        private void LoadStaticSuffix()
        {
            this.listStaticSuffix.Add(new DictionaryEntry(1, "One "));
            this.listStaticSuffix.Add(new DictionaryEntry(2, "Two "));
            this.listStaticSuffix.Add(new DictionaryEntry(3, "Three "));
            this.listStaticSuffix.Add(new DictionaryEntry(4, "Four "));
            this.listStaticSuffix.Add(new DictionaryEntry(5, "Five "));
            this.listStaticSuffix.Add(new DictionaryEntry(6, "Six "));
            this.listStaticSuffix.Add(new DictionaryEntry(7, "Seven "));
            this.listStaticSuffix.Add(new DictionaryEntry(8, "Eight "));
            this.listStaticSuffix.Add(new DictionaryEntry(9, "Nine "));
            this.listStaticSuffix.Add(new DictionaryEntry(10, "Ten "));
            this.listStaticSuffix.Add(new DictionaryEntry(11, "Eleven "));
            this.listStaticSuffix.Add(new DictionaryEntry(12, "Twelve "));
            this.listStaticSuffix.Add(new DictionaryEntry(13, "Thirteen "));
            this.listStaticSuffix.Add(new DictionaryEntry(14, "Fourteen "));
            this.listStaticSuffix.Add(new DictionaryEntry(15, "Fifteen "));
            this.listStaticSuffix.Add(new DictionaryEntry(16, "Sixteen "));
            this.listStaticSuffix.Add(new DictionaryEntry(17, "Seventeen "));
            this.listStaticSuffix.Add(new DictionaryEntry(18, "Eighteen "));
            this.listStaticSuffix.Add(new DictionaryEntry(19, "Nineteen "));
            this.listStaticSuffix.Add(new DictionaryEntry(20, "Twenty "));
            this.listStaticSuffix.Add(new DictionaryEntry(30, "Thirty "));
            this.listStaticSuffix.Add(new DictionaryEntry(40, "Fourty "));
            this.listStaticSuffix.Add(new DictionaryEntry(50, "Fifty "));
            this.listStaticSuffix.Add(new DictionaryEntry(60, "Sixty "));
            this.listStaticSuffix.Add(new DictionaryEntry(70, "Seventy "));
            this.listStaticSuffix.Add(new DictionaryEntry(80, "Eighty "));
            this.listStaticSuffix.Add(new DictionaryEntry(90, "Ninty "));
        }

        /// <summary>
        /// Fill static prffix dictionary.
        /// </summary>
        private void LoadStaticPrefix()
        {
            this.listStaticPrefix.Add(new DictionaryEntry(2, "Thousand "));
            this.listStaticPrefix.Add(new DictionaryEntry(3, "Lac "));
            this.listStaticPrefix.Add(new DictionaryEntry(4, "Crore "));
            this.listStaticPrefix.Add(new DictionaryEntry(5, "Arab "));
            this.listStaticPrefix.Add(new DictionaryEntry(6, "Kharab "));
            this.listStaticPrefix.Add(new DictionaryEntry(7, "Neel "));
            this.listStaticPrefix.Add(new DictionaryEntry(8, "Padma "));
            this.listStaticPrefix.Add(new DictionaryEntry(9, "Shankh "));
            this.listStaticPrefix.Add(new DictionaryEntry(10, "Maha-shankh "));
            this.listStaticPrefix.Add(new DictionaryEntry(11, "Ank "));
            this.listStaticPrefix.Add(new DictionaryEntry(12, "Jald "));
            this.listStaticPrefix.Add(new DictionaryEntry(13, "Madh "));
            this.listStaticPrefix.Add(new DictionaryEntry(14, "Paraardha "));
            this.listStaticPrefix.Add(new DictionaryEntry(15, "Ant "));
            this.listStaticPrefix.Add(new DictionaryEntry(16, "Maha-ant "));
            this.listStaticPrefix.Add(new DictionaryEntry(17, "Shisht "));
            this.listStaticPrefix.Add(new DictionaryEntry(18, "Singhar "));
            this.listStaticPrefix.Add(new DictionaryEntry(19, "Maha-singhar "));
            this.listStaticPrefix.Add(new DictionaryEntry(20, "Adant-singhar "));
        }

        /// <summary>
        /// Fill help notation dictionary.
        /// </summary>
        private void LoadHelpofNotation()
        {
            this.listHelpNotation.Add(new DictionaryEntry(2, "=1,000 (3 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(3, "=1,00,000 (5 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(4, "=1,00,00,000 (7 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(5, "=1,00,00,00,000 (9 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(6, "=1,00,00,00,00,000 (11 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(7, "=1,00,00,00,00,00,000 (13 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(8, "=1,00,00,00,00,00,00,000 (15 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(9, "=1,00,00,00,00,00,00,00,000 (17 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(10, "=1,00,00,00,00,00,00,00,00,000 (19 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(11, "=1,00,00,00,00,00,00,00,00,00,000 (21 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(12, "=1,00,00,00,00,00,00,00,00,00,00,000 (23 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(13, "=1,00,00,00,00,00,00,00,00,00,00,00,000 (25 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(14, "=1,00,00,00,00,00,00,00,00,00,00,00,00,000 (27 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(15, "=1,00,00,00,00,00,00,00,00,00,00,00,00,00,000 (29 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(16, "=1,00,00,00,00,00,00,00,00,00,00,00,00,00,00,000 (31 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(17, "=1,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,000 (33 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(18, "=1,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,000 (35 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(19, "=1,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,000 (37 Trailing Zeros)"));
            this.listHelpNotation.Add(new DictionaryEntry(20, "=1,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,000 (39 Trailing Zeros)"));
        }

        /// <summary>
        /// Reverse the punctuations.
        /// </summary>
        private void ReverseHashTable()
        {
            Hashtable hashtable = new Hashtable();
            foreach (DictionaryEntry item in this.hashTablePunctuation)
            {
                hashtable.Add(item.Key, this.Reverse(item.Value.ToString()));
            }

            this.hashTablePunctuation.Clear();
            this.hashTablePunctuation = hashtable;
        }

        /// <summary>
        /// Insert punctuation in punctuation hash table.
        /// </summary>
        /// <param name="stringValue">The value to add.</param>
        private void InsertToPunctuationTable(string stringValue)
        {
            this.hashTablePunctuation.Add(1, stringValue.Substring(0, 3).ToString());
            int j = 2;
            for (int i = 3; i < stringValue.Length; i = i + 2)
            {
                if (stringValue.Substring(i).Length > 0)
                {
                    if (stringValue.Substring(i).Length >= 2)
                    {
                        this.hashTablePunctuation.Add(j, stringValue.Substring(i, 2).ToString());
                    }
                    else
                    {
                        this.hashTablePunctuation.Add(j, stringValue.Substring(i, 1).ToString());
                    }
                }
                else
                {
                    break;
                }

                j++;
            }
        }

        /// <summary>
        /// Reverse the string.
        /// </summary>
        /// <param name="stringValue">The string to reverse.</param>
        /// <returns>The reversed string.</returns>
        private string Reverse(string stringValue)
        {
            string reversed = string.Empty;
            foreach (char character in stringValue)
            {
                reversed = character + reversed;
            }

            return reversed;
        }

        /// <summary>
        /// Get the word conversion of specifixc number.
        /// </summary>
        /// <param name="inputNumber">The number to convert.</param>
        /// <returns>The word conversion from number.</returns>
        private string GetWordConversion(string inputNumber)
        {
            string toReturnWord = string.Empty;
            if (inputNumber.Length <= 3 && inputNumber.Length > 0)
            {
                if (inputNumber.Length == 3)
                {
                    if (int.Parse(inputNumber.Substring(0, 1)) > 0)
                    {
                        toReturnWord = toReturnWord + this.StaticSuffixFind(inputNumber.Substring(0, 1)) + "Hundread ";
                    }

                    string tempString = this.StaticSuffixFind(inputNumber.Substring(1, 2));
                    if (tempString == string.Empty)
                    {
                        toReturnWord = toReturnWord + this.StaticSuffixFind(inputNumber.Substring(1, 1) + "0");
                        toReturnWord = toReturnWord + this.StaticSuffixFind(inputNumber.Substring(2, 1));
                    }

                    toReturnWord = toReturnWord + tempString;
                }

                if (inputNumber.Length == 2)
                {
                    string tempString = this.StaticSuffixFind(inputNumber.Substring(0, 2));
                    if (tempString == string.Empty)
                    {
                        toReturnWord = toReturnWord + this.StaticSuffixFind(inputNumber.Substring(0, 1) + "0");
                        toReturnWord = toReturnWord + this.StaticSuffixFind(inputNumber.Substring(1, 1));
                    }

                    toReturnWord = toReturnWord + tempString;
                }

                if (inputNumber.Length == 1)
                {
                    toReturnWord = toReturnWord + this.StaticSuffixFind(inputNumber.Substring(0, 1));
                }
            }

            return toReturnWord;
        }

        /// <summary>
        /// Find the static suffix for number.
        /// </summary>
        /// <param name="numberKey">The number.</param>
        /// <returns>the static suffix for number.</returns>
        private string StaticSuffixFind(string numberKey)
        {
            string valueFromNumber = string.Empty;
            foreach (DictionaryEntry pair in this.listStaticSuffix)
            {
                if (pair.Key.ToString().Trim() == numberKey.Trim())
                {
                    valueFromNumber = pair.Value.ToString();
                }
            }

            return valueFromNumber;
        }

        /// <summary>
        /// Find the static prefix for number.
        /// </summary>
        /// <param name="numberKey">The number.</param>
        /// <returns>the static prefix for number.</returns>
        private string StaticPrefixFind(string numberKey)
        {
            string valueFromNumber = string.Empty;
            foreach (DictionaryEntry pair in this.listStaticPrefix)
            {
                if (pair.Key.ToString().Trim() == numberKey.Trim())
                {
                    valueFromNumber = pair.Value.ToString();
                    /*
                    else
                        ValueFromNumber = "<span title='" + StaticHelpNotationFind(Pair.Key.ToString()) + "' style='color:" + this.color.ToKnownColor().ToString() + "'>" + Pair.Value.ToString() + "</span>";
                    */
                }
            }

            return valueFromNumber;
        }

        /// <summary>
        /// Find the static help notation for number.
        /// </summary>
        /// <param name="numberKey">The number.</param>
        /// <returns>the static help notation for number.</returns>
        private string StaticHelpNotationFind(string numberKey)
        {
            string helpText = string.Empty;
            foreach (DictionaryEntry pair in this.listHelpNotation)
            {
                if (pair.Key.ToString().Trim() == numberKey.Trim())
                {
                    helpText = pair.Value.ToString();
                }
            }

            return helpText;
        }
        #endregion
    }
}

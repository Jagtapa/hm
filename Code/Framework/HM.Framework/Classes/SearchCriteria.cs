// <copyright file="SearchCriteria.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class represents SearchCriteria entity.
// </summary>
namespace HM.Framework
{
    using System;
    using System.Collections.Specialized;
    using System.Runtime.Serialization;

    /// <summary>
    /// This class holds a collection of Errors (Error IDs and respective Error Messages).
    /// </summary>
    /// <remarks>
    /// This is the error class. the intended use for this class is to store
    /// error messages that need to be displayed to the user. This implies that all
    /// errors are be displayed to the user at one go. This class is a collection
    /// of Name-Value pairs which stores the Error ID as the Name and the Error
    /// Message as the Value.
    /// <seealso cref="NameValueCollection"/>
    /// </remarks>
    [DataContract]
    [Serializable]
    public class SearchCriteria
    {
        #region Private Variables
        /// <summary>
        /// Search name value collection.
        /// </summary>
        private NameValueCollection searchNameValueCollection = null; 
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the SearchCriteria class.
        /// </summary>
        public SearchCriteria()
        {
            this.searchNameValueCollection = new NameValueCollection();
        } 
        #endregion

        #region Public Methods
        /// <summary>
        /// This method is to add the column name and search value.
        /// </summary>
        /// <param name="columnName">string columnName</param>
        /// <param name="searchValue">string searchValue</param>
        public void Add(string columnName, string searchValue)
        {
            this.searchNameValueCollection.Add(columnName, searchValue);
        }

        /// <summary>
        /// This function returns the search value added in the object of <see cref="SearchCriteria"/>
        /// class w.r.t. to a particular column name.
        /// </summary>
        /// <param name="columnName">The Column Name for which the search value 
        /// is required.</param>
        /// <returns>The search value for the desired Column Name.</returns>
        public string Get(string columnName)
        {
            return this.searchNameValueCollection[columnName];
        }

        /// <summary>
        /// This function returns the search value in the object of <see cref="SearchCriteria"/>
        /// class w.r.t. to a particular Index.
        /// </summary>
        /// <param name="index">The Index for which the search value
        /// is required.</param>
        /// <returns>The search value at the desired Index.</returns>
        public string Get(int index)
        {
            return this.searchNameValueCollection[index];
        }

        /// <summary>
        /// This function returns the column name added in the object of <see cref="SearchCriteria"/>
        /// class w.r.t. to a particular Index.
        /// </summary>
        /// <param name="index">The Index for which the column name
        /// is required.</param>
        /// <returns>The column name at the desired Index.</returns>
        public string GetColumnName(int index)
        {
            return this.searchNameValueCollection.Keys[index];
        }

        /// <summary>
        /// Gets the count of search criteria added.
        /// </summary>
        /// <returns>The search criteria Count.</returns>
        public int GetCount()
        {
            return this.searchNameValueCollection.Count;
        }

        /// <summary>
        /// Removes the search criteria from the search criteria object based on the column name.
        /// </summary>
        /// <param name="columnName">The criteria to be removed from the search object</param>
        public void RemoveCriteria(string columnName)
        {
            this.searchNameValueCollection.Remove(columnName);
        } 
        #endregion
    }
}

﻿// <copyright file="KaizenXmlConfigurationManager.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>07-09-2013</date>
// <summary> This class consists of following static methods for xml congiguration reading.
// </summary>
namespace HM.Framework
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;

    /// <summary>
    /// This class consists of following static methods for xml congiguration reading.
    /// The class is extensible means it can have more static methods.
    /// </summary>
    public static class KaizenXmlConfigurationManager
    {        
    }
}

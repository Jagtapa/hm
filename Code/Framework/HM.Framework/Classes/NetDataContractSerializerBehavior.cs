﻿// <copyright file="NetDataContractSerializerBehavior.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class is used to serialize the interface using net data contract serializer interfaces.
// </summary>
namespace HM.Framework
{
    using System;
    using System.Collections.ObjectModel;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Description;
    using System.ServiceModel.Dispatcher;

    /// <summary>
    /// NetDataContractSerializerBehavior class.
    /// </summary>
    public class NetDataContractSerializerBehavior : Attribute, IServiceBehavior, IEndpointBehavior
    {
        #region Public Methods
        /// <summary>
        /// Validates the behavior
        /// </summary>
        /// <param name="serviceDescription">ServiceDescription serviceDescription</param>
        /// <param name="serviceHostBase">ServiceHostBase serviceHostBase</param>
        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        /// <summary>
        /// Add bind parameters.
        /// </summary>
        /// <param name="serviceDescription">ServiceDescription serviceDescription</param>
        /// <param name="serviceHostBase">ServiceHostBase serviceHostBase</param>
        /// <param name="endpoints">Collection of ServiceEndpoint i.e. endpoints</param>
        /// <param name="bindingParameters">BindingParameterCollection bindingParameters</param>
        public void AddBindingParameters(
            ServiceDescription serviceDescription,
            ServiceHostBase serviceHostBase,
            Collection<ServiceEndpoint> endpoints,
            BindingParameterCollection bindingParameters)
        {
        }

        /// <summary>
        /// Register contracts.
        /// </summary>
        /// <param name="serviceDescription">ServiceDescription serviceDescription</param>
        /// <param name="serviceHostBase">ServiceHostBase serviceHostBase</param>
        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (var endpoint in serviceDescription.Endpoints)
            {
                this.RegisterContract(endpoint);
            }
        }

        /// <summary>
        /// Validates endpoint.
        /// </summary>
        /// <param name="endpoint">ServiceEndpoint endpoint</param>
        public void Validate(ServiceEndpoint endpoint)
        {
        }

        /// <summary>
        /// Register contract from endpoints
        /// </summary>
        /// <param name="endpoint">ServiceEndpoint endpoint</param>
        /// <param name="bindingParameters">BindingParameterCollection bindingParameters</param>
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
            this.RegisterContract(endpoint);
        }

        /// <summary>
        /// Apply dispatcher for end points.
        /// </summary>
        /// <param name="endpoint">ServiceEndpoint endpoint</param>
        /// <param name="endpointDispatcher">EndpointDispatcher endpointDispatcher</param>
        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        /// <summary>
        /// Apply clisnt behavoir for service end points.
        /// </summary>
        /// <param name="endpoint">ServiceEndpoint endpoint</param>
        /// <param name="clientRuntime">ClientRuntime clientRuntime</param>
        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Registers the contract for services to expose.
        /// </summary>
        /// <param name="endpoint">The Service Endpoint.</param>
        protected void RegisterContract(ServiceEndpoint endpoint)
        {
            foreach (OperationDescription desc in endpoint.Contract.Operations)
            {
                var dcsOperationBehavior = desc.Behaviors.Find<DataContractSerializerOperationBehavior>();
                if (dcsOperationBehavior != null)
                {
                    int idx = desc.Behaviors.IndexOf(dcsOperationBehavior);
                    desc.Behaviors.Remove(dcsOperationBehavior);
                    desc.Behaviors.Insert(idx, new NetDataContractSerializerOperationBehavior(desc));
                }
            }
        }
        #endregion
    }
}

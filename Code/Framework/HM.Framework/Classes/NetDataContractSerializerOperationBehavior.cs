﻿// <copyright file="NetDataContractSerializerOperationBehavior.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class is used to create the serializer for interfaces.
// </summary>
namespace HM.Framework
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters;
    using System.ServiceModel.Description;
    using System.Xml;

    /// <summary>
    /// NetDataContractSerializerOperationBehavior class.
    /// </summary>
    public class NetDataContractSerializerOperationBehavior : DataContractSerializerOperationBehavior
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the NetDataContractSerializerOperationBehavior class.
        /// </summary>
        /// <param name="operationDescription">OperationDescription operationDescription</param>
        public NetDataContractSerializerOperationBehavior(OperationDescription operationDescription)
            : base(operationDescription)
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Creates the serializer for net data contract.
        /// </summary>
        /// <param name="type">The type of entity.</param>
        /// <param name="name">root name of entity.</param>
        /// <param name="ns">root namespace of entity.</param>
        /// <param name="knownTypes">The known type interfaces.</param>
        /// <returns>Serializer for interfaces.</returns>
        public override XmlObjectSerializer CreateSerializer(
            Type type,
            string name,
            string ns,
            IList<Type> knownTypes)
        {
            return new NetDataContractSerializer(
                name,
                ns,
                new StreamingContext(),
                int.MaxValue,
                false,
                FormatterAssemblyStyle.Simple,
                null);
        }

        /// <summary>
        /// Creates the serializer for net data contract.
        /// </summary>
        /// <param name="type">The type of entity.</param>
        /// <param name="name">root name.</param>
        /// <param name="ns">root namespace.</param>
        /// <param name="knownTypes">The know types of interfaces.</param>
        /// <returns>The serializer for interfaces.</returns>
        public override XmlObjectSerializer CreateSerializer(
            Type type,
            XmlDictionaryString name,
            XmlDictionaryString ns,
            IList<Type> knownTypes)
        {
            return new NetDataContractSerializer(
                name,
                ns,
                new StreamingContext(),
                int.MaxValue,
                this.IgnoreExtensionDataObject,
                FormatterAssemblyStyle.Simple,
                null);
        }
        #endregion
    }
}

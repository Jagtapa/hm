﻿// <copyright file="SoapLoggerExtensionAttribute.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Sandip Salunkhe</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>27-06-2013</date>
// <summary>This class is used for logging the soap message.
// </summary>

namespace HM.Framework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Services.Protocols;

    /// <summary>
    /// Attribute method
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class SoapLoggerExtensionAttribute : SoapExtensionAttribute
    {
        /// <summary>
        /// Priority variable
        /// </summary>
        private int priority = 1;

        /// <summary>
        /// Priority property for the soap log.
        /// </summary>
        public override int Priority
        {
            get { return this.priority; }
            set { this.priority = value; }
        }

        /// <summary>
        /// ExtensionType  property for the soap log.
        /// </summary>
        public override System.Type ExtensionType
        {
            get { return typeof(SoapLoggerExtension); }
        }
    }
}

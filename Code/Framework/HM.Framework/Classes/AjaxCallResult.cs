// <copyright file="AjaxCallResult.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This file used to handle Ajax Call Result.
// </summary>

namespace HM.Framework
{
    using System;

    /// <summary>
    /// Grid value struct.
    /// </summary>
    [Serializable]
    public struct StructGridValue
    {
        #region Private Members
        /// <summary>
        /// Grid client ID.
        /// </summary>
        private string gridClientId;

        /// <summary>
        /// Row index.
        /// </summary>
        private int rowIndex;

        /// <summary>
        /// Column header name.
        /// </summary>
        private string columnHeader;

        /// <summary>
        /// Column value.
        /// </summary>
        private string value; 
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the StructGridValue struct.
        /// </summary>
        /// <param name="gridId">string gridId</param>
        /// <param name="rowInd">int rowInd</param>
        /// <param name="colHeader">string colHeader</param>
        /// <param name="val">string val</param>
        public StructGridValue(string gridId, int rowInd, string colHeader, string val)
        {
            this.gridClientId = gridId;
            this.rowIndex = rowInd;
            this.columnHeader = colHeader;
            this.value = val;
        } 
        #endregion
    }

    /// <summary>
    /// AjaxCallResult class.
    /// </summary>
    [Serializable]
    public class AjaxCallResult
    {
        #region Properties
        /// <summary>
        /// Gets or sets the message to be displayed as alert.
        /// </summary>
        /// <remarks>
        /// A typical scenario for the use of this property would be to show any validation message as alert e.g. 'Only one row can be selected' in case of Manage JobComments.
        /// </remarks>
        public string AlertMsg
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the message to be displayed on page.
        /// </summary>
        /// <remarks>
        /// A typical scenario for the use of this property would be to show any message on the page e.g. 'No Haulier selected.'.
        /// </remarks>
        public string ErrorMessage
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the client id of error label on the page.
        /// </summary>
        /// <remarks>
        /// A typical scenario for the use of this property would be to identify label to show any message on the page e.g. 'No Haulier selected.'.
        /// </remarks>
        public string ErrorLabelClientId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether bool for success/failure of Ajax call.
        /// </summary>
        /// <remarks>
        /// A typical scenario for the use of this property would be indicate that Ajax call has failed.
        /// </remarks>
        public bool Result
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the URL of the pop up page.
        /// </summary>
        /// <remarks>
        /// A typical scenario for the use of this property would be to open pop up page for context menu.
        /// </remarks>
        public string PopUpUrl
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the  URL of the page application will be redirected to, in case of session expiry.
        /// </summary>
        public string SessionExpiredUrl
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the attributes for window.open() i.e. third argument for window.open.
        /// </summary>
        /// <remarks>
        /// A typical scenario for the use of this property would be to open a pop up window for a specific size.
        /// </remarks>
        public string PopUpAttributes
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Name of the pop up window.
        /// </summary>
        /// <remarks>
        /// A typical scenario for the use of this property would be to open pop up page with specific Name.
        /// </remarks>
        public string PopUpName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the return value.
        /// </summary>
        /// <remarks>
        /// A typical scenario for the use of this property would be to return any string value other than provided properties.
        /// </remarks>
        public string ReturnValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the array of structGridValue structures.
        /// </summary>
        /// <remarks>
        /// A typical scenario for the use of this property would be to return one structGridValue structure for each row to be set with new value.
        /// </remarks>
        public StructGridValue[] GridValues
        {
            get;
            set;
        }

        #endregion
    }
}

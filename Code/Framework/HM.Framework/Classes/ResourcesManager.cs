// <copyright file="ResourcesManager.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class is used to manage the resources in resource file.
// </summary>
namespace HM.Framework
{
    using System;
    using System.Reflection;
    using System.Resources;

    /// <summary>
    /// ResourcesManager Class is for retrieving the resources.
    /// </summary>
    public static class ResourcesManager
    {
        #region Private Variables
        /// <summary>
        /// Assembly base name.
        /// </summary>
        private static string baseName = ConfigManager.ReadAppSettings(Global.CONSTSTRBASENAME);

        /// <summary>
        /// Assembly name.
        /// </summary>
        private static string assemblyName = ConfigManager.ReadAppSettings(Global.CONSTSTRASSEMBLYNAME);

        /// <summary>
        /// Resource manager instance.
        /// </summary>
        private static ResourceManager resourcesManager = new ResourceManager(baseName, Assembly.Load(assemblyName)) { IgnoreCase = true };
        #endregion

        #region Public Methods
        /// <summary>
        /// This function returns the translated string from resource manager.
        /// </summary>
        /// <param name="resourceKey">Token id for resource manager.</param>
        /// <returns>The value of the specified key from the resource file</returns>
        public static string GetResourceString(this string resourceKey)
        {
            if (string.IsNullOrEmpty(resourceKey))
            {
                return string.Empty;
            }

            return resourcesManager.GetString(resourceKey);
        }

        /// <summary>
        /// This function returns the translated string from resource manager.
        /// </summary>
        /// <param name="resourceKey">Token id for resource manager.</param>
        /// <param name="placeHolderValue">Format in which the string is to be returned</param>
        /// <returns>The value of the specified key from the resource file</returns>
        public static string GetResourceString(this string resourceKey, string placeHolderValue)
        {
            if (string.IsNullOrEmpty(resourceKey))
            {
                return string.Empty;
            }

            return string.Format(resourcesManager.GetString(resourceKey), placeHolderValue);
        }

        /// <summary>
        /// This function returns the translated string from resource manager.
        /// </summary>
        /// <param name="resourceKey">Token id for resource manager.</param>
        /// <param name="placeHolderValues">Formats in which the string is to be returned</param>
        /// <returns>The value of the specified key from the resource file</returns>
        public static string GetResourceString(this string resourceKey, string[] placeHolderValues)
        {
            if (string.IsNullOrEmpty(resourceKey))
            {
                return string.Empty;
            }

            return string.Format(resourcesManager.GetString(resourceKey), placeHolderValues);
        }

        /// <summary>
        /// Function to get the resource value, other than string from resource file
        /// </summary>
        /// <param name="resourceKey">Token id for resource manager.</param>
        /// <returns>The value of the specified object from the resource file</returns>
        public static object GetResourceObject(this string resourceKey)
        {
            if (string.IsNullOrEmpty(resourceKey))
            {
                return string.Empty;
            }

            return resourcesManager.GetObject(resourceKey);
        }
        #endregion
    }
}

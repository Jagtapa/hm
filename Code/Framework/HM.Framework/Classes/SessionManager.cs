﻿// <copyright file="SessionManager.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>13-08-2013</date>
// <summary>This class used to manage Session handling.
// </summary>

namespace HM.Framework
{
    using System;
    using System.Web;

    /// <summary>
    /// Session Manager class to handle session operations.
    /// </summary>
    public sealed class SessionManager
    {
        /// <summary>
        /// SessionManager class name constant.
        /// </summary>
        private const string SESSIONMANAGER = "SessionManager";

        /// <summary>
        /// the http context.
        /// </summary>
        private HttpContext httpContext;

        /// <summary>
        /// the GUID of application.
        /// </summary>
        private Guid sessionID;

        /// <summary>
        /// The session object.
        /// </summary>
        private object sessionObject;

        /// <summary>
        /// Initializes a new instance of the SessionManager class.
        /// </summary>
        /// <param name="httpContext">The http context</param>
        public SessionManager(HttpContext httpContext)
        {
            this.httpContext = httpContext;
        }

        /// <summary>
        /// Gets the instance of current session maanger class.
        /// </summary>
        public static SessionManager Current
        {
            get { return (SessionManager)HttpContext.Current.Items[SESSIONMANAGER]; }
        }

        /// <summary>
        /// Gets or sets the session iD.
        /// </summary>
        public Guid SessionID
        {
            get
            {
                return this.sessionID;
            }

            set
            {
                if (!this.sessionID.Equals(value))
                {
                    this.sessionID = value;
                    this.GetSessionObject(value);
                }
            }
        }

        /// <summary>
        /// Gets the session object.
        /// </summary>
        public object SessionObject
        {
            get { return this.sessionObject; }
        }

        /// <summary>
        /// Initialize the Session Manager when application start.
        /// </summary>
        /// <param name="httpContext">The http context</param>
        public static void Initialize(HttpContext httpContext)
        {
            httpContext.Items.Add(SESSIONMANAGER, new SessionManager(httpContext));
        }

        /// <summary>
        /// Gets the session object based on session id.
        /// </summary>
        /// <param name="sessionID">The session ID.</param>
        public void GetSessionObject(Guid sessionID)
        {
            this.sessionObject = this.httpContext.Cache[sessionID.ToString()];
            if (this.sessionObject == null)
            {
                this.SetSessionObject(sessionID, new object());
            }
        }

        /// <summary>
        /// Set the session object.
        /// </summary>
        /// <param name="sessionID">The session ID.</param>
        /// <param name="sessionObject">The session object.</param>
        private void SetSessionObject(Guid sessionID, object sessionObject)
        {
            this.httpContext.Cache.Add(sessionID.ToString(), sessionObject, null, DateTime.MaxValue, TimeSpan.FromHours(2), System.Web.Caching.CacheItemPriority.High, null);
            this.sessionObject = sessionObject;
        }
    }
}

// <copyright file="ExceptionHandler.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class used to log and handle the exceptions.
// </summary>
namespace HM.Framework
{
    using System;
    using System.IO;
    using System.Text;
    using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

    /// <summary>
    /// ExceptionHandler class to handle the exceptions.
    /// </summary>
    public static class ExceptionHandler
    {
        #region Public Methods
        /// <summary>
        /// handles the exception
        /// </summary>
        /// <param name="exception">An exception to handle.</param>
        /// <param name="policyName">Exception policy.</param>
        public static void HandleException(this Exception exception, string policyName)
        {
            try
            {
                bool rethrow = ExceptionPolicy.HandleException(exception, policyName);
                if (rethrow)
                {
                    throw exception;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Format exception.
        /// </summary>
        /// <param name="exception">Exception exception</param>
        /// <returns>The formated exception string.</returns>
        public static string GetFormattedException(this Exception exception)
        {
            StringBuilder stringBuilder = new StringBuilder();
            StringWriter stringWriter = new StringWriter(stringBuilder);
            TextExceptionFormatter formatter = new TextExceptionFormatter(stringWriter, exception);
            formatter.Format();
            return stringBuilder.ToString();
        } 
        #endregion
    }
}
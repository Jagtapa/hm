// <copyright file="AsyncInvoker.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class used to Async calls.
// </summary>

namespace HM.Framework
{
    using System;

    /// <summary>
    /// AsyncInvoker class.
    /// </summary>
    public sealed class AsyncInvoker
    {
        #region Private Members
        /// <summary>
        /// Asynch result.
        /// </summary>
        private IAsyncResult asyncResult; 
        #endregion

        #region Constrcutor
        /// <summary>
        /// Initializes a new instance of the AsyncInvoker class.
        /// </summary>
        /// <param name="methodDelegateArgs">MethodDelegate methodDelegateArgs</param>
        public AsyncInvoker(MethodDelegate methodDelegateArgs)
        {
            this.MethodDelegateParam = new MethodDelegate(methodDelegateArgs);
        }

        /// <summary>
        /// Initializes a new instance of the AsyncInvoker class.
        /// </summary>
        /// <param name="methodDelegateWithParameterArgs">MethodDelegateWithParameter methodDelegateWithParameterArgs</param>
        /// <param name="args">object array args</param>
        public AsyncInvoker(MethodDelegateWithParameter methodDelegateWithParameterArgs, object[] args)
        {
            this.MethodDelegateWithParam = new MethodDelegateWithParameter(methodDelegateWithParameterArgs);
            this.MethodArgs = args;
        } 
        #endregion

        #region Delegates
        /// <summary>
        /// MethodDelegate delegate.
        /// </summary>
        public delegate void MethodDelegate();

        /// <summary>
        /// MethodDelegateWithParameter delegate.
        /// </summary>
        /// <param name="value">object value</param>
        public delegate void MethodDelegateWithParameter(object value); 
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets or sets the method delegate.
        /// </summary>
        public MethodDelegate MethodDelegateParam { get; set; }

        /// <summary>
        /// Gets or sets the method delegate with parameter.
        /// </summary>
        public MethodDelegateWithParameter MethodDelegateWithParam { get; set; }

        /// <summary>
        /// Gets or sets the MethodArgs.
        /// </summary>
        public object[] MethodArgs { get; set; }

        /// <summary>
        /// Invokes the begin request.
        /// </summary>
        /// <param name="form">windows form object.</param>
        public void Invoke(System.Windows.Forms.Form form)
        {
            if (this.MethodArgs != null)
            {
                this.asyncResult = form.BeginInvoke(this.MethodDelegateWithParam, this.MethodArgs);
            }
            else
            {
                this.asyncResult = form.BeginInvoke(this.MethodDelegateParam);
            }
        }

        /// <summary>
        /// Result of asynch operation.
        /// </summary>
        /// <param name="form">The windows form object.</param>
        /// <returns>Asynch result callback.</returns>
        public IAsyncResult Result(System.Windows.Forms.Form form)
        {
            object obj = form.EndInvoke(this.asyncResult);
            return this.asyncResult;
        } 
        #endregion
    }
}

// <copyright file="Global.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>
// This class used to define the global constants.
// </summary>

namespace HM.Framework
{
    /// <summary>
    /// Global class for constants.
    /// </summary>
    public class Global
    {
        #region General Constants

        /// <summary>
        /// Constant used for SELECT Queries with NOLOCK Attribute
        /// </summary>
        public const string CONSTSTRSELECTWITHNOLOCK = " WITH (NOLOCK) ";

        /// <summary>
        /// Constant used for UPDATE Queries with ROWLOCK Attribute
        /// </summary>
        public const string CONSTSTRUPDATEWITHROWLOCK = " WITH (ROWLOCK) ";

        /// <summary>
        /// Constact for command time out value to be used when connecting to database.
        /// </summary>
        public const string CONSTSTRCOMMANDTIMEOUT = "CommandTimeout";

        /// <summary>
        /// Constant for Base Name of Resource file.
        /// </summary>
        public const string CONSTSTRBASENAME = "ResourceBaseName";

        /// <summary>
        /// Constant for Assembly Name for Resource file.
        /// </summary>
        public const string CONSTSTRASSEMBLYNAME = "ResourceAssemblyName";

        /// <summary>
        /// Constant for a long DateTime format SQL Server.
        /// </summary>
        public const string CONSTFORMATDATETIMESQL101 = "yyyy-MM-dd HH:mm:ss";

        /// <summary>
        /// Constant for a long DateTime format SQL Server.
        /// </summary>
        public const string CONSTFORMATDATESQL121 = "yyyy-MM-dd 00:00:00";

        /// <summary>
        /// Constant for a long DateTime format SQL Server.
        /// </summary>
        public const string CONSTFORDBENDTIME = "yyyy-MM-dd 23:59:59";

        /// <summary>
        /// Constant for a config key for "DEFAULTDATETIMEFORMAT"
        /// </summary>
        public const string CONSTSTRCONFIGDEFAULTDATETIMEFORMAT = "DEFAULTDATETIMEFORMAT";

        /// <summary>
        /// Constant for a config key for "DEFAULTDATETIMEFORMAT"
        /// </summary>
        public const string CONSTSTRCONFIGDEFAULTDATEFORMAT = "DEFAULTDATEFORMAT";

        /// <summary>
        /// Constant for a config key for "DEFAULTDATETIMEFORMAT"
        /// </summary>
        public const string CONSTSTRCONFIGDEFAULTTIMEFORMAT = "DEFAULTTIMEFORMAT";

        /// <summary>
        /// Constant for a start time.
        /// </summary>
        public const string CONSTSTRSTARTTIME = "00:00:00";

        /// <summary>
        /// Constant for a end time.
        /// </summary>
        public const string CONSTSTRENDTIME = "23:59:59";

        /// <summary>
        /// Constant for Error Message for Config File Load Error.
        /// </summary>
        public const string CONSTSTRSERVERAPPCONFIGLOADERROR = "STR_SERVERAPPCONFIGLOADERROR";

        /// <summary>
        /// Constant for Error Message for Max Length Input validation.
        /// </summary>
        public const string CONSTERRMAXLENGTH = "ERR_MAXLENGTH";

        /// <summary>
        /// Confirm message.
        /// </summary>
        public const string CONSTSTRCONFIRM = "STR_CONFIRM";

        /// <summary>
        /// Constant for combobox default value
        /// </summary>
        public const string CONSTCOMBOBOXDEFAULTVALUE = "** SELECT **";

        /// <summary>
        /// A constant representing regular expression for comma seperated numbers.
        /// </summary>
        public const string CONSTSTRREGEXCOMMASEPERATEDNUMBERS = "^[0-9]+( *, *[0-9]+)*$";

        #endregion

        #region Logger
        /// <summary>
        /// The below values will be replaced by .config entry that will specify their status
        /// Information Category.
        /// </summary>
        public const string INFORMATIONCATEGORY = "InformationCategory";

        /// <summary>
        /// The below values will be replaced by .config entry that will specify their status
        /// Error Category.
        /// </summary>
        public const string ERRORCATEGORY = "ErrorCategory";

        /// <summary>
        /// The below values will be replaced by .config entry that will specify their status
        /// Warnings Category.
        /// </summary>
        public const string WARNINGSCATEGORY = "WarningCategory";

        /// <summary>
        /// The below values will be replaced by .config entry that will specify their status
        /// Soap message Category.
        /// </summary>
        public const string SOAPINFOCATEGORY = "SoapInfoCategory";

        /// <summary>
        /// Is logging enabled.
        /// </summary>
        public const string LOGENABLED = "LogEnabled";
        #endregion

        /// <summary>
        /// Resource string for string "Select"
        /// </summary>
        public const string CONSTSTRSELECT = "STR_SELECT";

        /// <summary>
        /// Resource string for string "All"
        /// </summary>
        public const string CONSTSTRALL = "STR_ALL";

        /// <summary>
        /// Resource string for error message when no records are found in the query
        /// </summary>
        public const string CONSTSTRNORECORDSFOUND = "STR_NORECORDSFOUND";

        /// <summary>
        /// Resource string for warning message when no of records retrieved in the 
        /// query exceeds the maximum limit
        /// </summary>
        public const string CONSTSTRMAXDBRECORDSEXCEEDED = "STR_MAXDBRECORDSEXCEEDED";

        /// <summary>
        /// Resource string for error when no data is available matching the request
        /// </summary>
        public const string CONSTSTRNODATAAVAILABLESTRING = "STR_NODATAAVAILABLESTRING";

        /// <summary>
        /// Resource string for confirmation message while deleting an entity
        /// </summary>
        public const string CONSTSTRDELETECONFIRMATION = "STR_DELETECONFIRMATION";

        /// <summary>
        /// A string constant for displaying message when the data is saved successfully.
        /// </summary>
        public const string CONSTSTRSAVEDSUCCESSFULLY = "STR_SAVEDSUCCESSFULLY";

        /// <summary>
        /// A string constant for displaying message when the data is deleted successfully.
        /// </summary>
        public const string CONSTSTRDELETEDSUCCESSFULLY = "STR_DELETEDSUCCESSFULLY";

        /// <summary>
        /// A string constant for displaying delete confirmation message.
        /// </summary>
        public const string CONSTSTRGRIDROWSDELETECONFIRMATION = "STR_GRIDROWSDELETECONFIRMATION";

        /// <summary>
        /// A string constant for displaying delete warning
        /// </summary>
        public const string CONSTSTRDELETEROWWARNING = "STR_DELETEROWWARNING";

        /// <summary>
        /// A string constant for displaying EDIT warning
        /// </summary>
        public const string CONSTSTREDITROWROWWARNING = "STR_EDITROWROWWARNING";

        /// <summary>
        /// A string constant for displaying EDIT warning
        /// </summary>
        public const string CONSTSTRMULTIPLEEDITWARNING = "STR_MULTIPLEEDITWARNING";

        /// <summary>
        /// A string constant for displaying message to select a row in the grid control for modify/delete operation.
        /// </summary>
        public const string CONSTSTRSELECTGRIDROWMSG = "STR_SELECTGRIDROWMSG";

        /// <summary>
        /// A string constant for display message when there are no records to delete.
        /// </summary>
        public const string CONSTERRSTRNORECORDSTODELETEMSG = "STR_NORECORDSTODELETEMSG";

        /// <summary>
        /// No records to save.
        /// </summary>
        public const string CONSTERRSTRNORECORDSTOSAVEMSG = "STR_NORECORDSTOSAVEMSG";

        /// <summary>
        /// A string constant for displaying error message when there is an error during delete operation.
        /// See the STR_DELETINGERROR in the resource file.
        /// </summary>
        public const string CONSTERRSTRDELETINGERROR = "STR_DELETINGERROR";

        /// <summary>
        /// A string constant for displaying error message when there is a error during save operation.
        /// See the STR_SAVINGERROR in the resource file.
        /// </summary>
        public const string CONSTERRSTRSAVINGERROR = "STR_SAVINGERROR";

        /// <summary>
        /// Exception policy for bussiness classes.
        /// </summary>
        public const string CONSTSTREXCPOLICYBUSINESS = "Business Policy";

        /// <summary>
        /// Exception policy for UI classes.
        /// </summary>
        public const string CONSTSTREXCPOLICYUI = "UI Policy";

        /// <summary>
        /// Exception policy for resume classes.
        /// </summary>
        public const string CONSTSTREXCPOLICYRESUME = "Resume policy";

        /// <summary>
        /// Exception mails subject line.
        /// </summary>
        public const string CONSTSTRQCEXCEPTIONMAILSUBJECT = "QCEXCEPTIONMAILSUBJECT";

        /// <summary>
        /// from mail id
        /// </summary>
        public const string CONSTSTRQCEXCEPTIONMAILFROM = "QCEXCEPTIONMAILFROM";

        /// <summary>
        /// To mail id
        /// </summary>
        public const string CONSTSTRQCEXCEPTIONMAILTO = "QCEXCEPTIONMAILTO";

        /// <summary>
        /// CC mail id
        /// </summary>
        public const string CONSTSTRQCEXCEPTIONMAILCC = "QCEXCEPTIONMAILCC";

        /// <summary>
        /// BCC mail id
        /// </summary>
        public const string CONSTSTRQCEXCEPTIONMAILBCC = "QCEXCEPTIONMAILBCC";

        /// <summary>
        /// SMTP mail server.
        /// </summary>
        public const string CONSTSTRQCEXCEPTIONMAILSMTPSERVER = "QCEXCEPTIONMAILSMTPSERVER";

        /// <summary>
        /// Gets the Constant for a default DateTime format for all screens.
        /// </summary>
        public const string CONSTSTRDATEDEFAULTFORMAT = "dd-MM-yyyy hh:mm:ss";

        /// <summary>
        /// The end time of day.
        /// </summary>
        public const string ENDTIME = "23:59:59";

        /// <summary>
        /// The start time of day.
        /// </summary>
        public const string STARTTIME = "00:00:00";
        
        /// <summary>
        /// Gets or sets the Constant for a default DateTime format for all screens.
        /// </summary>
        public static string CONSTSTRDATETIMEDEFAULTFORMAT { get; set; }

        /// <summary>
        /// Gets or sets the Constant for a default DateTime format for all screens.
        /// </summary>
        public static string CONSTSTRTIMEDEFAULTFORMAT
        {
            get;
            set;
        }       
    }
}

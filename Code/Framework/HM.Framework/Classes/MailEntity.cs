﻿// <copyright file="MailEntity.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class represents mail entity.
// </summary>
namespace HM.Framework
{
    using System;
    using System.Collections.Generic;
    using System.Net.Mime;
    using System.Runtime.Serialization;

    /// <summary>
    /// MailEntity class.
    /// </summary>
    [DataContract]
    [Serializable]
    public class MailEntity : EntityBase
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the MailEntity class.
        /// </summary>
        public MailEntity()
        {
            this.From = string.Empty;
            this.ToAddressList = new List<string>();
            this.CcAddressList = new List<string>();
            this.BccAddressList = new List<string>();
            this.Attachments = new List<string>();
            this.Subject = string.Empty;
            this.Body = string.Empty;
            this.Host = string.Empty;
            this.IsBodyHtml = false;
            this.MediaType = MediaTypeNames.Application.Octet;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the From email ID.
        /// </summary>
        [DataMember]
        public string From
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the CC mail ids list.
        /// </summary>
        [DataMember]
        public List<string> CcAddressList
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the BCC mail ids list.
        /// </summary>
        [DataMember]
        public List<string> BccAddressList
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the To mail ids list.
        /// </summary>
        [DataMember]
        public List<string> ToAddressList
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the List of mail attachments.
        /// </summary>
        [DataMember]
        public List<string> Attachments
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the subject of mail.
        /// </summary>
        [DataMember]
        public string Subject
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the body message of email.
        /// </summary>
        [DataMember]
        public string Body
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether email body is html or plain text.
        /// </summary>
        [DataMember]
        public bool IsBodyHtml
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the The mail exchange server.
        /// </summary>
        [DataMember]
        public string Host
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the The mail exchange server port.
        /// </summary>
        [DataMember]
        public int Port
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the The type of message exchange.
        /// </summary>
        [DataMember]
        public string MediaType
        {
            get;
            set;
        }
        #endregion
    }
}

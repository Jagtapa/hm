// <copyright file="kpiEnum.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>27-09-2013</date>
// <summary>This class is used to define the application level enumerations.
// </summary>

namespace HM.Framework
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The Log Message.
    /// </summary>
    [DataContract]
    [Serializable]
    public enum MessageLogType
    {
        /// <summary>
        /// no message type.
        /// </summary>
        [EnumMember]
        None = 0,

        /// <summary>
        /// Information Log Message.
        /// </summary>
        [EnumMember]
        Info = 1,

        /// <summary>
        /// Error Log Message.
        /// </summary>
        [EnumMember]
        Error = 2,

        /// <summary>
        /// Ignored Log Message
        /// </summary>
        [EnumMember]
        Ignored = 3
    }

    /// <summary>
    /// The Log Message.
    /// </summary>
    [DataContract]
    [Serializable]
    public enum Frequency
    {
        /// <summary>
        /// no message type.
        /// </summary>
        [EnumMember]
        None = 0,

        /// <summary>
        /// Frequency type Daily.
        /// </summary>
        [EnumMember]
        Daily = 1,

        /// <summary>
        /// Frequency type Weekly.
        /// </summary>
        [EnumMember]
        Weekly = 2,

        /// <summary>
        /// Frequency type Monthly.
        /// </summary>
        [EnumMember]
        Monthly = 3,

        /// <summary>
        /// Frequency type Quarterly.
        /// </summary>
        [EnumMember]
        Quarterly = 4,

        /// <summary>
        /// Frequency type Half Yearly.
        /// </summary>
        [EnumMember]
        HalfYearly = 5,

        /// <summary>
        /// Frequency type Yearly.
        /// </summary>
        [EnumMember]
        Yearly = 6,

        /// <summary>
        /// Frequency type Hourly.
        /// </summary>
        [EnumMember]
        Hourly = 7
    }

    /// <summary>
    /// The Approval enum.
    /// </summary>
    [DataContract]
    [Serializable]
    public enum Approval
    {
        /// <summary>
        /// Approval pending.
        /// </summary>
        [EnumMember]
        Pending = 0,

        /// <summary>
        /// Approval done.
        /// </summary>
        [EnumMember]
        Approved = 1,

        /// <summary>
        /// Approval Dis-Approved.
        /// </summary>
        [EnumMember]
        Rejected = 2
    }

    /// <summary>
    /// The KPI processed flag enum.
    /// </summary>
    [DataContract]
    [Serializable]
    public enum KpiProcessStatus
    {
        /// <summary>
        /// KPI flag None.
        /// </summary>
        [EnumMember]
        None = 0,

        /// <summary>
        /// KPI flag Pending.
        /// </summary>
        [EnumMember]
        Pending = 1,

        /// <summary>
        /// KPI flag Processed.
        /// </summary>
        [EnumMember]
        Processed = 2
    }

    /// <summary>
    /// The KPI Level enum.
    /// </summary>
    [DataContract]
    [Serializable]
    public enum KpiLevel
    {
        /// <summary>
        /// KPI Level None.
        /// </summary>
        [EnumMember]
        None = 0,

        /// <summary>
        /// KPI Level Network.
        /// </summary>
        [EnumMember]
        Network = 1,

        /// <summary>
        /// KPI Level NetworkIn.
        /// </summary>
        [EnumMember]
        NetworkIn = 2,

        /// <summary>
        /// KPI Level NetworkOut.
        /// </summary>
        [EnumMember]
        NetworkOut = 3,

        /// <summary>
        /// KPI Level NetworkHourly.
        /// </summary>
        [EnumMember]
        NetworkHourly = 4,

        /// <summary>
        /// KPI Level NetworkInHourly.
        /// </summary>
        [EnumMember]
        NetworkInHourly = 5,

        /// <summary>
        /// KPI Level NetworkOutHourly.
        /// </summary>
        [EnumMember]
        NetworkOutHourly = 6,

        /// <summary>
        /// KPI Level User.
        /// </summary>
        [EnumMember]
        User = 7,

        /// <summary>
        /// KPI Level UserIn.
        /// </summary>
        [EnumMember]
        UserIn = 8,

        /// <summary>
        /// KPI Level UserOut.
        /// </summary>
        [EnumMember]
        UserOut = 9,

        /// <summary>
        /// KPI Level UserHourly.
        /// </summary>
        [EnumMember]
        UserHourly = 10,

        /// <summary>
        /// KPI Level UserInHourly.
        /// </summary>
        [EnumMember]
        UserInHourly = 11,

        /// <summary>
        /// KPI Level UserOutHourly.
        /// </summary>
        [EnumMember]
        UserOutHourly = 12,

        /// <summary>
        /// KPI Level Circle.
        /// </summary>
        [EnumMember]
        Circle = 13,

        /// <summary>
        /// KPI Level CircleIn.
        /// </summary>
        [EnumMember]
        CircleIn = 14,

        /// <summary>
        /// KPI Level CircleOut.
        /// </summary>
        [EnumMember]
        CircleOut = 15,

        /// <summary>
        /// KPI Level CircleHourly.
        /// </summary>
        [EnumMember]
        CircleHourly = 16,

        /// <summary>
        /// KPI Level CircleInHourly.
        /// </summary>
        [EnumMember]
        CircleInHourly = 17,

        /// <summary>
        /// KPI Level CircleOutHourly.
        /// </summary>
        [EnumMember]
        CircleOutHourly = 18,

        /// <summary>
        /// KPI Level City.
        /// </summary>
        [EnumMember]
        City = 19,

        /// <summary>
        /// KPI Level CityIn.
        /// </summary>
        [EnumMember]
        CityIn = 20,

        /// <summary>
        /// KPI Level CityOut.
        /// </summary>
        [EnumMember]
        CityOut = 21,

        /// <summary>
        /// KPI Level CityHourly.
        /// </summary>
        [EnumMember]
        CityHourly = 22,

        /// <summary>
        /// KPI Level CityInHourly.
        /// </summary>
        [EnumMember]
        CityInHourly = 23,

        /// <summary>
        /// KPI Level CityOutHourly.
        /// </summary>
        [EnumMember]
        CityOutHourly = 24,

        /// <summary>
        /// KPI Level Cell.
        /// </summary>
        [EnumMember]
        Cell = 25,

        /// <summary>
        /// KPI Level CellIn.
        /// </summary>
        [EnumMember]
        CellIn = 26,

        /// <summary>
        /// KPI Level CellOut.
        /// </summary>
        [EnumMember]
        CellOut = 27,

        /// <summary>
        /// KPI Level CellHourly.
        /// </summary>
        [EnumMember]
        CellHourly = 28,

        /// <summary>
        /// KPI Level CellInHourly.
        /// </summary>
        [EnumMember]
        CellInHourly = 29,

        /// <summary>
        /// KPI Level CellOutHourly.
        /// </summary>
        [EnumMember]
        CellOutHourly = 30
    }

    /// <summary>
    /// The Grade enum.
    /// </summary>
    [DataContract]
    [Serializable]
    public enum Grade
    {
        /// <summary>
        /// Grade enum None.
        /// </summary>
        [EnumMember]
        None = 0,

        /// <summary>
        /// Grade enum GOOD.
        /// </summary>
        [EnumMember]
        Good = 1,

        /// <summary>
        /// Grade enum BAD.
        /// </summary>
        [EnumMember]
        Bad = 2,
    }
    
    /// <summary>
    /// The KPI enum.
    /// </summary>
    [DataContract]
    [Serializable]
    public enum Kpi
    {
        /// <summary>
        /// KPI None.
        /// </summary>
        [EnumMember]
        None = 0,

        /// <summary>
        /// KPI Rssi enum.
        /// </summary>
        [EnumMember]
        Rssi = 1,

        /// <summary>
        /// KPI UL enum.
        /// </summary>
        [EnumMember]
        Ul = 2,

        /// <summary>
        /// KPI DL enum.
        /// </summary>
        [EnumMember]
        Dl = 3,

        /// <summary>
        /// KPI ExtLatency enum.
        /// </summary>
        [EnumMember]
        ExtLatency = 4,

        /// <summary>
        /// KPI IntraLatency enum.
        /// </summary>
        [EnumMember]
        IntraLatency = 5
    }

    /// <summary>
    /// The Report Level enum.
    /// </summary>
    [DataContract]
    [Serializable]
    public enum ReportLevel
    {
        /// <summary>
        /// The Report Level None.
        /// </summary>
        [EnumMember]
        None = 0,

        /// <summary>
        /// The Report Level NETWORK enum.
        /// </summary>
        [EnumMember]
        Network = 1,

        /// <summary>
        /// The Report Level IMSI enum.
        /// </summary>
        [EnumMember]
        Imsi = 2,

        /// <summary>
        /// The Report Level CIRCLE enum.
        /// </summary>
        [EnumMember]
        Circle = 3,

        /// <summary>
        /// The Report Level CITY enum.
        /// </summary>
        [EnumMember]
        City = 4,

        /// <summary>
        /// The Report Level CELL enum.
        /// </summary>
        [EnumMember]
        Cell = 5
    }
    
}
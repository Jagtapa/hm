// <copyright file="Enum.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>
// This class contains all the enum definations required for application.
// </summary>
namespace HM.Framework
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Enumeration for User Types.
    /// </summary>
    [DataContract]
    [Serializable]
    public enum UserType
    {
        /// <summary>
        /// The none user type.
        /// </summary>
        [EnumMember]
        None = 0
    }

    /// <summary>
    /// Enumeration for Sort Order
    /// </summary>
    [DataContract]
    [Serializable]
    public enum SortOrder
    {
        /// <summary>
        /// Descending order.
        /// </summary>
        [EnumMember]
        Descending = 2,

        /// <summary>
        /// Ascending order.
        /// </summary>
        [EnumMember]
        Ascending = 1
    }

    /// <summary>
    /// Email type enumeration.
    /// </summary>
    public enum EmailTypes
    {
        /// <summary>
        /// Email type None enumeration.
        /// </summary>
        [EnumMember]
        None = 0
    }

    /// <summary>
    /// Enumeration for Log Type
    /// </summary>
    [DataContract]
    [Serializable]
    public enum LogType
    {
        /// <summary>
        /// Warning log.
        /// </summary>
        [EnumMember]
        Warning,

        /// <summary>
        /// Error log.
        /// </summary>
        [EnumMember]
        Error,

        /// <summary>
        /// Information log.
        /// </summary>
        [EnumMember]
        Information

    }

    /// <summary>
    /// Enumeration for displaying Select/All in the dropdown or listbox
    /// </summary>
    [DataContract]
    [Serializable]
    public enum SelectAllOption
    {
        /// <summary>
        /// None select option None = 0.
        /// </summary>
        [EnumMember]
        None = 0,

        /// <summary>
        /// Select option select=1.
        /// </summary>
        [EnumMember]
        Select = 1,

        /// <summary>
        /// All selected. All = 2
        /// </summary>
        [EnumMember]
        All = 2
    }

    /// <summary>
    /// Enumeration for Yes/No.
    /// </summary>
    [DataContract]
    [Serializable]
    public enum OptionYesNo
    {
        /// <summary>
        /// None option. 
        /// </summary>
        [EnumMember]
        None = 2,

        /// <summary>
        /// No option. No = 0
        /// </summary>
        [EnumMember]
        No = 0,

        /// <summary>
        /// Yes option. Yes = 1
        /// </summary>
        [EnumMember]
        Yes = 1
    }

    /// <summary>
    /// Enumeration for Y/N.
    /// </summary>
    [DataContract]
    [Serializable]
    public enum OptionYN
    {
        /// <summary>
        /// N option. N = 0
        /// </summary>
        [EnumMember]
        N = 0,

        /// <summary>
        /// Y option. Y = 1
        /// </summary>
        [EnumMember]
        Y = 1
    }

    /// <summary>
    /// Enumeration for Operation Modes on a form.
    /// </summary>
    [DataContract]
    [Serializable]
    public enum Operation
    {
        /// <summary>
        /// None operation enum.
        /// </summary>
        [EnumMember]
        None = 0,

        /// <summary>
        /// Add operation  enum.
        /// </summary>
        [EnumMember]
        Add = 1,

        /// <summary>
        /// Edit operation  enum.
        /// </summary>
        [EnumMember]
        Modify = 2,

        /// <summary>
        /// Delete operation  enum.
        /// </summary>
        [EnumMember]
        Delete = 3
    }

    /// <summary>
    /// Enumeration for Contact Types
    /// </summary>
    [DataContract]
    [Serializable]
    public enum ContactType
    {
        /// <summary>
        /// None contact type.
        /// </summary>
        [EnumMember]
        None = 0,

        /// <summary>
        /// Telephone contact type.
        /// </summary>
        [EnumMember]
        Telephone = 1,

        /// <summary>
        /// Fax contact type.
        /// </summary>
        [EnumMember]
        Fax = 2,

        /// <summary>
        /// Email contact type.
        /// </summary>
        [EnumMember]
        Email = 3,

        /// <summary>
        /// MCS contact type.
        /// </summary>
        [EnumMember]
        MCS = 4,
    }

    /// <summary>
    /// Deleted flag enum to indicate deleted or not..
    /// </summary>
    [DataContract]
    [Serializable]
    public enum DeletedFlag
    {
        /// <summary>
        /// Deleted yes  enum.
        /// </summary>
        [EnumMember]
        Deleted = 1,

        /// <summary>
        /// Not deleted  enum.
        /// </summary>
        [EnumMember]
        NotDeleted = 0
    }

    /// <summary>
    /// MessageStatus enum  to indicate status of message.
    /// </summary>
    [DataContract]
    [Serializable]
    public enum MessageStatus
    {
        /// <summary>
        /// Error while sending message.
        /// </summary>
        [EnumMember]
        E = 1,

        /// <summary>
        /// New Meesage enum.
        /// </summary>
        [EnumMember]
        N = 2,

        /// <summary>
        /// message Sent enum.
        /// </summary>
        [EnumMember]
        S = 3,

        /// <summary>
        /// Awaiting response from Port.
        /// </summary>
        [EnumMember]
        W = 4,
    }

    /// <summary>
    /// Enumeration for Session values.
    /// </summary>
    [DataContract]
    [Serializable]
    public enum SessionVariableName
    {
        /// <summary>
        /// User entity enum.
        /// </summary>
        [EnumMember]
        UserEntity = 1
    }
}

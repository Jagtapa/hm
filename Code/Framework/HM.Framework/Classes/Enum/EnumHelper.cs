﻿// <copyright file="EnumHelper.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>19-07-2013</date>
// <summary> The EnumHelper class.
// </summary>

namespace HM.Framework
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    /// <summary>
    /// Class contains helper functions to read enum description and fields.
    /// </summary>
    public static class EnumHelper
    {
        /// <summary>
        /// Gets the description of the supplied enumeration, as defined by the Description attribute of the enumeration.
        /// If no Description attribute is defined, the string value of the enumeration will be returned.
        /// </summary>
        /// <param name="value">The enumeration value to get the description for</param>
        /// <returns>A description string for the supplied enumeration</returns>
        public static string GetEnumDescription(Enum value)
        {
            if (value == null)
            {
                return string.Empty;
            }

            FieldInfo fi = value.GetType().GetField(value.ToString());
            return GetDescriberText(value, fi);
        }

        /// <summary>
        /// Gets the enum from description.
        /// </summary>
        /// <param name="description">description string for the supplied enumeration.</param>
        /// <param name="passedType">Enumeration Type</param>
        /// <returns>ENum corresponding to passed description</returns>
        public static Enum GetEnumFromDescription(string description, Type passedType)
        {
            FieldInfo[] fields = passedType.GetFields();
            foreach (FieldInfo field in fields)
            {
                if (field.FieldType.Name == passedType.Name && description.Equals(GetDescriberText(field.GetValue(passedType) as Enum, field)))
                {
                    return field.GetValue(passedType) as Enum;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the enum fileds with description.
        /// </summary>
        /// <param name="passedType">Type of the passed Enum.</param>
        /// <returns>Dictionary of field as key and description as value</returns>
        public static Dictionary<string, string> GetEnumFieldsWithDescription(Type passedType)
        {
            FieldInfo[] fields = passedType.GetFields();
            Dictionary<string, string> results = new Dictionary<string, string>();

            foreach (FieldInfo field in fields)
            {
                if (field.FieldType.Name == passedType.Name)
                {
                    results.Add(field.Name, GetEnumDescription(field.GetValue(passedType) as Enum));
                }
            }

            return results;
        }

        /// <summary>
        /// Reads the custom attribute 'Description' to return its value
        /// </summary>
        /// <param name="value">Enum for which description is to be read</param>
        /// <param name="fi">Instance of custom Attribute provider</param>
        /// <returns>Description of the Enum Field</returns>
        private static string GetDescriberText(Enum value, ICustomAttributeProvider fi)
        {
            DescriberAttribute[] attributes = (DescriberAttribute[])fi.GetCustomAttributes(typeof(DescriberAttribute), false);

            return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
        }
    }
}

﻿// <copyright file="DescriberAttribute.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>19-07-2013</date>
// <summary> The describer custom attribute class.
// </summary>

namespace HM.Framework
{
    using System;

    /// <summary>
    /// The describer custom attribute class.
    /// </summary>
    public sealed class DescriberAttribute : Attribute
    {
        /// <summary>
        /// The description property.
        /// </summary>
        private readonly string description;

        /// <summary>
        /// Initializes a new instance of the DescriberAttribute class.
        /// </summary>
        /// <param name="description">
        /// A description for the constant.
        /// </param>
        public DescriberAttribute(string description)
        {
            this.description = description;
        }

        /// <summary>
        /// Gets the description for the constant.
        /// </summary>
        public string Description
        {
            get { return this.description; }
        }
    }
}

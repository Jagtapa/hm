// <copyright file="StateManager.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class is used to manage the state of application..
// </summary>
namespace HM.Framework
{
    using System.Collections;
    using System.Web;

    /// <summary>
    /// This class will manage the state/session values in the application.
    /// Methods will be static so that they can be accessed without instantiaing
    /// the class.
    /// </summary>
    public static class StateManager
    {
        #region Private Variables
        /// <summary>
        /// state hash table.
        /// </summary>
        private static Hashtable stateHashtable = new Hashtable();
        #endregion

        #region Public Methods
        /// <summary>
        /// Adds a value to session.
        /// </summary>
        /// <param name="sessionVariableName">The name with which the value will be stored in Session</param>
        /// <param name="value">The value to be stored in session</param>
        public static void AddToSession(this SessionVariableName sessionVariableName, object value)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Session[sessionVariableName.ToString()] = value;
            }
            else
            {
                if (stateHashtable.ContainsKey(sessionVariableName.ToString()))
                {
                    stateHashtable.Remove(sessionVariableName.ToString());
                }

                stateHashtable.Add(sessionVariableName.ToString(), value);
            }
        }

        /// <summary>
        /// Adds a value to session.
        /// </summary>
        /// <param name="session">The name with which the value will be stored in Session</param>
        /// <param name="value">The value to be stored in session</param>
        public static void AddToSession(this string session, object value)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Session[session] = value;
            }
            else
            {
                if (stateHashtable.ContainsKey(session))
                {
                    stateHashtable.Remove(session);
                }

                stateHashtable.Add(session, value);
            }
        }

        /// <summary>
        /// Retrieves the session value for the specified session variable.
        /// The returned value has to be type casted to it's requisite type.
        /// </summary>
        /// <param name="sessionVariableName">The name of the session variable</param>
        /// <returns>The value of the session variable</returns>
        public static object GetFromSession(this SessionVariableName sessionVariableName)
        {
            if (HttpContext.Current != null)
            {
                return HttpContext.Current.Session[sessionVariableName.ToString()];
            }
            else
            {
                if (stateHashtable.ContainsKey(sessionVariableName.ToString()))
                {
                    return stateHashtable[sessionVariableName.ToString()];
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Retrieves the session value for the specified session variable.
        /// The returned value has to be type casted to it's requisite type.
        /// </summary>
        /// <param name="session">The name of the session variable</param>
        /// <returns>The value of the session variable</returns>
        public static object GetFromSession(this string session)
        {
            if (HttpContext.Current != null)
            {
                return HttpContext.Current.Session[session];
            }
            else
            {
                if (stateHashtable.ContainsKey(session))
                {
                    return stateHashtable[session];
                }
                else
                {
                    return null;
                }
            }
        }
        #endregion
    }
}

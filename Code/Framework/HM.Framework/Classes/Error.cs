// <copyright file="Error.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class used to maintain errors.
// </summary>
namespace HM.Framework
{
    using System;
    using System.Collections.Specialized;
    using System.Runtime.Serialization;

    /// <summary>
    /// This class holds a collection of Errors (Error IDs and respective Error Messages).
    /// </summary>
    /// <remarks>
    /// This is the error class. the intended use for this class is to store
    /// error messages that need to be displayed to the user. This implies that all
    /// errors are be displayed to the user at one go. This class is a collection
    /// of Name-Value pairs which stores the Error ID as the Name and the Error
    /// Message as the Value.
    /// <seealso cref="NameValueCollection"/>
    /// </remarks>
    [DataContract]
    [Serializable]
    public class Error
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the Error class.
        /// </summary>
        public Error()
        {
            this.ErrorCollection = new EntityCollectionBase<ErroIdValue>();
        } 
        #endregion

        #region Private Properties
        /// <summary>
        /// Gets or sets the The error collection.
        /// </summary>
        [DataMember]
        private IEntityCollectionBase ErrorCollection
        {
            get;
            set;
        } 
        #endregion

        #region Public Methods
        /// <summary>
        /// This function is used to log an error to the object of <see cref="Error"/>
        /// class. The function reads the relative error message from the error
        /// resource file based on the Error ID.
        /// </summary>
        /// <param name="errorID">The Error ID for which the relevant error message
        /// need to be logged.</param>
        public void Add(string errorID)
        {
            this.ErrorCollection.Add(new ErroIdValue(errorID, ResourcesManager.GetResourceString(errorID)));
        }

        /// <summary>
        ///  This method is used to log an parameterized error.
        /// </summary>
        /// <param name="errorID">string errorID.</param>
        /// <param name="errorParams">ErrorParams errorParams</param>
        public void Add(string errorID, ErrorParams errorParams)
        {
            this.ErrorCollection.Add(new ErroIdValue(errorID, string.Format(ResourcesManager.GetResourceString(errorID), errorParams.FromResource ? ResourcesManager.GetResourceString(errorParams.Param) : errorParams.Param)));
        }

        /// <summary>
        /// This method is used to log an parameterized error. If parameter are more than one.
        /// </summary>
        /// <param name="errorID">string errorID.</param>
        /// <param name="errorParams">ErrorParams[] errorParams.</param>
        public void Add(string errorID, ErrorParams[] errorParams)
        {
            string[] parameters = new string[errorParams.Length];
            int count = 0;

            foreach (ErrorParams errorParam in errorParams)
            {
                parameters[count] = errorParam.FromResource ? ResourcesManager.GetResourceString(errorParam.Param) : errorParam.Param;
                count++;
            }

            string errorString = ResourcesManager.GetResourceString(errorID);
            int counter = 0;

            while (true)
            {
                if (errorString.IndexOf("{" + counter.ToString() + "}") == -1)
                {
                    break;
                }

                counter++;
            }

            if (counter == parameters.Length)
            {
                errorString = string.Format(errorString, parameters);
            }
            else
            {
                errorString = errorString + " < No. of supplied parameters doesn't match string > ";
            }

            this.ErrorCollection.Add(new ErroIdValue(errorID, errorString));
        }

        /// <summary>
        /// Add error into erro list.
        /// </summary>
        /// <param name="errorID">An error ID.</param>
        /// <param name="value">Error value.</param>
        public void Add(string errorID, string value)
        {
            this.ErrorCollection.Add(new ErroIdValue(errorID, value));
        }

        /// <summary>
        /// Method added for adding non resource string
        /// </summary>
        /// <param name="error">string constant for error</param>
        public void AddNonResource(string error)
        {
            this.ErrorCollection.Add(new ErroIdValue(error, error));
        }

        /// <summary>
        /// This method will return all the Error Messages delimited by ";".
        /// </summary>
        /// <returns>String of Error Messages</returns>
        public string GetAll()
        {
            //// use the overloaded version with default delimiter.
            return this.GetAll("; ");
        }

        /// <summary>
        /// This method will return all the Error Messages delimited by sDelimiter.
        /// </summary>
        /// <param name="delimiter">Delimiter String</param>
        /// <returns>String of Error Messages</returns>
        public string GetAll(string delimiter)
        {
            string allErrors = null;

            for (int count = 0; count < this.ErrorCollection.Count; ++count)
            {
                allErrors += (count > 0 ? delimiter : string.Empty) + (this.ErrorCollection[count] as ErroIdValue).ErrorValue;
            }

            return allErrors;
        }

        /// <summary>
        /// This function returns the error message logged in the object of <see cref="Error"/>
        /// class w.r.t. to a particular Error ID.
        /// </summary>
        /// <param name="errorID">The Error ID for which the error message
        /// is required.</param>
        /// <returns>The error message for the desired Error ID.</returns>
        public string Get(string errorID)
        {
            foreach (ErroIdValue erroIdValue in this.ErrorCollection)
            {
                if (erroIdValue.ErrorId == errorID)
                {
                    return erroIdValue.ErrorValue;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// This function returns the error message logged in the object of <see cref="Error"/>
        /// class w.r.t. to a particular Index.
        /// </summary>
        /// <param name="index">The Index for which the error message
        /// is required.</param>
        /// <returns>The error message at the desired Index.</returns>
        public string Get(int index)
        {
            return (this.ErrorCollection[index] as ErroIdValue).ErrorValue;
        }

        /// <summary>
        /// This function returns the error ID logged in the object of <see cref="Error"/>
        /// class w.r.t. to a particular Index.
        /// </summary>
        /// <param name="index">The Index for which the error ID
        /// is required.</param>
        /// <returns>The error ID at the desired Index.</returns>
        public string GetErrorID(int index)
        {
            return (this.ErrorCollection[index] as ErroIdValue).ErrorId;
        }

        /// <summary>
        /// Gets the count of Errors logged.
        /// </summary>
        /// <returns>The Error Count.</returns>
        public int GetCount()
        {
            return this.ErrorCollection.Count;
        }

        /// <summary>
        /// Removes the error from the Error object based on the Error ID.
        /// </summary>
        /// <param name="errorId">The Error ID for which the error message
        /// is to be removed.</param>
        public void RemoveErrorId(string errorId)
        {
            foreach (ErroIdValue nameValue in this.ErrorCollection)
            {
                if (nameValue.ErrorId == errorId)
                {
                    this.ErrorCollection.Remove(nameValue);
                    break;
                }
            }
        } 
        #endregion
    }
}

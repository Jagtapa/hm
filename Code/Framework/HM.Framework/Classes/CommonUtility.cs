// <copyright file="CommonUtility.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class contains all the common methods required.
// </summary>
namespace HM.Framework
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Transactions;
    using System.Web.UI.WebControls;
    using Newtonsoft.Json;

    /// <summary>
    /// This class consists of following static methods for common tasks to be carried out. The class is extensible means it can have more static methods.
    /// </summary>
    public static class CommonUtility
    {
        #region Public Methods
        /// <summary>
        /// Convert the custom class object into XML.
        /// </summary>
        /// <param name="userObject">The object that needs to be converted into XML</param>
        /// <returns>XML representation of the object</returns>
        public static string ConvertToXML(this object userObject)
        {
            StringBuilder output = new StringBuilder();

            StringBuilder refOutput = new StringBuilder();

            Type objectBaseType = userObject.GetType().BaseType;

            int count = (objectBaseType == typeof(object) || objectBaseType == typeof(EntityBase)) ? 1 : (userObject as IEntityCollectionBase).Count;

            output.Append((objectBaseType == typeof(object) || objectBaseType == typeof(EntityBase)) ? "<" + userObject.GetType().Name + ">" : "<Objects>");

            for (int i = 0; i <= count - 1; i++)
            {
                PropertyInfo[] objectFields = (objectBaseType == typeof(object) || objectBaseType == typeof(EntityBase)) ? userObject.GetType().GetProperties() : (userObject as IEntityCollectionBase)[i].GetType().GetProperties();
                object tempObject = (objectBaseType == typeof(object) || objectBaseType == typeof(EntityBase)) ? userObject : (userObject as IEntityCollectionBase)[i] as object;
                output.Append("<" + objectFields[0].ReflectedType.Name.ToString());
                foreach (PropertyInfo prop in objectFields)
                {
                    if (prop.CanRead)
                    {
                        if ((!prop.PropertyType.IsValueType) && (prop.PropertyType != typeof(string)) && (prop.PropertyType != typeof(Error)))
                        {
                            object customObject = prop.GetValue(tempObject as object, null) as object;
                            if (null != customObject)
                            {
                                refOutput.Append(ConvertToXML(customObject));
                            }
                        }
                        else
                        {
                            object strObjectString = prop.GetValue(tempObject, null);
                            if (null != strObjectString)
                            {
                                output.Append(" " + prop.Name.ToString() + "=\"");
                                strObjectString = strObjectString.ToString().Replace("&", "&amp;");
                                strObjectString = strObjectString.ToString().Replace("<", "&lt;");
                                strObjectString = strObjectString.ToString().Replace(">", "&gt;");
                                output.Append(strObjectString.ToString() + "\"");
                            }
                        }
                    }
                }

                if (refOutput.Length > 0)
                {
                    output.Append(">");
                    output.Append(refOutput.ToString());
                    refOutput.Remove(0, refOutput.Length);
                    output.Append("</" + objectFields[0].ReflectedType.Name.ToString() + ">");
                }
                else
                {
                    output.Append("/>");
                }
            }

            output.Append((objectBaseType == typeof(object) || objectBaseType == typeof(EntityBase)) ? "</" + userObject.GetType().Name + ">" : "</Objects>");
            return output.ToString();
        }

        /// <summary>
        /// Removes the specified string value from the drop down list.
        /// </summary>
        /// <param name="dropDownList">The drop down list from which to remove the value</param>
        /// <param name="value">The value to be removed from the drop down list</param>
        public static void RemoveFromDropdown(this DropDownList dropDownList, string value)
        {
            dropDownList.Items.Remove(dropDownList.Items.FindByValue(value));
        }

        /// <summary>
        /// Removes the specified integer value from the drop down list.
        /// </summary>
        /// <param name="dropDownList">The drop down list from which to remove the value</param>
        /// <param name="value">The value to be removed from the drop down list</param>
        public static void RemoveFromDropdown(this DropDownList dropDownList, int value)
        {
            dropDownList.Items.Remove(dropDownList.Items.FindByValue(value.ToString()));
        }

        /// <summary>
        /// Fills a drop down list with numbers
        /// </summary>
        /// <param name="dropDownList">The drop down list to be populated</param>
        /// <param name="upperLimit">The number upto which drop down is to be populated</param>
        public static void FillDropDownFromNumber(this DropDownList dropDownList, int upperLimit)
        {
            for (int count = 1; count <= upperLimit; count++)
            {
                dropDownList.Items.Add(new ListItem(count.ToString(), count.ToString()));
            }
        }

        /// <summary>
        /// Fills a drop down list with numbers, allowing selection of all values
        /// </summary>
        /// <param name="dropDownList">The drop down list to be populated</param>
        /// <param name="upperLimit">The number upto which drop down is to be populated</param>
        /// <param name="selectAll">Indicator specifying if "All" to be added to the list</param>
        public static void FillDropDownFromNumber(this DropDownList dropDownList, int upperLimit, SelectAllOption selectAll)
        {
            FillDropDownFromNumber(dropDownList, upperLimit);
        }

        /// <summary>
        /// Fills a drop down list with numbers, allowing selection of all values
        /// and a value to be selected by default
        /// </summary>
        /// <param name="dropDownList">The drop down list to be populated</param>
        /// <param name="upperLimit">The number upto which drop down is to be populated</param>
        /// <param name="selectAllOption">Indicator specifying if "All" to be added to the list</param>
        /// <param name="selectedIndex">Value to be selected by default in the list</param>
        public static void FillDropDownFromNumber(this DropDownList dropDownList, int upperLimit, SelectAllOption selectAllOption, string selectedIndex)
        {
            FillDropDownFromNumber(dropDownList, upperLimit);
            dropDownList.SelectedIndex = dropDownList.Items.IndexOf(dropDownList.Items.FindByValue(selectedIndex));
        }

        /// <summary>
        /// Selects the specified string value in the drop down list
        /// </summary>
        /// <param name="drp">The drop down list containing the value to be selected</param>
        /// <param name="strValue">The value to be selected in the drop down</param>
        public static void SetDropdownIndex(this DropDownList drp, string strValue)
        {
            drp.SelectedIndex = drp.Items.IndexOf(drp.Items.FindByValue(strValue));
        }

        /// <summary>
        /// Selects the specified integer value in the drop down list
        /// </summary>
        /// <param name="dropDownList">The drop down list containing the value to be selected</param>
        /// <param name="value">The value to be selected in the drop down</param>
        public static void SetDropdownIndex(this DropDownList dropDownList, int value)
        {
            dropDownList.SelectedIndex = dropDownList.Items.IndexOf(dropDownList.Items.FindByValue(value.ToString()));
        }

        /// <summary>
        /// This to replace the "_" and "%" from the given string, it checks first character of the given string. for e.g '_Hello' will be replaced with '[_]Hello'.
        /// </summary>
        /// <param name="strValue">the string to remove wild characters.</param>
        /// <returns>the string to removed wild characters.</returns>
        public static string RemoveSqlWildChar(this string strValue)
        {
            string strFirstChar = strValue.Substring(0, 1);
            if ((strFirstChar == "%") || (strFirstChar == "_"))
            {
                return strValue.Replace(strFirstChar, "[" + strFirstChar + "]");
            }
            else
            {
                return strValue;
            }
        }

        /// <summary>
        /// Gets the string representation of a given enumeration
        /// </summary>
        /// <param name="enumParam">The enumeration collection from which value is required</param>
        /// <param name="value">The enumeration value of which the string value is required</param>
        /// <returns>String representation of an enumeration</returns>
        public static string GetStringFromEnum(this object enumParam, int value)
        {
            return ResourcesManager.GetResourceString(Enum.GetName(enumParam.GetType(), value));
        }

        /// <summary>
        /// To Prevent SQL Injection Attacks.
        /// </summary>
        /// <param name="query">The query to replace double quotes.</param>
        /// <returns>The query with replaced double quotes.</returns>
        public static string ReplaceSingleQuotes(this string query)
        {
            query = !string.IsNullOrEmpty(query) ? query.Replace("'", "`") : string.Empty;
            return query;
        }

        /// <summary>
        /// Replace comma with semicolan.
        /// </summary>
        /// <param name="property">The value to Replace comma with semicolan.</param>
        /// <returns>The value with replaced comma.</returns>
        public static string ReplaceCommaWithSemicolan(this string property)
        {
            property = !string.IsNullOrEmpty(property) ? property.Replace(",", ";") : string.Empty;
            return property;
        }

        /// <summary>
        /// Gets XML representation of the collection object
        /// </summary>
        /// <param name="entityCollectionBase">The collection object of which XML data is required</param>
        /// <returns>XML representation of the collection object</returns>
        public static string GetXMLDataForRemoveList(this IEntityCollectionBase entityCollectionBase)
        {
            return GetXMLData(entityCollectionBase);
        }

        /// <summary>
        /// Gets XML representation of the collection object
        /// </summary>
        /// <param name="entityCollectionBase">The collection object of which XML data is required</param>
        /// <returns>XML representation of the collection object</returns>
        public static string GetXMLData(this IEntityCollectionBase entityCollectionBase)
        {
            System.Text.StringBuilder xmlText = new System.Text.StringBuilder();
            string retValue = null;
            xmlText.Append("<data>");
            for (int collectionItemCount = 0; collectionItemCount <= entityCollectionBase.Count - 1; collectionItemCount++)
            {
                retValue = entityCollectionBase[collectionItemCount].ToString();
                xmlText.Append(retValue);
            }

            xmlText.Append("</data>");
            return xmlText.ToString();
        }

        /// <summary>
        /// Converts a Hashtable to a DataTable
        /// </summary>
        /// <param name="sourceHashtable">The hash table to be converted</param>
        /// <param name="keyColumnName">The column name which will have the key</param>
        /// <param name="valueColumnName">The column name which will have the value</param>
        /// <returns>The data table.</returns>
        public static DataTable GetDataTable(this Hashtable sourceHashtable, string keyColumnName, string valueColumnName)
        {
            string keyColumn = keyColumnName;
            string valueColumn = valueColumnName;
            DataTable dataTable = null;
            DataColumn dataColumn = null;
            DataRow dataRow = null;
            IDictionaryEnumerator hashtableEnumerator = null;
            if (keyColumn == null || keyColumn == string.Empty)
            {
                keyColumn = "Key";
            }

            if (valueColumn == null || valueColumn == string.Empty)
            {
                valueColumn = "Value";
            }

            if (sourceHashtable != null)
            {
                dataTable = new DataTable();
                Type stringColumn = System.Type.GetType("System.String");
                dataColumn = new DataColumn(keyColumn, stringColumn);
                dataTable.Columns.Add(dataColumn);
                dataColumn = new DataColumn(valueColumn, stringColumn);
                dataTable.Columns.Add(dataColumn);
                hashtableEnumerator = sourceHashtable.GetEnumerator();
                while (hashtableEnumerator.MoveNext())
                {
                    dataRow = dataTable.NewRow();
                    DictionaryEntry de = (DictionaryEntry)hashtableEnumerator.Current;
                    dataRow[keyColumn] = de.Key;
                    dataRow[valueColumn] = de.Value;
                    dataTable.Rows.Add(dataRow);
                }
            }

            return dataTable;
        }

        /// <summary>
        /// Sorts a DataTable
        /// </summary>
        /// <param name="dataTable">The DataTable to be sorted</param>
        /// <param name="sortingColumnName">The column name on which to sort</param>
        /// <param name="isAscending">Flag indicating if sorting is in ascending order</param>
        public static void SortDataTable(this DataTable dataTable, string sortingColumnName, bool isAscending)
        {
            string sortColumn = sortingColumnName;
            const string DESC = " DESC";
            if (dataTable != null && sortColumn != null)
            {
                if (!isAscending)
                {
                    sortColumn += DESC;
                }

                dataTable.DefaultView.Sort = sortColumn;
            }
        }

        /// <summary>
        /// Binds a ListControl to a DataTable
        /// </summary>
        /// <param name="targetList">The ListControl to be bound to</param>
        /// <param name="dataTable">The DataTable to bind to the ListControl</param>
        /// <param name="keyColumnName">The key column name</param>
        /// <param name="valueColumnName">The value column name</param>
        public static void BindListControl(this ListControl targetList, DataTable dataTable, string keyColumnName, string valueColumnName)
        {
            if (dataTable != null && targetList != null)
            {
                targetList.Items.Clear();
                targetList.DataTextField = valueColumnName;
                targetList.DataValueField = keyColumnName;
                targetList.DataSource = dataTable;
                targetList.DataBind();
            }
        }

        /// <summary>
        /// Alphabetes , Space , ', and Numbers
        /// </summary>
        /// <param name="strToCheck">string to validate.</param>
        /// <returns>true or false</returns>
        public static bool IsAlphaNumeric(this string strToCheck)
        {
            Regex objAlphaNumericPattern = new Regex("[^a-zA-Z0-9' ] ");
            return !objAlphaNumericPattern.IsMatch(strToCheck);
        }

        /// <summary>
        /// Alphabetes and Numbers
        /// </summary>
        /// <param name="strToCheck">string to validate.</param>
        /// <returns>true or false</returns>
        public static bool IsAlphaNumericWithNoSpace(this string strToCheck)
        {
            Regex objAlphaNumericPattern = new Regex("^[a-zA-Z0-9]*$");
            return objAlphaNumericPattern.IsMatch(strToCheck);
        }

        /// <summary>
        /// Alphabetes and '
        /// </summary>
        /// <param name="strToCheck">string to validate.</param>
        /// <returns>true or false</returns>
        public static bool IsAlphabates(this string strToCheck)
        {
            Regex objAlphaNumericPattern = new Regex("[^a-zA-Z']");
            return !objAlphaNumericPattern.IsMatch(strToCheck);
        }

        /// <summary>
        /// Only Integer 
        /// </summary>
        /// <param name="strToCheck">string to validate.</param>
        /// <returns>true or false</returns>
        public static bool IsInteger(this string strToCheck)
        {
            Regex objAlphaNumericPattern = new Regex("[^0-9]");
            return !objAlphaNumericPattern.IsMatch(strToCheck);
        }

        /// <summary>
        /// Alphabates only and Space. No '
        /// </summary>
        /// <param name="strToCheck">string to validate.</param>
        /// <returns>true or false</returns>
        public static bool IsAlphabatesOnly(this string strToCheck)
        {
            Regex objAlphaNumericPattern = new Regex("[^a-zA-Z] ");
            return !objAlphaNumericPattern.IsMatch(strToCheck);
        }

        /// <summary>
        /// Alphabates only and Space. No '
        /// </summary>
        /// <param name="strToCheck">string to validate.</param>
        /// <returns>true or false</returns>
        public static bool IsAlphabatesOnlyNoSpace(this string strToCheck)
        {
            Regex objAlphaNumericPattern = new Regex("[^a-zA-Z]");
            return !objAlphaNumericPattern.IsMatch(strToCheck);
        }

        /// <summary>
        /// Validates given E-Mail Address
        /// </summary>
        /// <param name="strToCheck">EMail Address</param>
        /// <returns>true if valid</returns>
        public static bool IsValidEmail(this string strToCheck)
        {
            return Regex.IsMatch(strToCheck, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{1,4}|[0-9]{1,3})(\]?)$");
        }

        /// <summary>
        /// Validates commaseparated email address given.
        /// </summary>
        /// <param name="emailAddresses">Comma separated email addresses</param>
        /// <returns>true if all are valid</returns>        
        public static bool ValidateEmailAddresses(this string emailAddresses)
        {
            return emailAddresses.ValidateEmailAddresses(new char[] { ',' });
        }

        /// <summary>
        /// Validates email address given with specified separators.
        /// </summary>
        /// <param name="emailAddresses">Email addresses</param>
        /// <param name="separators">Separator chars</param>
        /// <returns>true if all are valid, else false</returns>
        public static bool ValidateEmailAddresses(this string emailAddresses, char[] separators)
        {
            // check if there are any email addresses. if not return false.
            if (emailAddresses.Replace(',', ' ').Trim().Length == 0)
            {
                return false;
            }

            // split the recieved parameter and validate each string.
            foreach (string emailAddress in emailAddresses.Split(separators, StringSplitOptions.RemoveEmptyEntries))
            {
                // if any one of the given email addresses fail validation, return false.
                if (!CommonUtility.IsValidEmail(emailAddress))
                {
                    return false;
                }
            }

            // all are valid email addresses. return true.
            return true;
        }

        /// <summary>
        /// This function will return value and name of types in enum as key - value pair in hashtable
        /// </summary>
        /// <param name="enumParam">object enum Param.</param>
        /// <returns>The Hashtable.</returns>
        public static Hashtable GetHashTableFromEnum(this object enumParam)
        {
            Hashtable hashtable = new Hashtable();
            foreach (int enumValue in (int[])Enum.GetValues(enumParam.GetType()))
            {
                hashtable.Add(enumValue, Enum.GetName(enumParam.GetType(), enumValue));
            }

            return hashtable;
        }

        /// <summary>
        /// Get Required transaction options.
        /// </summary>
        /// <returns>Required transaction options</returns>
        public static TransactionOptions GetRequireTransactionOptions()
        {
            TransactionOptions transactionOptions = new TransactionOptions();
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;
            transactionOptions.Timeout = TimeSpan.FromMinutes(Convert.ToInt32(ConfigManager.ReadAppSettings("REQUIREDTANSTIMEOUT")));
            return transactionOptions;
        }

        /// <summary>
        /// Get Supported transaction options.
        /// </summary>
        /// <returns>Supported transaction options.</returns>
        public static TransactionOptions GetSuppresTransactionOptions()
        {
            TransactionOptions transactionOptions = new TransactionOptions();
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;
            transactionOptions.Timeout = TimeSpan.FromMinutes(Convert.ToInt32(ConfigManager.ReadAppSettings("SUPPORTEDTANSTIMEOUT")));
            return transactionOptions;
        }

        /// <summary>
        /// Wrapper method to close data reader.
        /// </summary>
        /// <param name="dataReader">IDataReader dataReader</param>
        public static void CloseDataReader(this IDataReader dataReader)
        {
            if (dataReader != null && !dataReader.IsClosed)
            {
                dataReader.Close();
                dataReader.Dispose();
            }
        }

        /// <summary>
        /// Converts object to int32.
        /// </summary>
        /// <param name="parameter">The object to convert in to int.</param>
        /// <returns>The integer value.</returns>
        public static int ObjectToInt32(this object parameter)
        {
            int value = 0;
            int.TryParse(ObjectToString(parameter), out value);
            return value;
        }

        /// <summary>
        /// Converts object to long.
        /// </summary>
        /// <param name="parameter">The object to convert in to long.</param>
        /// <returns>The int64 long value.</returns>
        public static long ObjectToInt64(this object parameter)
        {
            long value = 0;
            long.TryParse(ObjectToString(parameter), out value);
            return value;
        }

        /// <summary>
        /// Converts object to datetime.
        /// </summary>
        /// <param name="parameter">The object to convert in to date time.</param>
        /// <returns>The date time value.</returns>
        public static DateTime? ObjectToDateTime(this object parameter)
        {
            DateTime value;
            if (parameter == null || parameter == DBNull.Value)
            {
                return null;
            }

            DateTime.TryParse(ObjectToString(parameter), out value);
            return value;
        }

        /// <summary>
        /// Converts object to datetime.
        /// </summary>
        /// <param name="parameter">The object to convert in to date time.</param>
        /// <param name="stringFormat">The date time format.</param>
        /// <returns>The date time value.</returns>
        public static DateTime? ObjectToExactFormatDateTime(this object parameter, string stringFormat)
        {
            return DateTime.ParseExact(Convert.ToString(parameter), stringFormat, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts string to date in the specified format.
        /// </summary>
        /// <param name="date">The date to convert.</param>
        /// <param name="dateTimeFormat">The date time format.</param>
        /// <returns>The date time value.</returns>
        public static DateTime? ObjectToDateTime(object date, string dateTimeFormat)
        {
            if (!string.IsNullOrEmpty(dateTimeFormat))
            {
                string datetime = date.ToString();
                string dateFormatted = ValidateAndFormatDate(datetime);
                DateTimeFormatInfo dfi = new DateTimeFormatInfo();
                return DateTime.ParseExact(dateFormatted, dateTimeFormat, dfi);
            }

            return date.ObjectToDateTime();
        }

        /// <summary>
        /// Converts string to date in the specified format.
        /// </summary>
        /// <param name="date">The date to convert.</param>
        /// <param name="dateTimeFormat">The date time format.</param>
        /// <returns>The date time value.</returns>
        public static string ObjectToDateTimeString(this object date, string dateTimeFormat)
        {
            return Convert.ToDateTime(date).ToString(dateTimeFormat);
        }

        /// <summary>
        /// This method will validate and format date
        /// </summary>
        /// <param name="date">Specify date</param>
        /// <returns>return default screen format date, if input string date is valid, Empty otherwise</returns>
        public static string ValidateAndFormatDate(string date)
        {
            string[] dividerValues = { " ", "/", "_", ".", "," };
            string mrpSeperator = "-";

            // Since the formats does not accept Whitespace, replacing the whitespace in the format with hyphen 
            for (int dividerCount = 0; dividerCount < dividerValues.Length; dividerCount++)
            {
                if (date.Contains(dividerValues[dividerCount]))
                {
                    date = date.Replace(dividerValues[dividerCount], mrpSeperator);
                    break;
                }
            }

            string[] formats = new string[] { "yyyyMMdd", "d-M-yyyy", "d-M-yy", "d-MM-yy", "d-MM-yyyy", "d-MMM-yy", "dd-M-yyyy", "dd-M-yy", "dd-MMM-yy", "dd-MM-yy", "dd-MM-yyyy", "d-MMM-yyyy", "dd-MMM-yyyy", "dMyyyy", "dMyy", "dMMyy", "dMMyyyy", "dMMMyy", "ddMyyyy", "ddMyy", "ddMMMyy", "ddMMyy", "ddMMyyyy", "dMMMyyyy", "ddMMMyyyy" };
            DateTimeFormatInfo dfi = new DateTimeFormatInfo();
            try
            {
                DateTime dateTime = DateTime.ParseExact(date, formats, dfi, DateTimeStyles.None);
                if (dateTime.Year < 1901)
                {
                    return string.Empty;
                }
                else
                {
                    return dateTime.ToString(Global.CONSTSTRDATEDEFAULTFORMAT);
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Converts object to decimal.
        /// </summary>
        /// <param name="parameter">The object to convert in to decimal.</param>
        /// <returns>The decimal value.</returns>
        public static decimal ObjectToDecimal(this object parameter)
        {
            decimal value = 0;
            decimal.TryParse(ObjectToString(parameter), out value);
            return value;
        }

        /// <summary>
        /// Converts object to bool.
        /// </summary>
        /// <param name="parameter">The object to convert in to bool.</param>
        /// <returns>The boolean value.</returns>
        public static bool ObjectToBoolean(this object parameter)
        {
            bool value = false;
            bool.TryParse(ObjectToString(parameter), out value);
            return value;
        }

        /// <summary>
        /// Converts object to database format datetime.
        /// </summary>
        /// <param name="parameter">The object to convert in to date time.</param>
        /// <returns>The date time value.</returns>
        public static string ObjectToDBDateTime(this DateTime parameter)
        {
            return parameter.ToString(Global.CONSTFORMATDATETIMESQL101);
        }

        /// <summary>
        /// Converts object to database format datetime with time as 23:59:59.
        /// </summary>
        /// <param name="parameter">The object to convert in to date time.</param>
        /// <returns>The date time value.</returns>
        public static string ObjectToDBEndDate(this DateTime parameter)
        {
            return parameter.ToString(Global.CONSTFORDBENDTIME);
        }

        /// <summary>
        /// Converts object to database format date with time 00:00:00.
        /// </summary>
        /// <param name="parameter">The object to convert in to date.</param>
        /// <returns>The date value.</returns>
        public static string ObjectToDBDate(this DateTime parameter)
        {
            return parameter.ToString(Global.CONSTFORMATDATESQL121);
        }

        /// <summary>
        /// Converts object to string.
        /// </summary>
        /// <param name="parameter">The object to convert in to string.</param>
        /// <returns>The string value.</returns>
        public static string ObjectToString(this object parameter)
        {
            return Convert.ToString(parameter);
        }

        /// <summary>
        /// Converts object to string.
        /// </summary>
        /// <param name="parameter">The object to convert in to string.</param>
        /// <param name="stringFormat">The string format.</param>
        /// <returns>The string value.</returns>
        public static string ObjectToString(this object parameter, string stringFormat)
        {
            if (string.IsNullOrEmpty(Convert.ToString(parameter)))
            {
                return Convert.ToString(parameter);
            }
            else
            {
                return string.Format(stringFormat, parameter);
            }
        }

        /// <summary>
        /// Add an error in to result object. 
        /// </summary>
        /// <param name="result">The result object.</param>
        /// <param name="error">The error parameter.</param>
        public static void AddError(this Result result, string error)
        {
            try
            {
                string message = ResourcesManager.GetResourceString(error);
                if (string.IsNullOrEmpty(message))
                {
                    result.Error.Add(message);
                }
                else
                {
                    result.Error.AddNonResource(error);
                }
            }
            catch
            {
                result.Error.AddNonResource(error);
            }
        }

        /// <summary>
        /// Serialize the given object 
        /// </summary>
        /// <param name="parameter">The object to convert in to json string.</param>
        /// <returns>The JSON string.</returns>
        public static string ConvertToJsonString(this object parameter)
        {
            var jsonSerializer = new JsonSerializer()
            {
                NullValueHandling = NullValueHandling.Include,
                MissingMemberHandling = MissingMemberHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                DateFormatString = "dd-MM-yyyy hh:mm:ss",
                DateParseHandling = DateParseHandling.DateTime
            };

            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.Converters = new List<JsonConverter>() { new JSONCustomDateConverter("yyyy-MM-dd HH:mm:ss") };
            jsonSerializerSettings.NullValueHandling = NullValueHandling.Include;
            jsonSerializerSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
            jsonSerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Serialize;
            jsonSerializerSettings.MaxDepth = int.MaxValue;

            string jsonString = JsonConvert.SerializeObject(parameter, jsonSerializerSettings);
            return jsonString;
        }

        /// <summary>
        /// De-Serialize the given object 
        /// </summary>
        /// <param name="jsonString">The JSON string to convert in to object.</param>
        /// <param name="type">The type of object.</param>
        /// <returns>The converted object.</returns>
        public static object JsonToObject(this string jsonString, Type type)
        {
            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.Converters = new List<JsonConverter>() { new JSONCustomDateConverter("yyyy-MM-dd HH:mm:ss") };
            jsonSerializerSettings.NullValueHandling = NullValueHandling.Include;
            jsonSerializerSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
            jsonSerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Serialize;
            object convertedObject = JsonConvert.DeserializeObject(jsonString, type, jsonSerializerSettings);
            return convertedObject;
        }

        /// <summary>
        /// Gets the end date format date time.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns>the end date format date time</returns>
        public static DateTime? GetDateAsEndDate(this DateTime dateTime)
        {
            return dateTime.Date.Add(TimeSpan.Parse(Global.ENDTIME));
        }

        /// <summary>
        /// Move file to specified directory.
        /// </summary>
        /// <param name="filePath">The full file path to move from.</param>
        /// <param name="directoryPath">The directory path to move file to.</param>
        /// <param name="fileLogId">The processed file log id.</param>
        public static void MoveFile(string filePath, string directoryPath, long fileLogId)
        {
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            string destinationFilePath = directoryPath + Path.GetFileName(filePath);
            if (fileLogId > 0)
            {
                destinationFilePath = directoryPath + Path.GetFileNameWithoutExtension(filePath) + "_" + fileLogId + Path.GetExtension(filePath);
            }

            File.Move(filePath, destinationFilePath);
        }

        /// <summary>
        /// Gets the javascript time stamp
        /// </summary>
        /// <param name="input">input date.</param>
        /// <returns>the timestamp of date.</returns>
        public static long GetJavascriptTimestamp(this DateTime input)
        {
            TimeSpan span = new TimeSpan(System.DateTime.Parse("1/1/1970").Ticks);
            ////span = span.Add(new TimeSpan(5, 0, 0));
            DateTime time = input.Subtract(span);
            return (long)(time.Ticks / 10000);
        }

        /// <summary>
        /// Checks if file is already opened by another process or user.
        /// </summary>
        /// <param name="fileName">The name of file to check.</param>
        /// <returns>true if it si opened else false.</returns>
        public static bool IsFileinUse(string fileName)
        {
            FileStream stream = null;
            FileInfo file = null;

            try
            {
                file = new FileInfo(fileName);
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                Logger.Write(string.Format("The File {0} is already opened by another User or Process", fileName), LogType.Information);
                return true;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                    stream = null;
                }

                file = null;
            }

            return false;
        }

        /// <summary>
        /// Convert int 0 value to DB null.
        /// </summary>
        /// <param name="value">The int value</param>
        /// <returns>The proper DB value</returns>
        public static string IntZeroToDbNull(this int value)
        {
            if (value == 0)
            {
                return "NULL";
            }
            else
            {
                return value.ObjectToString();
            }
        }

        /// <summary>
        /// Convert long 0 value to DB null.
        /// </summary>
        /// <param name="value">The long value</param>
        /// <returns>The proper DB value</returns>
        public static string LongZeroToDbNull(this long value)
        {
            if (value == 0)
            {
                return "NULL";
            }
            else
            {
                return value.ObjectToString();
            }
        }

        /// <summary>
        /// Convert decimal 0 value to DB null.
        /// </summary>
        /// <param name="value">The decimal value</param>
        /// <returns>The proper DB value</returns>
        public static string DecimalZeroToDbNull(this decimal value)
        {
            if (value == 0)
            {
                return "NULL";
            }
            else
            {
                return value.ObjectToString();
            }
        }

        public static T GetEnum<T>(this string str) where T : struct, IConvertible
        {
            Type enumType = typeof(T);
            if (!enumType.IsEnum)
            {
                throw new Exception("T must be an Enumeration type.");
            }

            T val;
            return Enum.TryParse<T>(str, true, out val) ? val : default(T);
        }

        public static T GetEnum<T>(this int intValue) where T : struct, IConvertible
        {
            Type enumType = typeof(T);
            if (!enumType.IsEnum)
            {
                throw new Exception("T must be an Enumeration type.");
            }

            return (T)Enum.ToObject(enumType, intValue);
        }

        public static string ToMySqlParameter(this string parameter)
        {
            return string.Format("p_{0}", parameter);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Creates a Hashtable from an enumeration
        /// </summary>
        /// <param name="sourceEnum">The source enumeration from which Hashtable is to be created</param>
        /// <returns>A Hashtable with the enumeration values</returns>
        private static Hashtable GetHashTable(this object sourceEnum)
        {
            Hashtable targetHashtable = null;
            string[] names = null;
            Array values = null;
            if (sourceEnum != null)
            {
                targetHashtable = new Hashtable();
                names = Enum.GetNames(sourceEnum.GetType());
                values = Enum.GetValues(sourceEnum.GetType());
                for (int i = 0; i < names.Length; i++)
                {
                    targetHashtable.Add(((int)values.GetValue(i)).ToString(), ResourcesManager.GetResourceString(names[i]));
                }
            }

            return targetHashtable;
        }

        /// <summary>
        /// Creates a Hashtable from an enumeration
        /// </summary>
        /// <param name="sourceEnum">The source enumeration from which Hashtable is to be created</param>
        /// <param name="enumValuesToBeDisplayed">Enumeration values to be populated</param>
        /// <returns>A Hashtable with the enumeration values</returns>
        private static Hashtable GetHashTable(object sourceEnum, int[] enumValuesToBeDisplayed)
        {
            Hashtable targetHashtable = null;
            string[] names = null;
            Array values = null;
            if (sourceEnum != null && enumValuesToBeDisplayed != null)
            {
                targetHashtable = new Hashtable();
                names = Enum.GetNames(sourceEnum.GetType());
                values = Enum.GetValues(sourceEnum.GetType());
                for (int i = 0; i < names.Length; i++)
                {
                    for (int k = 0; k < enumValuesToBeDisplayed.Length; k++)
                    {
                        if (enumValuesToBeDisplayed[k] == (int)values.GetValue(i))
                        {
                            targetHashtable.Add(((int)values.GetValue(i)).ToString(), ResourcesManager.GetResourceString(names[i]));
                        }
                    }
                }
            }

            return targetHashtable;
        }

        #endregion
    }
}

// <copyright file="Result.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class represents Result entity.
// </summary>
namespace HM.Framework
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Result class.
    /// </summary>
    [DataContract]
    [Serializable]
    public class Result
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the Result class.
        /// </summary>
        public Result()
        {
            this.Error = new Error();
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the object of entity.
        /// </summary>
        /// <remarks>
        /// A typical scenario for the use of this property would be while doing a getdeatils() for an entity.
        /// Mostly in case of Add/Edit views.
        /// </remarks>
        [DataMember]
        public IEntityBase Entity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the list of records.
        /// </summary>
        /// <remarks>
        /// A typical scenario for this property would be when list of records to be shown in a view. Mostly Search or list pages.
        /// </remarks>
        [DataMember]
        public IEntityCollectionBase EntityCollection
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Error object.
        /// </summary>
        /// <remarks>
        /// If there are any businesses validation errors occurring at the manager side, then use this property to hold messages.
        /// </remarks>
        [DataMember]
        public Error Error
        {
            get;
            set;
        }
        #endregion
    }
}

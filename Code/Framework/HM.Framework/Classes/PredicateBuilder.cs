﻿// <copyright file="PredicateBuilder.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>24-02-2014</date>
// <summary>This class is used to generate LINQ predicates to filter the collection.
// </summary>

namespace HM.Framework
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// This class is used to generate LINQ predicates to filter the collection.
    /// </summary>
    public static class PredicateBuilder
    {
        /// <summary>
        /// The or condition
        /// </summary>
        /// <typeparam name="T">The type on which the property filter apply to.</typeparam>
        /// <returns>The result of expression[true/false]</returns>
        public static Expression<Func<T, bool>> True<T>()
        {
            return f => true;
        }

        /// <summary>
        /// The And condition
        /// </summary>
        /// <typeparam name="T">The type on which the property filter apply to.</typeparam>
        /// <returns>The result of expression[true/false]</returns>
        public static Expression<Func<T, bool>> False<T>()
        {
            return f => false;
        }

        /// <summary>
        /// The OR condition expression
        /// </summary>
        /// <typeparam name="T">he type on which the property filter apply to.</typeparam>
        /// <param name="expr1">The first expression.</param>
        /// <param name="expr2">The second expression.</param>
        /// <returns>The result of expression[true/false]</returns>
        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expr1, Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>(Expression.OrElse(expr1.Body, invokedExpr), expr1.Parameters);
        }

        /// <summary>
        /// The AND condition expression
        /// </summary>
        /// <typeparam name="T">he type on which the property filter apply to.</typeparam>
        /// <param name="expr1">The first expression.</param>
        /// <param name="expr2">The second expression.</param>
        /// <returns>The result of expression[true/false]</returns>
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expr1, Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>(Expression.AndAlso(expr1.Body, invokedExpr), expr1.Parameters);
        }
    }
}

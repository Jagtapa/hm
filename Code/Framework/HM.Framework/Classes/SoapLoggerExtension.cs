﻿// <copyright file="SoapLoggerExtension.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Sandip Salunkhe</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>27-06-2013</date>
// <summary>This class is used for logging the soap message.
// </summary>

namespace HM.Framework
{
    using System;
    using System.IO;
    using System.Web.Services.Protocols;
    using System.Xml;

    /// <summary>
    /// Extention method to log sopa message
    /// </summary>
    public class SoapLoggerExtension : SoapExtension
    {
        /// <summary>
        /// Old stream variable
        /// </summary>
        private Stream oldStream;

        /// <summary>
        /// New  stream variable
        /// </summary>
        private Stream newStream;

        /// <summary>
        /// GetInitializer Method for the soap log.
        /// </summary>
        /// <param name="methodInfo"> Method info paramater for intialization</param>
        /// <param name="attribute">attrib  paramater for intialization</param>
        /// <returns> returns object after initialization</returns>
        public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute)
        {
            return null;
        }

        /// <summary>
        /// GetInitializer Method for the soap log.
        /// </summary>
        /// <param name="serviceType">service type parameter</param>
        /// <returns>  returns null</returns>
        public override object GetInitializer(Type serviceType)
        {
            return null;
        }

        /// <summary>
        /// Initialize Method for the soap log.
        /// </summary>
        /// <param name="initializer"> Initializer object overrider</param>
        public override void Initialize(object initializer)
        {
        }

        /// <summary>
        /// ChainStream Method for the soap log.
        /// </summary>
        /// <param name="stream">Stram input parameter for further processing</param>
        /// <returns>  returns null</returns>
        public override System.IO.Stream ChainStream(System.IO.Stream stream)
        {
            this.oldStream = stream;
            this.newStream = new MemoryStream();
            return this.newStream;
        }

        /// <summary>
        /// ProcessMessage Method for the soap log.
        /// </summary>
        /// <param name="message">captured soap message</param>
        public override void ProcessMessage(SoapMessage message)
        {
            switch (message.Stage)
            {
                case SoapMessageStage.BeforeSerialize:
                    break;
                case SoapMessageStage.AfterSerialize:
                    this.Log(message, "AfterSerialize");
                    this.CopyStream(this.newStream, this.oldStream);
                    this.newStream.Position = 0;
                    break;
                case SoapMessageStage.BeforeDeserialize:
                    this.CopyStream(this.oldStream, this.newStream);
                    this.Log(message, "BeforeDeserialize");
                    break;
                case SoapMessageStage.AfterDeserialize:
                    break;
            }
        }

        /// <summary>
        /// Log Method to log the entry
        /// </summary>
        /// <param name="message">captured soap message</param>
        /// <param name="stage">String prefix for the log</param>
        public void Log(SoapMessage message, string stage)
        {
            this.newStream.Position = 0;
            string contents = (message is SoapServerMessage) ? "SoapRequest " : "SoapResponse ";
            contents += stage + ";";

            StreamReader reader = new StreamReader(this.newStream);

            contents += reader.ReadToEnd();

            this.newStream.Position = 0;

            Logger.Write(contents, LogType.Information);
        }

        /// <summary>
        /// ReverseIncomingStream Method for the soap log.
        /// </summary>
        public void ReverseIncomingStream()
        {
            this.ReverseStream(this.newStream);
        }

        /// <summary>
        /// ReverseOutgoingStream Method for the soap log.
        /// </summary>
        public void ReverseOutgoingStream()
        {
            this.ReverseStream(this.newStream);
        }

        /// <summary>
        /// ReverseStream Method for the soap log.
        /// </summary>
        /// <param name="stream">Method for reversing the captured message</param>
        public void ReverseStream(Stream stream)
        {
            TextReader tr = new StreamReader(stream);
            string str = tr.ReadToEnd();
            char[] data = str.ToCharArray();
            Array.Reverse(data);
            string strReversed = new string(data);

            TextWriter tw = new StreamWriter(stream);
            stream.Position = 0;
            tw.Write(strReversed);
            tw.Flush();
        }

        /// <summary>
        /// CopyAndReverse Method for the soap log.
        /// </summary>
        /// <param name="from">From location to copy</param>
        /// <param name="to">To location to copy</param>
        private void CopyAndReverse(Stream from, Stream to)
        {
            TextReader tr = new StreamReader(from);
            TextWriter tw = new StreamWriter(to);

            string str = tr.ReadToEnd();
            char[] data = str.ToCharArray();
            Array.Reverse(data);
            string strReversed = new string(data);
            tw.Write(strReversed);
            tw.Flush();
        }

        /// <summary>
        /// CopyStream  Method for the soap log.
        /// </summary>
        /// <param name="fromStream">from stream object parameter</param>
        /// <param name="toStream">To stream object parameter</param>
        private void CopyStream(Stream fromStream, Stream toStream)
        {
            try
            {
                StreamReader sr = new StreamReader(fromStream);
                StreamWriter sw = new StreamWriter(toStream);
                sw.WriteLine(sr.ReadToEnd());
                sw.Flush();
            }
            catch (Exception ex)
            {
                string message = string.Format("CopyStream failed because: {0}", ex.Message);
                ex.HandleException(Global.CONSTSTREXCPOLICYBUSINESS);
            }
        }

        /// <summary>
        /// ReturnStream Method for the soap log.
        /// </summary>
        private void ReturnStream()
        {
            this.CopyAndReverse(this.newStream, this.oldStream);
        }

        /// <summary>
        /// ReceiveStream Method for the soap log.
        /// </summary>
        private void ReceiveStream()
        {
            this.CopyAndReverse(this.newStream, this.oldStream);
        }
    }
}

﻿// <copyright file="PropertyComparer.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>29-10-2013</date>
// <summary>This class used to find distinct property values from collection.
// </summary>

namespace HM.Framework
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    /// <summary>
    /// This is a comparer to find distinct values from collection.
    /// </summary>
    /// <typeparam name="T">The type of object.</typeparam>
    public class PropertyComparer<T> : IEqualityComparer<T>
    {
        /// <summary>
        /// The property for which we need distinct records.
        /// </summary>
        private PropertyInfo propertyInfo;

        /// <summary>
        /// Compare with match case.
        /// </summary>
        private bool matchCase;

        /// <summary>
        /// Initializes a new instance of the PropertyComparer class.
        /// </summary>
        /// <param name="propertyName">The name of the property on type T to perform the comparison on.</param>
        public PropertyComparer(string propertyName)
        {
            // store a reference to the property info object for use during the comparison
            this.propertyInfo = typeof(T).GetProperty(propertyName, BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.Public);
            if (this.propertyInfo == null)
            {
                throw new ArgumentException(string.Format("{0} is not a property of type {1}.", propertyName, typeof(T)));
            }

            this.matchCase = true;
        }

        /// <summary>
        /// Initializes a new instance of the PropertyComparer class.
        /// </summary>
        /// <param name="propertyName">The name of the property on type T to perform the comparison on.</param>
        /// <param name="matchCase">the flag to indicate match case.</param>
        public PropertyComparer(string propertyName, bool matchCase)
        {
            // store a reference to the property info object for use during the comparison
            this.propertyInfo = typeof(T).GetProperty(propertyName, BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.Public);
            if (this.propertyInfo == null)
            {
                throw new ArgumentException(string.Format("{0} is not a property of type {1}.", propertyName, typeof(T)));
            }

            this.matchCase = matchCase;
        }

        #region IEqualityComparer<T> Members
        /// <summary>
        /// The comparrer to compare values of specified property.
        /// </summary>
        /// <param name="x">The x value of property to comapre.</param>
        /// <param name="y">The y value of property to comapre.</param>
        /// <returns>The result of comparison.</returns>
        public bool Equals(T x, T y)
        {
            // get the current value of the comparison property of x and of y
            object xvalue = this.propertyInfo.GetValue(x, null);
            object yvalue = this.propertyInfo.GetValue(y, null);

            // if the xValue is null then we consider them equal if and only if yValue is null
            if (xvalue == null)
            {
                return yvalue == null;
            }

            // use the default comparer for whatever type the comparison property is.
            if (this.matchCase)
            {
                return xvalue.Equals(yvalue);
            }

            return xvalue.ObjectToString().Equals(yvalue.ObjectToString(), StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// The hash code for an object.
        /// </summary>
        /// <param name="obj">The object to get hash code.</param>
        /// <returns>The hash code of object.</returns>
        public int GetHashCode(T obj)
        {
            // get the value of the comparison property out of obj
            object propertyValue = this.propertyInfo.GetValue(obj, null);
            if (propertyValue == null)
            {
                return 0;
            }
            else
            {
                return propertyValue.GetHashCode();
            }
        }

        #endregion
    }
}

﻿// <copyright file="ErroIdValue.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class represents ErroIdValue entity.
// </summary>
namespace HM.Framework
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// ErroIdValue class.
    /// </summary>
    [DataContract]
    [Serializable]
    public class ErroIdValue : EntityBase
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ErroIdValue class.
        /// </summary>
        public ErroIdValue()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ErroIdValue class.
        /// </summary>
        /// <param name="errorId">string errorId</param>
        /// <param name="errorValue">string errorValue</param>
        public ErroIdValue(string errorId, string errorValue)
        {
            this.ErrorId = errorId;
            this.ErrorValue = errorValue;
        } 
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets or sets the id of error.
        /// </summary>
        [DataMember]
        public string ErrorId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the error string.
        /// </summary>
        [DataMember]
        public string ErrorValue
        {
            get;
            set;
        } 
        #endregion
    }
}

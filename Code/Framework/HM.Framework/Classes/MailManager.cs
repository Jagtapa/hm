// <copyright file="MailManager.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class is used to send the mails.
// </summary>
namespace HM.Framework
{
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Mail;
    using System.Net.Mime;

    /// <summary>
    /// MailManager class.
    /// </summary>
    public class MailManager
    {
        #region Public Methods
        /// <summary>
        /// Send mails.
        /// </summary>
        /// <param name="mailEntity">MailEntity details of mail attributes.</param>
        public static void SendMail(MailEntity mailEntity)
        {
            MailMessage mailMessage = new MailMessage();
            Attachment data = null;
            SmtpClient smtpClient = null;
            try
            {
                if (string.IsNullOrEmpty(mailEntity.From) || mailEntity.ToAddressList.Count <= 0)
                {
                    return;
                }

                mailMessage.From = new MailAddress(mailEntity.From);
                foreach (string toAddress in mailEntity.ToAddressList)
                {
                    if (!string.IsNullOrEmpty(toAddress.Trim()))
                    {
                        mailMessage.To.Add(toAddress);
                    }
                }

                foreach (string carbonCopyAddress in mailEntity.CcAddressList)
                {
                    if (!string.IsNullOrEmpty(carbonCopyAddress.Trim()))
                    {
                        mailMessage.CC.Add(carbonCopyAddress);
                    }
                }

                foreach (string bccAddress in mailEntity.BccAddressList)
                {
                    if (!string.IsNullOrEmpty(bccAddress.Trim()))
                    {
                        mailMessage.Bcc.Add(bccAddress);
                    }
                }

                mailMessage.Subject = mailEntity.Subject;
                mailMessage.Body = mailEntity.Body;
                mailMessage.IsBodyHtml = mailEntity.IsBodyHtml;

                foreach (string attachment in mailEntity.Attachments)
                {
                    if (!string.IsNullOrEmpty(attachment))
                    {
                        data = new Attachment(attachment, mailEntity.MediaType);

                        // Add time stamp information for the file.
                        ContentDisposition disposition = data.ContentDisposition;
                        disposition.CreationDate = File.GetCreationTime(attachment);
                        disposition.ModificationDate = File.GetLastWriteTime(attachment);
                        disposition.ReadDate = File.GetLastAccessTime(attachment);

                        // Add the file attachment to this e-mail message.
                        mailMessage.Attachments.Add(data);
                    }
                }

                smtpClient = new SmtpClient(mailEntity.Host);
                smtpClient.Credentials = CredentialCache.DefaultNetworkCredentials;
                smtpClient.Send(mailMessage);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (data != null)
                {
                    data.Dispose();
                }

                data = null;
                smtpClient = null;
            }
        } 
        #endregion
    }
}

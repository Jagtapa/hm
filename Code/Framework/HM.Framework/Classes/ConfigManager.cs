// <copyright file="ConfigManager.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class used to manage config file.
// </summary>

namespace HM.Framework
{
    using System.Configuration;
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// This class manages the configuration of the application.
    /// Setting and retrieving of configuration values are to be done using
    /// this class.
    /// </summary>
    public class ConfigManager
    {
        #region Public Methods
        /// <summary>
        /// Read appSettings values from the config file.
        /// </summary>
        /// <param name="key">The key for which the value is required</param>
        /// <returns>Value of the key</returns>
        public static string ReadAppSettings(string key)
        {
            if (ConfigurationManager.AppSettings[key] != null)
            {
                return ConfigurationManager.AppSettings[key];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Read Database Setting values from the config file.
        /// </summary>
        /// <param name="key">The key for which the value is required</param>
        /// <returns>Value of the key</returns>
        public static string ReadDatabaseSettings(string key)
        {
            Database database = EnterpriseLibraryContainer.Current.GetInstance<Database>(key);
            return database.ConnectionString;
        } 
        #endregion
    }
}

// <copyright file="CacheConstants.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This file contains cache constants.
// </summary>
namespace HM.Framework
{
    /// <summary>
    /// This static class has all the constants which are used for the cache purpose.
    /// Any new cache is required in application use this class for key.
    /// </summary>
    public class CacheConstants
    {
        /// <summary>
        /// SQLDependancy DB Entry Constants
        /// </summary>
        public const string CACHESQLDEPDBENTRYNAME = "CACHE_MATS2_PLANNING";

        /// <summary>
        /// The COMBINEDMASTER ids with msc cache Constant
        /// </summary>
        public const string COMBINEDMASTERIDWITHMSCLIST = "COMBINEDMASTERIDWITHMSCLIST";

        /// <summary>
        /// The COMBINEDMASTER ids with cell and circle cache Constant
        /// </summary>
        public const string COMBINEDMASTERWITHCELLANDCIRCLELIST = "COMBINEDMASTERWITHCELLANDCIRCLELIST";

        /// <summary>
        /// Get the distinct region, circle, districts , town, bsc and sam ids cache Constant
        /// </summary>
        public const string DISTINCTCOMBINEDMASTERIDS = "DISTINCTCOMBINEDMASTERIDS";

        /// <summary>
        /// Get the distinct COMBINEDMASTERIDWITHMSCRAINBOWCITY cache Constant
        /// </summary>
        public const string COMBINEDMASTERIDWITHMSCRAINBOWCITY = "COMBINEDMASTERIDWITHMSCRAINBOWCITY";

        /// <summary>
        /// Get the REGIONCIRCLELIST cache Constant
        /// </summary>
        public const string REGIONCIRCLELIST = "REGIONCIRCLELIST";

        /// <summary>
        /// Get the REGIONLIST cache Constant
        /// </summary>
        public const string REGIONLIST = "REGIONLIST";

        /// <summary>
        /// The CIRCLE LOOKUP cache Constant
        /// </summary>
        public const string CIRCLELOOKUPLIST = "CIRCLELOOKUPLIST";

        /// <summary>
        /// The GSM DSR MASTERS cache Constant
        /// </summary>
        public const string DSRSAMMASTERLIST = "DSRSAMMASTERLIST";

        /// <summary>
        /// The CFR MASTERS cache Constant
        /// </summary>
        public const string CFRMASTERLIST = "CFRMASTERLIST";

        /// <summary>
        /// Get the FILETYPELIST cache Constant
        /// </summary>
        public const string FILETYPELIST = "FILETYPELIST";

        /// <summary>
        /// Get the CIRCLELIST3G cache Constant
        /// </summary>
        public const string CIRCLELIST3G = "CIRCLELIST3G";

        /// <summary>
        /// The COMBINEDMASTER ids with msc cache Constant
        /// </summary>
        public const string COMBINEDMASTERIDWITHMSCLIST3G = "COMBINEDMASTERIDWITHMSCLIST3G";

        /// <summary>
        /// The COMBINEDMASTER ids with cell and circle cache Constant
        /// </summary>
        public const string COMBINEDMASTERWITHCELLANDCIRCLELIST3G = "COMBINEDMASTERWITHCELLANDCIRCLELIST3G";

        /// <summary>
        /// Get the distinct region, circle, districts , town, bsc and sam ids cache Constant
        /// </summary>
        public const string DISTINCTCOMBINEDMASTERIDS3G = "DISTINCTCOMBINEDMASTERIDS3G";

        /// <summary>
        /// Get the distinct COMBINEDMASTERIDWITHMSCRAINBOWCITY3G cache Constant
        /// </summary>
        public const string COMBINEDMASTERIDWITHMSCRAINBOWCITY3G = "COMBINEDMASTERIDWITHMSCRAINBOWCITY3G";

        /// <summary>
        /// Get the REGIONCIRCLELIST3G cache Constant
        /// </summary>
        public const string REGIONCIRCLELIST3G = "REGIONCIRCLELIST3G";

        /// <summary>
        /// Get the REGIONLIST3G cache Constant
        /// </summary>
        public const string REGIONLIST3G = "REGIONLIST3G";

        /// <summary>
        /// The CIRCLE LOOKUP cache Constant
        /// </summary>
        public const string CIRCLELOOKUPLIST3G = "CIRCLELOOKUPLIST3G";

        /// <summary>
        /// The GSM DSR MASTERS cache Constant
        /// </summary>
        public const string DSRSAMMASTERLIST3G = "DSRSAMMASTERLIST3G";

        /// <summary>
        /// The CFR MASTERS cache Constant
        /// </summary>
        public const string CFRMASTERLIST3G = "CFRMASTERLIST3G";

        /// <summary>
        /// Get the FILETYPELIST3G cache Constant
        /// </summary>
        public const string FILETYPELIST3G = "FILETYPELIST3G";
    }
}

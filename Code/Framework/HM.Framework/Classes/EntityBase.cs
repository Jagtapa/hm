// <copyright file="EntityBase.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class represents (EntityBase) the base entity for all entities.
// </summary>
namespace HM.Framework
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// EntityBase class.
    /// </summary>
    [DataContract]
    [Serializable]
    public class EntityBase : IEntityBase, ICloneable
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the EntityBase class.
        /// </summary>
        public EntityBase()
        {
            this.IsValid = true;
            this.Error = new Error();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Error object.
        /// </summary>
        [DataMember]
        public virtual Error Error
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether Is valid.
        /// </summary>
        [DataMember]
        public virtual bool IsValid
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether key for entity.
        /// </summary>
        [DataMember]
        public virtual int Key
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the created by user id.
        /// </summary>
        [DataMember]
        public int CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the created on date and time.
        /// </summary>
        [DataMember]
        public DateTime? CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the updated by user id.
        /// </summary>
        [DataMember]
        public int UpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets the updated on date and time.
        /// </summary>
        [DataMember]
        public DateTime? UpdatedOn { get; set; }

        /// <summary>
        /// Gets or sets the deleted status. 0-Not deleted 1-deleted
        /// </summary>
        [DataMember]
        public int Deleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Converts the current entity to xml version.
        /// </summary>
        /// <returns>The XML string.</returns>
        public virtual string ToXML()
        {
            return CommonUtility.ConvertToXML(this);
        }

        /// <summary>
        /// Creates the dummy entity.
        /// </summary>
        /// <returns>The cloned entity.</returns>
        public virtual object Clone()
        {
            try
            {
                return this.MemberwiseClone() as EntityBase;
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}

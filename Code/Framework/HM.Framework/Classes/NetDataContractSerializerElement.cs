﻿// <copyright file="NetDataContractSerializerElement.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class is used to create the behavior for interfaces.
// </summary>
namespace HM.Framework
{
    using System;
    using System.ServiceModel.Configuration;

    /// <summary>
    /// NetDataContractSerializerElement class.
    /// </summary>
    public class NetDataContractSerializerElement : BehaviorExtensionElement
    {
        #region Public Properties
        /// <summary>
        /// Gets the type of behavior.
        /// </summary>
        public override Type BehaviorType
        {
            get { return typeof(NetDataContractSerializerBehavior); }
        } 
        #endregion

        #region Protected Methods
        /// <summary>
        /// Overrides the behavior with cutom behavior.
        /// </summary>
        /// <returns>The net data contract serializer behavior.</returns>
        protected override object CreateBehavior()
        {
            return new NetDataContractSerializerBehavior();
        } 
        #endregion
    }
}

// <copyright file="Logger.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class used to log the erros, information and warnings.
// </summary>
namespace HM.Framework
{
    using System;
    using Microsoft.Practices.EnterpriseLibrary.Logging;

    /// <summary>
    /// LogHelper to logg all the error, warnings and informations.
    /// </summary>
    public class Logger
    {
        #region Public Methods
        /// <summary>
        /// Write a new log entry to the default category. 
        /// </summary>
        /// <param name="message">The message to logg.</param>
        /// <param name="logType">The log type.</param>
        public static void Write(string message, LogType logType)
        {
            if (LoggingEnabled())
            {
                LogEntry logEntry = new LogEntry();
                logEntry.Categories.Add(ReadCategory(logType));
                logEntry.Message = message;
                Microsoft.Practices.EnterpriseLibrary.Logging.Logger.Write(logEntry);
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Reads the config entry for the LoggingEnabledtrue or false.
        /// </summary>
        /// <returns>bool value indication whether or not to write to the log file.</returns>
        private static bool LoggingEnabled()
        {
            return (ConfigManager.ReadAppSettings(Global.LOGENABLED) != null
                && Convert.ToBoolean(ConfigManager.ReadAppSettings(Global.LOGENABLED))) ? true : false;
        }

        /// <summary>
        /// Reads the category appropriately.
        /// </summary>
        /// <param name="logType">LogType logType</param>
        /// <returns>String value that specifies the category the log should be writtn into.</returns>
        private static string ReadCategory(LogType logType)
        {
            string category = Global.INFORMATIONCATEGORY;
            switch (logType)
            {
                case LogType.Information:
                    category = Global.INFORMATIONCATEGORY;
                    break;
                case LogType.Warning:
                    category = Global.WARNINGSCATEGORY;
                    break;
                case LogType.Error:
                    category = Global.ERRORCATEGORY;
                    break;
            }

            return category;
        }
        #endregion
    }
}

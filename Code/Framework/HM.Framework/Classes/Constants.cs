﻿// <copyright file="Constants.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>05-09-2014</date>
// <summary> This class defines all the constants in REQUIRED FOR APPLICATION.
// </summary>

namespace HM.Framework
{
    /// <summary>
    /// This class defines all the constants in KPI system.
    /// </summary>
    public class Constants
    {
        #region DB CONSTANTS

        #region QUERY CONSTANTS
        /// <summary>
        /// The pUSERID PARAMETER name constant.
        /// </summary>
        public const string PARAMETERUSERID = "pUSERID";

        /// <summary>
        /// The  PARAMETER PREFIX name constant.
        /// </summary>
        public const string PARAMPREFIX = "P{0}";

        /// <summary>
        /// The BUCKETDISTANCE PARAMETER name constant.
        /// </summary>
        public const string PARAMBUCKETDISTANCE = "BUCKETDISTANCE";

        #endregion

        #region SP CONSTANTS

        /// <summary>
        /// The column name KPILEVEL
        /// </summary>
        public const string KPILEVEL = "KPILEVEL";

        /// <summary>
        /// The REGISTERUSER SP name constant.
        /// </summary>
        public const string SPREGISTERUSER = "REGISTERUSER";

        /// <summary>
        /// The GETUSERDETAILS SP name constant.
        /// </summary>
        public const string SPAUTHENTICATE = "AUTHENTICATE";

        /// <summary>
        /// The GETUSERDETAILS SP name constant.
        /// </summary>
        public const string SPGETUSERDETAILS = "GETUSERDETAILS";

        /// <summary>
        /// The UPDATEUSER SP name constant.
        /// </summary>
        public const string SPUPDATEUSER = "UPDATEUSER";

        /// <summary>
        /// The DELETEUSER SP name constant.
        /// </summary>
        public const string SPDELETEUSER = "DELETEUSER";

        /// <summary>
        /// The ADDHOME SP name constant.
        /// </summary>
        public const string SPADDHOME = "ADDHOME";

        /// <summary>
        /// The UPDATEHOME SP name constant.
        /// </summary>
        public const string SPUPDATEHOME = "UPDATEHOME";

        /// <summary>
        /// The DELETEHOME SP name constant.
        /// </summary>
        public const string SPDELETEHOME = "DELETEHOME";

        /// <summary>
        /// The GETHOMES SP name constant.
        /// </summary>
        public const string SPGETHOMES = "GETHOMES";

        /// <summary>
        /// The ADDHOMEUSER SP name constant.
        /// </summary>
        public const string SPADDHOMEUSER = "ADDHOMEUSER";

        /// <summary>
        /// The DELETEHOMEUSER SP name constant.
        /// </summary>
        public const string SPDELETEHOMEUSER = "DELETEHOMEUSER";

        /// <summary>
        /// The ADDROOMITEMTYPE SP name constant.
        /// </summary>
        public const string SPADDROOMITEMTYPE = "ADDROOMITEMTYPE";

        /// <summary>
        /// The UPDATEROOMITEMTYPE SP name constant.
        /// </summary>
        public const string SPUPDATEROOMITEMTYPE = "UPDATEROOMITEMTYPE";

        /// <summary>
        /// The DELETEROOMITEMTYPE SP name constant.
        /// </summary>
        public const string SPDELETEROOMITEMTYPE = "DELETEROOMITEMTYPE";

        /// <summary>
        /// The GETROOMITEMTYPES SP name constant.
        /// </summary>
        public const string SPGETROOMITEMTYPES = "GETROOMITEMTYPES";

        /// <summary>
        /// The ADDROOMTYPE SP name constant.
        /// </summary>
        public const string SPADDROOMTYPE = "ADDROOMTYPE";

        /// <summary>
        /// The UPDATEROOMTYPE SP name constant.
        /// </summary>
        public const string SPUPDATEROOMTYPE = "UPDATEROOMTYPE";

        /// <summary>
        /// The DELETEROOMTYPE SP name constant.
        /// </summary>
        public const string SPDELETEROOMTYPE = "DELETEROOMTYPE";

        /// <summary>
        /// The GETROOMTYPES SP name constant.
        /// </summary>
        public const string SPGETROOMTYPES = "GETROOMTYPES";

        /// <summary>
        /// The ADDROOMDETAILS SP name constant.
        /// </summary>
        public const string SPADDROOMDETAILS = "ADDROOMDETAILS";

        /// <summary>
        /// The UPDATEROOMDETAILS SP name constant.
        /// </summary>
        public const string SPUPDATEROOMDETAILS = "UPDATEROOMDETAILS";

        /// <summary>
        /// The DELETEROOMDETAILS SP name constant.
        /// </summary>
        public const string SPDELETEROOMDETAILS = "DELETEROOMDETAILS";

        /// <summary>
        /// The GETROOMDETAILS SP name constant.
        /// </summary>
        public const string SPGETROOMDETAILS = "GETROOMDETAILS";

        /// <summary>
        /// The ADDROOM SP name constant.
        /// </summary>
        public const string SPADDROOM = "ADDROOM";

        /// <summary>
        /// The UPDATEROOM SP name constant.
        /// </summary>
        public const string SPUPDATEROOM = "UPDATEROOM";

        /// <summary>
        /// The DELETEROOM SP name constant.
        /// </summary>
        public const string SPDELETEROOM = "DELETEROOM";

        /// <summary>
        /// The GETROOMS SP name constant.
        /// </summary>
        public const string SPGETROOMS = "GETROOMS";

        #endregion

        #region COLUMN CONSTANTS

        #region Common Column Names
        /// <summary>
        /// The column name ID
        /// </summary>
        public const string ID = "ID";

        /// <summary>
        /// The column name CREATEDBY
        /// </summary>
        public const string CREATEDBY = "CREATEDBY";

        /// <summary>
        /// The column name CREATEDON
        /// </summary>
        public const string CREATEDON = "CREATEDON";

        /// <summary>
        /// The column name UPDATEDBY
        /// </summary>
        public const string UPDATEDBY = "UPDATEDBY";

        /// <summary>
        /// The column name UPDATEDON
        /// </summary>
        public const string UPDATEDON = "UPDATEDON";

        /// <summary>
        /// The column name DELETED
        /// </summary>
        public const string DELETED = "DELETED";

        /// <summary>
        /// The column name COUNT
        /// </summary>
        public const string COUNT = "COUNT";

        /// <summary>
        /// The column name STARTDATE
        /// </summary>
        public const string STARTDATE = "STARTDATE";

        /// <summary>
        /// The column name ENDDATE
        /// </summary>
        public const string ENDDATE = "ENDDATE";

        #endregion

        #region USER COLUMNS

        /// <summary>
        /// The column name FIRSTNAME
        /// </summary>
        public const string FIRSTNAME = "FIRSTNAME";

        /// <summary>
        /// The column name LASTNAME
        /// </summary>
        public const string LASTNAME = "LASTNAME";

        /// <summary>
        /// The column name NETWORKID
        /// </summary>
        public const string NETWORKID = "NETWORKID";

        /// <summary>
        /// The column name EMAILID
        /// </summary>
        public const string EMAILID = "EMAILID";

        /// <summary>
        /// The column name ROLE
        /// </summary>
        public const string ROLE = "ROLE";

        /// <summary>
        /// The column name ISADMIN
        /// </summary>
        public const string ISADMIN = "ISADMIN";

        /// <summary>
        /// The column name USERPASSWORD
        /// </summary>
        public const string USERPASSWORD = "PASSWORD";

        /// <summary>
        /// The column name USERID
        /// </summary>
        public const string USERID = "USERID";

        /// <summary>
        /// The column name LOGINTIME
        /// </summary>
        public const string LOGINTIME = "LOGINTIME";

        /// <summary>
        /// The column name LOGOUTTIME
        /// </summary>
        public const string LOGOUTTIME = "LOGOUTTIME";
        #endregion

        #region HOME

        /// <summary>
        /// The column name HOUSENAME
        /// </summary>
        public const string HOUSENAME = "HOUSENAME";

        /// <summary>
        /// The column name HOUSENO
        /// </summary>
        public const string HOUSENO = "HOUSENO";

        /// <summary>
        /// The column name STREET
        /// </summary>
        public const string STREET = "STREET";

        /// <summary>
        /// The column name COUNTRY
        /// </summary>
        public const string COUNTRY = "COUNTRY";

        /// <summary>
        /// The column name CITY
        /// </summary>
        public const string CITY = "CITY";

        /// <summary>
        /// The column name TOWN
        /// </summary>
        public const string TOWN = "TOWN";

        /// <summary>
        /// The column name LOCALITY
        /// </summary>
        public const string LOCALITY = "LOCALITY";

        /// <summary>
        /// The column name POSTCODE
        /// </summary>
        public const string POSTCODE = "POSTCODE";
        #endregion

        #region ROOM COLUMNS

        /// <summary>
        /// The column name HOMEID
        /// </summary>
        public const string HOMEID = "HOMEID";

        /// <summary>
        /// The column name TYPEID
        /// </summary>
        public const string TYPEID = "TYPEID";

        /// <summary>
        /// The column name DESCRIPTION
        /// </summary>
        public const string DESCRIPTION = "DESCRIPTION";

        /// <summary>
        /// The column name ROOMID
        /// </summary>
        public const string ROOMID = "ROOMID";

        /// <summary>
        /// The column name ROOMITEMTYPEID
        /// </summary>
        public const string ROOMITEMTYPEID = "ROOMITEMTYPEID";

        /// <summary>
        /// The column name BRAND
        /// </summary>
        public const string BRAND = "BRAND";

        /// <summary>
        /// The column name YEAROFINSTALLED
        /// </summary>
        public const string YEAROFINSTALLED = "YEAROFINSTALLED";

        /// <summary>
        /// The column name QUANTITY
        /// </summary>
        public const string QUANTITY = "QUANTITY";

        /// <summary>
        /// The column name ITEM
        /// </summary>
        public const string ITEM = "ITEM";

        /// <summary>
        /// The column name TYPE
        /// </summary>
        public const string TYPE = "TYPE";

        #endregion
        #endregion

        #endregion

        #region Error constants
        /// <summary>
        /// Failed to read SAM report file.
        /// </summary>
        public const string READINDGSMDSREXCELFAILED = "Failed to read SAM report file.";
        #endregion

        #region CacheConstatnts
        /// <summary>
        /// The application entity cache constant.
        /// </summary>
        public const string CACHEAPPLICATIONENTITY = "APPLICATIONENTITY";
        #endregion

        #region Config constants
        /// <summary>
        /// The mail host server key constant.
        /// </summary>
        public const string SMTPSERVER = "SmtpServer";

        /// <summary>
        /// The FROMEMAIL key constant.
        /// </summary>
        public const string FROMEMAIL = "FROMEMAIL";

        /// <summary>
        /// The ADMINISTRATOREMAIL key constant.
        /// </summary>
        public const string ADMINISTRATOREMAIL = "ADMINISTRATOREMAIL";

        /// <summary>
        /// Gets the FILEDIRECTORYPATH key constant.
        /// </summary>
        public const string FILEDIRECTORYPATH = "FILEDIRECTORYPATH";

        /// <summary>
        /// Gets the FILEPROCESSEDDIRECTORYPATH key constant.
        /// </summary>
        public const string FILEPROCESSEDDIRECTORYPATH = "FILEPROCESSEDDIRECTORYPATH";

        /// <summary>
        /// Gets the FILEUNPROCESSEDDIRECTORYPATH key constant.
        /// </summary>
        public const string FILEUNPROCESSEDDIRECTORYPATH = "FILEUNPROCESSEDDIRECTORYPATH";

        /// <summary>
        /// Gets the BUCKETDISTANCE key constant.
        /// </summary>
        public const string BUCKETDISTANCE = "BUCKETDISTANCE";
        
        /// <summary>
        /// Gets the PROCESSFILESANDDATAFREQUENCY key constant.
        /// </summary>
        public const string PROCESSFILESANDDATAFREQUENCY = "PROCESSFILESANDDATAFREQUENCY";
        
        /// <summary>
        /// Gets the SQLINSERTBATCHSIZE key constant.
        /// </summary>
        public const string SQLINSERTBATCHSIZE = "SQLINSERTBATCHSIZE";

        /// <summary>
        /// Gets the DATEFORMATINFILENAME key constant.
        /// </summary>
        public const string DATEFORMATINFILENAME = "DATEFORMATINFILENAME";

        /// <summary>
        /// Gets the DATETIMEFORMATINFILE key constant.
        /// </summary>
        public const string DATETIMEFORMATINFILE = "DATETIMEFORMATINFILE";        
        
        /// <summary>
        /// Gets the FILEDATACOLUMNS key constant.
        /// </summary>
        public const string FILEDATACOLUMNS = "FILEDATACOLUMNS";        
        #endregion

        #region Session Constants
        /// <summary>
        /// The current logged in user session constant.
        /// </summary>
        public const string CURRENTLOGGEDINUSER = "CurrentLoggedInUser";
        #endregion
    }
}

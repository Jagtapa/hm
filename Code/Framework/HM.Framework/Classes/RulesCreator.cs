// <copyright file="RulesCreator.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class is used to create the rules.
// </summary>
namespace HM.Framework
{
    using System;
    using System.Collections;
    using System.IO;
    using System.Text;
    using System.Xml;

    /// <summary>
    /// RulesCreator class.
    /// </summary>
    public abstract class RulesCreator
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the RulesCreator class.
        /// </summary>
        public RulesCreator()
        {
        } 
        #endregion

        #region Public Methods
        /// <summary>
        /// Loads validations from a file.
        /// </summary>
        /// <param name="level">level of the xml validation.</param>
        /// <param name="useCase">string use Case .</param>
        /// <param name="stream">Stream stream .</param>
        /// <param name="configStream">Stream configStream</param>
        /// <returns>The validations as XML string</returns>
        public static string LoadXMLValidations(int level, string useCase, Stream stream, Stream configStream)
        {
            string matchString = null;
            try
            {
                Hashtable configDataHashtable = new Hashtable();
                XmlDocument dataXmlDocument = new XmlDocument();
                XmlDocument configXmlDocument = new XmlDocument();
                dataXmlDocument.Load(stream);
                configXmlDocument.Load(configStream);

                if (configXmlDocument.ChildNodes.Item(1).ChildNodes.Item(0).ChildNodes.Count > 0)
                {
                    for (int nodeCount = 0; nodeCount <= configXmlDocument.ChildNodes.Item(1).ChildNodes.Item(0).ChildNodes.Count - 1; nodeCount++)
                    {
                        configDataHashtable.Add(configXmlDocument.ChildNodes.Item(1).ChildNodes.Item(0).ChildNodes.Item(nodeCount).Name.ToString(), configXmlDocument.ChildNodes.Item(1).ChildNodes.Item(0).ChildNodes.Item(nodeCount).InnerXml.ToString());
                    }
                }

                StringBuilder xsltText = new StringBuilder();
                XmlNodeList rootNode = dataXmlDocument.SelectNodes("/" + dataXmlDocument.ChildNodes.Item(0).Name.ToString() + "/" + dataXmlDocument.ChildNodes.Item(0).ChildNodes.Item(0).Name.ToString());
                matchString = "/" + dataXmlDocument.ChildNodes.Item(0).Name.ToString() + "/" + dataXmlDocument.ChildNodes.Item(0).ChildNodes.Item(0).Name.ToString();

                if (rootNode.Count <= 0)
                {
                    return null;
                }

                XmlNodeList nodeParentList = rootNode.Item(0).SelectNodes(matchString + "[@UseCase='" + useCase + "'][@Level='" + level + "']");
                if (nodeParentList.Count <= 0)
                {
                    return null;
                }

                xsltText.Append("<?xml version='1.0' encoding='UTF-8' ?><xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>");
                xsltText.Append("<xsl:template match=");
                string matchStringDoc = matchString;
                xsltText.Append("'" + matchString + "'>");
                xsltText.Append("<Errors>");
                xsltText.Append("<xsl:for-each select='*'>");
                foreach (XmlNode dataNode in nodeParentList.Item(0).ChildNodes)
                {
                    xsltText.Append("<xsl:element name='Detail' >");
                    foreach (XmlNode node in nodeParentList.Item(0).ChildNodes)
                    {
                        foreach (XmlNode validationNode in node)
                        {
                            if (validationNode.ChildNodes.Count > 0)
                            {
                                foreach (XmlNode childValidationNode in validationNode.ChildNodes)
                                {
                                    if (childValidationNode.Name.ToString() == "Validation")
                                    {
                                        if (childValidationNode.Attributes.Count > 0)
                                        {
                                            foreach (XmlAttribute childAttribute in childValidationNode.Attributes)
                                            {
                                                if ((childAttribute.Name.ToString() != "ERROR") && (childAttribute.Name.ToString() != "NestedFormula"))
                                                {
                                                    string templateString = childAttribute.Name.ToString();
                                                    templateString = configDataHashtable[templateString].ToString();
                                                    templateString = templateString.Replace("$FieldName$", childAttribute.Value.ToString());

                                                    templateString = templateString.Replace("$xPath$", "../" + childValidationNode.ParentNode.Name.ToString() + "/*");

                                                    templateString = templateString.Replace("[LT]", "&lt;");
                                                    templateString = templateString.Replace("[GT]", "&gt;");
                                                    templateString = templateString.Replace("[EQ]", "=");
                                                    templateString = templateString.Replace("[LE]", "&lt;=");
                                                    templateString = templateString.Replace("[GE]", "&gt;=");
                                                    templateString = templateString.Replace("[NE]", "!=");
                                                    templateString = templateString.Replace("$ErrorMessage$", childValidationNode.Attributes.GetNamedItem("ERROR").Value.ToString());
                                                    xsltText.Append(templateString);
                                                }
                                                else if ("NestedFormula" == childAttribute.Name.ToString())
                                                {
                                                    string templateString = GetNestedFormulaXSL(configDataHashtable, childValidationNode.ParentNode);
                                                    templateString = templateString.Replace("[LT]", "&lt;");
                                                    templateString = templateString.Replace("[GT]", "&gt;");
                                                    templateString = templateString.Replace("[EQ]", "=");
                                                    templateString = templateString.Replace("[LE]", "&lt;=");
                                                    templateString = templateString.Replace("[GE]", "&gt;=");
                                                    templateString = templateString.Replace("[NE]", "!=");
                                                    templateString = templateString.Replace("$ErrorMessage$", childValidationNode.Attributes.GetNamedItem("ERROR").Value.ToString());
                                                    xsltText.Append(templateString);
                                                }
                                            }
                                        }
                                    }
                                    else if (childValidationNode.Name.ToString() != "Validation")
                                    {
                                        xsltText.Append(LoadNestedValidations(configDataHashtable, childValidationNode, matchString + "/" + childValidationNode.ParentNode.Name.ToString() + "/" + childValidationNode.Name.ToString() + "/" + childValidationNode.Name.ToString()));
                                    }
                                }
                            }
                        }
                    }

                    xsltText.Append("</xsl:element>");
                    xsltText.Append("</xsl:for-each>");
                    xsltText.Append("</Errors>");
                    xsltText.Append("</xsl:template>");
                }

                xsltText.Append("</xsl:stylesheet>");
                return xsltText.ToString();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Loads nested validations
        /// </summary>
        /// <param name="configDataHashTable">Hashtable configDataHashTable</param>
        /// <param name="parentNode">XmlNode parentNode</param>
        /// <param name="path">string path</param>
        /// <returns>The validations as XML string</returns>
        private static string LoadNestedValidations(Hashtable configDataHashTable, XmlNode parentNode, string path)
        {
            StringBuilder xsltText = new StringBuilder();
            xsltText.Append(path == null ? string.Empty : "<xsl:for-each select='" + path + "'>");
            foreach (XmlNode node in parentNode)
            {
                if (node.ChildNodes.Count > 0)
                {
                    foreach (XmlNode childValidationNode in node.ChildNodes)
                    {
                        if (childValidationNode.Name.ToString() == "Validation")
                        {
                            if (childValidationNode.Attributes.Count > 0)
                            {
                                foreach (XmlAttribute childAttribute in childValidationNode.Attributes)
                                {
                                    if (childAttribute.Name.ToString() != "ERROR")
                                    {
                                        string templateString = childAttribute.Name.ToString();
                                        templateString = configDataHashTable[templateString].ToString();
                                        templateString = templateString.Replace("$FieldName$", childAttribute.Value.ToString());
                                        templateString = templateString.Replace("$xPath$", "../" + childValidationNode.ParentNode.Name.ToString() + "/*");

                                        templateString = templateString.Replace("[LT]", "&lt;");
                                        templateString = templateString.Replace("[GT]", "&gt;");
                                        templateString = templateString.Replace("[EQ]", "=");
                                        templateString = templateString.Replace("[LE]", "&lt;=");
                                        templateString = templateString.Replace("[GE]", "&gt;=");
                                        templateString = templateString.Replace("[NE]", "!=");
                                        templateString = templateString.Replace("$ErrorMessage$", childValidationNode.Attributes.GetNamedItem("ERROR").Value.ToString());
                                        xsltText.Append(templateString);
                                    }
                                }
                            }
                            else
                            {
                                xsltText.Append(LoadNestedValidations(configDataHashTable, childValidationNode, path == null ? null : path + "/" + childValidationNode.Name.ToString()));
                            }
                        }
                    }
                }
            }

            xsltText.Append(path == null ? string.Empty : "</xsl:for-each>");
            return xsltText.ToString();
        }

        /// <summary>
        /// Gets nested formulas for an XSL
        /// </summary>
        /// <param name="configDataHashtable">Hashtable configDataHashtable</param>
        /// <param name="xmlNode">XmlNode xmlNode</param>
        /// <returns>The nested formulas as XSL string</returns>
        private static string GetNestedFormulaXSL(Hashtable configDataHashtable, XmlNode xmlNode)
        {
            StringBuilder xsltText = new StringBuilder();
            if (xmlNode.ChildNodes.Count > 0)
            {
                foreach (XmlNode childNode in xmlNode.ChildNodes)
                {
                    foreach (XmlAttribute attribute in childNode.Attributes)
                    {
                        if ("ERROR" != attribute.Name.ToString())
                        {
                            xsltText.Append("<xsl:if test=\"" + attribute.Value.ToString() + "\">");
                            if (childNode.ChildNodes.Count > 0)
                            {
                                xsltText.Append(GetNestedFormulaXSL(configDataHashtable, childNode));
                            }

                            if (childNode.ChildNodes.Count <= 0)
                            {
                                xsltText.Append("<xsl:attribute name=\"$ErrorMessage$\">");
                                xsltText.Append("<xsl:text>Error</xsl:text>");
                                xsltText.Append("</xsl:attribute>");
                            }

                            xsltText.Append("</xsl:if>");
                        }
                    }
                }
            }

            return xsltText.ToString();
        } 
        #endregion
    }
}

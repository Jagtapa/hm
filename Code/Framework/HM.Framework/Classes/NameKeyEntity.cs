// <copyright file="NameKeyEntity.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class is used to for name key entity.
// </summary>

namespace HM.Framework
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// This class defines a single Name and a value of any Entity
    /// </summary>
    [DataContract]
    [Serializable]
    public class NameKeyEntity : EntityBase
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the NameKeyEntity class.
        /// </summary>
        public NameKeyEntity()
        {
            this.Name = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the NameKeyEntity class.
        /// </summary>
        /// <param name="name">The Name of the Entity</param>
        /// <param name="key">The Key of the entity</param>
        public NameKeyEntity(string name, int key)
        {
            this.Key = key;
            this.Name = name;
        }

        /// <summary>
        /// Initializes a new instance of the NameKeyEntity class.
        /// </summary>
        /// <param name="name">The Name of the Entity</param>
        /// <param name="key">The Key of the entity</param>
        /// <param name="compositeKey">A Composite Key of the entity</param>
        public NameKeyEntity(string name, int key, string compositeKey)
        {
            this.Key = key;
            this.Name = name;
            this.CompositeID = compositeKey;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Name property
        /// </summary>
        [DataMember]
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Composite Key of the entity
        /// </summary>
        [DataMember]
        public string CompositeID
        {
            get;
            set;
        }
        #endregion
    }
}

// <copyright file="CacheManager.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class used to manage caching.
// </summary>

namespace HM.Framework
{
    using Microsoft.Practices.EnterpriseLibrary.Caching;

    /// <summary>
    /// Cache manager class to handle chaching operations..
    /// </summary>
    public sealed class CacheManager
    {
        #region Private Members
        /// <summary>
        /// The cach manager object.
        /// </summary>
        private static ICacheManager cacheManager;
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes static members of the CacheManager class.
        /// </summary>
        /// <returns></returns>
        static CacheManager()
        {
            cacheManager = CacheFactory.GetCacheManager();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Use this method to add the data into the cache.
        /// </summary>
        /// <param name="key">A unique identifier.</param>
        /// <param name="value">The data which need to be pushed into cache.</param>
        public static void Add(string key, object value)
        {
            if (cacheManager.Contains(key))
            {
                cacheManager.Remove(key);
            }

            cacheManager.Add(key, value);
        }

        /// <summary>
        /// Function to get value from Cache
        /// </summary>
        /// <param name="key">string key</param>
        /// <returns>Cached data for key.</returns>
        public static object GetData(string key)
        {
            return cacheManager.GetData(key);
        }

        /// <summary>
        /// Function to Add SQL Dependancy in the application
        /// This will only work for web cache
        /// </summary>
        /// <param name="key">string key</param>
        /// <param name="value">object value</param>
        /// <param name="databaseEntryName">string databaseEntryName</param>
        /// <param name="tableName">string tableName</param>
        public static void Add(string key, object value, string databaseEntryName, string tableName)
        {
            if (cacheManager.Contains(key))
            {
                cacheManager.Remove(key);
            }

            cacheManager.Add(key, value);
        }

        /// <summary>
        /// Function to get value from Cache
        /// </summary>
        /// <param name="key">string key</param>
        /// <returns>true or false</returns>
        public static bool Contains(string key)
        {
            return cacheManager.Contains(key);
        }

        /// <summary>
        /// Method to return Caching DB Name
        /// </summary>
        /// <returns>DB entry name.</returns>
        public static string GetCachingDBEntryName()
        {
            return CacheConstants.CACHESQLDEPDBENTRYNAME;
        }

        /// <summary>
        /// Method to remove cache
        /// </summary>
        /// <param name="key">string key</param>
        public static void Remove(string key)
        {
            cacheManager.Remove(key);
        }

        /// <summary>
        /// Flush the Cache store.
        /// </summary>
        public static void Flush()
        {
            cacheManager.Flush();
        }
        #endregion
    }
}

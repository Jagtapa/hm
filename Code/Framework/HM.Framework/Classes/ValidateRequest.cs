// <copyright file="ValidateRequest.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class is used to validate http requests.
// </summary>
namespace HM.Framework
{
    using System;
    using System.IO;
    using System.Web;

    /// <summary>
    /// This class implements HttpRequest Module and has a handler for the 
    /// httpApplication's begin_request event
    /// </summary>
    public class ValidateRequest : IHttpModule
    {
        #region Public Methods
        /// <summary>
        /// Action to be taken on begin request of http request.
        /// </summary>
        /// <param name="context">HttpApplication context</param>
        void IHttpModule.Init(HttpApplication context)
        {
            // TODO:  Add ValidateRequest.Init implementation
            context.BeginRequest += new EventHandler(this.OnBeginRequest);
        }

        /// <summary>
        /// The dipose method.
        /// </summary>
        public void Dispose()
        {
            // TODO:  Add ValidateRequest.Dispose implementation
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Event Handler for Application_BeginRequest Event
        /// </summary>
        /// <param name="source"> The HttpApplication </param>
        /// <param name="eventArgs"> Event args </param>
        private void OnBeginRequest(object source, EventArgs eventArgs)
        {
            /* Check for the mallicious code */
            HttpRequest httpRequest = (source as HttpApplication).Request;
            this.CheckMalicious(source as HttpApplication, httpRequest.RawUrl);
        }

        /// <summary>
        /// This function checks the requested URL being mallicious
        /// </summary>
        /// <param name="source"> The httpApplication </param>
        /// <param name="strRawUrl"> The Url of the current program </param>
        private void CheckMalicious(HttpApplication source, string strRawUrl)
        {
            HttpRequest httpRequest = (source as HttpApplication).Request;
            string strPhysicalPath = httpRequest.PhysicalPath;

            /* Check for malfunctioning URL present in the request */
            if (httpRequest.Path.IndexOf('\\') >= 0 || Path.GetFullPath(strPhysicalPath) != strPhysicalPath)
            {
                throw new HttpException(404, "Wrong path found in the requested URL");
            }

            /* Check for the presence of a Query string parameter */
            int queryStart = strRawUrl.IndexOf('?');

            if (queryStart != -1)
            {
                // Since QueryString parameters are considered to be mallicious code
                throw new HttpException(404, "Query string used in the requested URL");
            }
        }
        #endregion
    }
}

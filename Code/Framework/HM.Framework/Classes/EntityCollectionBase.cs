// <copyright file="EntityCollectionBase.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class is used to handle all the collection operations.
// </summary>
namespace HM.Framework
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Text;

    /// <summary>
    /// This is abstract class which will be used to store the entites collection.
    /// It will be used as a base classes for all collections in the application for 
    /// e.g. ElementCollection, TerritoryCollection.
    /// The basic intention of this class is to give general implementation of add, fetch, sorting.
    /// </summary>
    /// <typeparam name="EntityType">The EntityType.</typeparam>
    [Serializable]
    [CollectionDataContract]
    public class EntityCollectionBase<EntityType> : CollectionBase, IList, IBindingList, IEntityCollectionBase, IEnumerable<EntityType>, IEnumerable
    {
        #region Private Variables.
        /// <summary>
        /// Will hold the sorted collection.
        /// </summary>
        private ArrayList sortedList = new ArrayList();

        /// <summary>
        /// Will contain the original collection before sorting.
        /// </summary>
        private ArrayList originalList;

        /// <summary>
        /// Boolean values indicating whether sorting took place.
        /// </summary>
        private bool isSorted = false;

        /// <summary>
        /// Will store the value by which the sorting took place.
        /// </summary>
        [NonSerialized]
        private PropertyDescriptor sortBy;

        /// <summary>
        /// <see cref="ListSortDirection "/> will store the direction by which the sorting took place.
        /// </summary>
        [NonSerialized]
        private ListSortDirection sortDirection = ListSortDirection.Descending;

        /// <summary>
        /// Provides data for the <see cref="ListChangedEvent"/> event.  
        /// </summary>
        [NonSerialized]
        private ListChangedEventArgs resetEvent = new ListChangedEventArgs(ListChangedType.Reset, -1);

        /// <summary>
        /// On list change event handler.
        /// </summary>
        [NonSerialized]
        private ListChangedEventHandler onListChangedEventHandler;
        #endregion

        #region Event
        /// <summary>
        /// Occurs when the list managed by the <see cref="EntityCollection"/> changes.
        /// </summary>
        public event ListChangedEventHandler ListChanged
        {
            add
            {
                this.onListChangedEventHandler += value;
            }

            remove
            {
                this.onListChangedEventHandler -= value;
            }
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the filtered entity collection.
        /// Usage Example 
        /// foreach (IJob job in response.Jobs)
        /// {
        /// ((EntityCollectionBase &lt;IJourney&gt;)response.Journeys).Filter = delegate(IJourney e)
        /// {
        /// return (e.JOB_ID == job.Job_ID);
        /// };
        /// {
        /// </summary>
        public Predicate<EntityType> Filter
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a value indicating whether you can add items to the list using <see cref="IBindingList.AddNew"/>.
        /// </summary>
        bool IBindingList.AllowNew
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether you can remove items from the list, using <see cref="IList.Remove"/> or <see cref="IList.RemoveAt"/>.
        /// </summary>
        bool IBindingList.AllowRemove
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether you can update items in the list.
        /// </summary>
        bool IBindingList.AllowEdit
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether the items in the list are sorted.
        /// </summary>
        bool IBindingList.IsSorted
        {
            get { return this.isSorted; }
        }

        /// <summary>
        /// Gets a value indicating whether the list supports sorting.
        /// </summary>
        bool IBindingList.SupportsSorting
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether the list supports searching using the <see cref="IBindingList.Find"/> method.
        /// </summary>
        bool IBindingList.SupportsSearching
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether an <see cref="ListChanged"/> event is raised when the list changes or an item in the list changes.
        /// </summary>
        bool IBindingList.SupportsChangeNotification
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the <see cref="PropertyDescriptor"/> that is being used for sorting.
        /// </summary>
        PropertyDescriptor IBindingList.SortProperty
        {
            get { return this.sortBy; }
        }

        /// <summary>
        /// Gets the direction of the sort.
        /// </summary>
        ListSortDirection IBindingList.SortDirection
        {
            get { return this.sortDirection; }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get or set the index.
        /// </summary>
        /// <param name="index">The int index.</param>
        /// <returns>The entity.</returns>
        public virtual EntityType this[int index]
        {
            get
            {
                return (EntityType)List[index];
            }

            set
            {
                this.List[index] = value;
            }
        }

        /// <summary>
        /// Get enumerator.
        /// </summary>
        /// <returns>enumerated list.</returns>
        IEnumerator<EntityType> IEnumerable<EntityType>.GetEnumerator()
        {
            foreach (EntityType item in this)
            {
                if (this.Filter == null || this.Filter(item))
                {
                    yield return item;
                }
            }
        }

        /// <summary>
        /// Get the enumerator.
        /// </summary>
        /// <returns>An Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)((IEnumerable<EntityType>)this).GetEnumerator();
        }

        /// <summary>
        /// Adds the entity of type EntityType into the collection.
        /// </summary>
        /// <param name="entityType">Entity which to be added.</param>
        /// <returns>The ID of the newly added entity</returns>
        public virtual int Add(EntityType entityType)
        {
            return List.Add(entityType);
        }

        /// <summary>
        /// Add entity in to list.
        /// </summary>
        /// <param name="entityBase">The entity to add</param>
        /// <returns>sccess failure</returns>
        public virtual int Add(IEntityBase entityBase)
        {
            EntityType entityType;
            if (entityBase is EntityType)
            {
                entityType = (EntityType)entityBase;
                return this.Add(entityType);
            }
            else
            {
                throw new Exception("Type Mismatch");
            }
        }

        /// <summary>
        /// It will return the entity from collection based on supplied key.
        /// </summary>
        /// <param name="key">The key of entity.</param>
        /// <returns>An entity.</returns>
        public EntityType Fetch(int key)
        {
            if (this.List.Count == 0)
            {
                return default(EntityType);
            }

            PropertyDescriptor searchby = TypeDescriptor.GetProperties(this.List[0]).Find("Key", false);
            int j = ((IBindingList)this).Find(searchby, key);
            if (j > -1)
            {
                return this[j];
            }
            else
            {
                return default(EntityType);
            }
        }

        /// <summary>
        /// It is used to fetch entity(s) based on criteria.
        /// Given a search column and search value. This method returns entity(s)
        /// </summary>
        /// <param name="searchColumn">The column on which search to be performed.</param>
        /// <param name="searchValue">The search value to be used match.</param>
        /// <returns>A collection of type EntityCollectionBase which will have results.</returns>
        public EntityType Fetch(string searchColumn, string searchValue)
        {
            try
            {
                if (this.List.Count == 0)
                {
                    return default(EntityType);
                }

                PropertyDescriptor searchby = TypeDescriptor.GetProperties(this.List[0]).Find(searchColumn.ToString(), false);
                int j = ((IBindingList)this).Find(searchby, searchValue);
                if (j > -1)
                {
                    return this[j];
                }
                else
                {
                    return default(EntityType);
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// This is to get all data which has the occurence of sSearchValue in it 
        /// </summary>
        /// <param name="searchColumn">string searchColumn</param>
        /// <param name="searchValue">string searchValue</param>
        /// <returns>The IEntityCollectionBase.</returns>
        public IEntityCollectionBase FetchAllLike(string searchColumn, string searchValue)
        {
            try
            {
                if (this.List.Count == 0)
                {
                    return null;
                }

                PropertyDescriptor searchby = TypeDescriptor.GetProperties(this.List[0]).Find(searchColumn.ToString(), false);
                IEntityCollectionBase entityCollection = new EntityCollectionBase<EntityType>();
                foreach (IEntityBase entityBase in this.List)
                {
                    if (searchby.GetValue(entityBase) != null && !(searchby.GetValue(entityBase).ToString().IndexOf(searchValue) < 0))
                    {
                        entityCollection.Add(entityBase);
                    }
                }

                return entityCollection;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// This is to get all data which begins with the sSearchValue.
        /// </summary>
        /// <param name="searchColumn">string searchColumn</param>
        /// <param name="searchValue">string searchValue</param>
        /// <param name="startWithSearch">bool startWithSearch</param>
        /// <returns>The IEntityCollectionBase</returns>
        public IEntityCollectionBase FetchAllLike(string searchColumn, string searchValue, bool startWithSearch)
        {
            try
            {
                if (!startWithSearch)
                {
                    return this.FetchAllLike(searchColumn, searchValue);
                }

                // TODO: Cover the implementation to do FetchAll
                if (this.List.Count == 0)
                {
                    return null;
                }

                PropertyDescriptor searchby = TypeDescriptor.GetProperties(this.List[0]).Find(searchColumn.ToString(), false);
                IEntityCollectionBase entityCollection = new EntityCollectionBase<EntityType>();
                foreach (IEntityBase entityBase in this.List)
                {
                    if (searchby.GetValue(entityBase) != null && searchby.GetValue(entityBase).ToString().StartsWith(searchValue, true, System.Globalization.CultureInfo.InvariantCulture))
                    {
                        entityCollection.Add(entityBase);
                    }
                }

                return entityCollection;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// It is used to fetch entity(s) based on criteria.
        /// Given a search column and search value. This method returns entity(s)
        /// </summary>
        /// <param name="searchColumn">The column on which search to be performed.</param>
        /// <param name="searchValue">The search value to be used match.</param>
        /// <returns>A collection of type EntityCollectionBase which will have results.</returns>
        public EntityType Fetch(string searchColumn, object searchValue)
        {
            try
            {
                if (this.List.Count == 0)
                {
                    return default(EntityType);
                }

                PropertyDescriptor searchby = TypeDescriptor.GetProperties(this.List[0]).Find(searchColumn.ToString(), false);
                int j = ((IBindingList)this).Find(searchby, searchValue);
                if (j > -1)
                {
                    return this[j];
                }
                else
                {
                    return default(EntityType);
                }
            }
            catch 
            {
                throw;
            }
        }

        /// <summary>
        /// It is used to Get Index based on criteria.
        /// Given a search column and search value. This method returns entity(s)
        /// </summary>
        /// <param name="searchColumn">The column on which search to be performed.</param>
        /// <param name="searchValue">The search value to be used match.</param>
        /// <returns>Index of type Integer</returns>
        public int GetIndex(string searchColumn, string searchValue)
        {
            try
            {
                if (this.List.Count == 0)
                {
                    return -1;
                }

                PropertyDescriptor searchby = TypeDescriptor.GetProperties(this.List[0]).Find(searchColumn.ToString(), false);
                int j = ((IBindingList)this).Find(searchby, searchValue);
                if (j > -1)
                {
                    return j;
                }
                else
                {
                    return -1;
                }
            }
            catch 
            {
                throw;
            }
        }

        /// <summary>
        /// It is used to Get Index based on criteria.
        /// Given a search column and search value. This method returns integer
        /// </summary>
        /// <param name="searchColumn">The column on which search to be performed.</param>
        /// <param name="searchValue">The search value to be used match.</param>
        /// <returns>Index of type Integer</returns>
        public int GetIndex(string searchColumn, object searchValue)
        {
            // TODO: Cover the implementation to do FetchAll
            if (this.List.Count == 0)
            {
                return 0;
            }

            PropertyDescriptor searchby = TypeDescriptor.GetProperties(this.List[0]).Find(searchColumn.ToString(), false);
            IEntityCollectionBase entityCollection = new EntityCollectionBase<EntityType>();  // this.MemberwiseClone() as IEntityCollectionBase ;

            int index = 0;

            foreach (IEntityBase entityBase in this.List)
            {
                if (searchby.GetValue(entityBase).Equals(searchValue))
                {
                    return index;
                }

                index++;
            }

            return index;
        }

        /// <summary>
        /// Finds a collection of <see cref="IEntity" /> objects in the current list.
        /// </summary>
        /// <param name="searchColumn">The column on which search to be performed.</param>
        /// <param name="searchValue">The search value to be used match.</param>
        /// <returns>A collection of type EntityCollectionBase which will have results.</returns>
        public IEntityCollectionBase FetchAll(string searchColumn, string searchValue)
        {
            // TODO: Cover the implementation to do FetchAll
            if (this.List.Count == 0)
            {
                return null;
            }

            // PropertyDescriptorCollection props = TypeDescriptor.GetProperties(this.List[0]);
            // PropertyDescriptor searchby = props["sSearchColumn"];
            PropertyDescriptor searchby = TypeDescriptor.GetProperties(this.List[0]).Find(searchColumn.ToString(), false);

            IEntityCollectionBase entityCollection = new EntityCollectionBase<EntityType>();  // this.MemberwiseClone() as IEntityCollectionBase ;

            foreach (IEntityBase entityBase in this.List)
            {
                if (searchby.GetValue(entityBase).Equals(searchValue))
                {
                    // IEntityBase copyEntity = oEntity
                    entityCollection.Add(entityBase);
                }
            }

            return entityCollection;
        }

        /// <summary>
        /// Fetch all records from collection.
        /// </summary>
        /// <param name="searchColumn">The search column.</param>
        /// <param name="searchValue">The search column value.</param>
        /// <returns>The IEntityCollectionBase</returns>
        public IEntityCollectionBase FetchAll(string searchColumn, object searchValue)
        {
            // TODO: Cover the implementation to do FetchAll
            if (this.List.Count == 0)
            {
                return null;
            }

            PropertyDescriptor searchby = TypeDescriptor.GetProperties(this.List[0]).Find(searchColumn.ToString(), false);
            IEntityCollectionBase entityCollection = new EntityCollectionBase<EntityType>();  // this.MemberwiseClone() as IEntityCollectionBase ;

            foreach (IEntityBase entityBase in this.List)
            {
                if (searchby.GetValue(entityBase).Equals(searchValue))
                {
                    entityCollection.Add(entityBase);
                }
            }

            return entityCollection;
        }

        /// <summary>
        /// Function to check the multiple values in the collection for a single property i.e. Or Condition
        /// </summary>
        /// <param name="searchColumn">Column to be searched</param>
        /// <param name="searchValue">object array</param>
        /// <returns>The IEntityCollectionBase</returns>
        public IEntityCollectionBase FetchAll(string searchColumn, object[] searchValue)
        {
            // TODO: Cover the implementation to do FetchAll
            if (this.List.Count == 0)
            {
                return null;
            }

            bool isFlagFound = false;
            PropertyDescriptor searchby = TypeDescriptor.GetProperties(this.List[0]).Find(searchColumn.ToString(), false);
            IEntityCollectionBase entityCollection = new EntityCollectionBase<EntityType>();  // this.MemberwiseClone() as IEntityCollectionBase ;
            foreach (IEntityBase entityBase in this.List)
            {
                isFlagFound = false;
                for (int i = 0; i < searchValue.Length; i++)
                {
                    if (searchby.GetValue(entityBase).Equals(searchValue[i]))
                    {
                        entityCollection.Add(entityBase);
                        isFlagFound = true;
                    }

                    if (isFlagFound == true)
                    {
                        break;
                    }
                }
            }

            return entityCollection;
        }

        /// <summary>
        /// Sercah collection for range.
        /// </summary>
        /// <param name="searchColumn">The column name .</param>
        /// <param name="fromSearchValue">From value to search.</param>
        /// <param name="toSearchValue">To Value  to search.</param>
        /// <returns>Filtered collection.</returns>
        public IEntityCollectionBase FetchAllBetween(string searchColumn, object fromSearchValue, object toSearchValue)
        {
            // TODO: Cover the implementation to do FetchAll
            if (this.List.Count == 0)
            {
                return null;
            }

            PropertyDescriptor searchby = TypeDescriptor.GetProperties(this.List[0]).Find(searchColumn.ToString(), false);
            IEntityCollectionBase entityCollection = new EntityCollectionBase<EntityType>();  // this.MemberwiseClone() as IEntityCollectionBase ;
            Type type = searchby.GetValue(this.List[0] as IEntityBase).GetType();

            if (typeof(DateTime).Equals(type))
            {
                if (typeof(DateTime).Equals(fromSearchValue.GetType()) && typeof(DateTime).Equals(toSearchValue.GetType()))
                {
                    foreach (IEntityBase entityBase in this.List)
                    {
                        if ((DateTime)searchby.GetValue(entityBase) >= ((DateTime)fromSearchValue) && (DateTime)searchby.GetValue(entityBase) <= (DateTime)toSearchValue)
                        {
                            entityCollection.Add(entityBase);
                        }
                    }
                }
                else
                {
                    throw new Exception("The search field type is different than the field type");
                }
            }
            else
            {
                throw new Exception("The specified type is not supported for this type");
            }

            return entityCollection;
        }

        /// <summary>
        /// Fetch All Method to find the bitwise values
        /// </summary>
        /// <param name="searchColumn">Property to Search</param>
        /// <param name="searchValue">Value to be searched</param>
        /// <returns>The IEntityCollectionBase</returns>
        public IEntityCollectionBase FetchAllBitwise(string searchColumn, int searchValue)
        {
            // TODO: Cover the implementation to do FetchAll
            if (this.List.Count == 0)
            {
                return null;
            }

            PropertyDescriptor searchby = TypeDescriptor.GetProperties(this.List[0]).Find(searchColumn.ToString(), false);
            IEntityCollectionBase entityCollection = new EntityCollectionBase<EntityType>();  // this.MemberwiseClone() as IEntityCollectionBase ;
            foreach (IEntityBase entityBase in this.List)
            {
                int columnValue = int.Parse(searchby.GetValue(entityBase).ToString());
                System.Collections.BitArray barrDBValue = new System.Collections.BitArray(System.BitConverter.GetBytes(columnValue));
                System.Collections.BitArray barrCheckFor = new System.Collections.BitArray(System.BitConverter.GetBytes(searchValue));

                for (int index = 0; index < barrCheckFor.Length; index++)
                {
                    if (barrCheckFor[index])
                    {
                        if (barrDBValue[index])
                        {
                            entityCollection.Add(entityBase);
                        }
                    }
                }
            }

            return entityCollection;
        }

        /// <summary>
        /// Refresh the values of collection
        /// </summary>
        /// <param name="searchColumn">Search column</param>
        /// <param name="searchValue">Search value</param>
        /// <param name="refreshColumn">Refresh Column</param>
        /// <param name="refreshValue">Refresh value</param>
        public void Update(string searchColumn, object searchValue, string refreshColumn, object refreshValue)
        {
            // TODO: Cover the implementation to do FetchAll
            if (this.List.Count == 0)
            {
                return;
            }

            PropertyDescriptor searchby = TypeDescriptor.GetProperties(this.List[0]).Find(searchColumn.ToString(), false);

            foreach (IEntityBase entityBase in this.List)
            {
                if (searchby.GetValue(entityBase).Equals(searchValue))
                {
                    Type type = entityBase.GetType();
                    PropertyInfo propertyInfo = type.GetProperty(refreshColumn);
                    propertyInfo.SetValue(entityBase, refreshValue, null);
                }
            }
        }

        /// <summary>
        /// Update the complete entity (replace with new one)
        /// </summary>
        /// <param name="searchColumn">Search column</param>
        /// <param name="searchValue">Search value</param>
        /// <param name="newEntityBase">New entity need to be replaced</param>
        public void Update(string searchColumn, object searchValue, IEntityBase newEntityBase)
        {
            // TODO: Cover the implementation to do FetchAll
            if (this.List.Count == 0)
            {
                return;
            }

            PropertyDescriptor searchby = TypeDescriptor.GetProperties(this.List[0]).Find(searchColumn.ToString(), false);

            foreach (IEntityBase entityBase in this.List)
            {
                if (searchby.GetValue(entityBase).Equals(searchValue))
                {
                    object value;
                    foreach (PropertyInfo propertyInfo in entityBase.GetType().GetProperties())
                    {
                        PropertyDescriptor propertyDescriptor = TypeDescriptor.GetProperties(newEntityBase).Find(propertyInfo.Name, false);
                        value = propertyDescriptor.GetValue(newEntityBase);
                        propertyDescriptor.SetValue(entityBase, value);
                    }
                }
            }
        }

        /// <summary>
        /// This method will perform soring
        /// </summary>
        /// <param name="sortDirection">SortOrder sortDirection</param>
        /// <param name="sortColumn">string sortColumn</param>
        public void Sort(SortOrder sortDirection, string sortColumn)
        {
            if (this.List.Count == 0)
            {
                return;
            }

            this.sortBy = TypeDescriptor.GetProperties(this.List[0]).Find(sortColumn.ToString(), false);
            this.sortDirection = (ListSortDirection)Enum.Parse(typeof(ListSortDirection), sortDirection.ToString());
            this.SortList();
        }

        /// <summary>
        /// Returns a String that represents the current EntityCollectionBase.
        /// </summary>
        /// <returns>string object.</returns>
        public override string ToString()
        {
            StringBuilder output = new StringBuilder();

            // TODO: Remove this implementation of looping in the 'foreach' cordon and add the 'for' loop instead.
            foreach (EntityType p in this.List)
            {
                output.Append(p.ToString());
            }

            return output.ToString();
        }

        /// <summary>
        /// Removes the sort on collection entity column.
        /// </summary>
        public void RemoveSort()
        {
            if (this.isSorted)
            {
                InnerList.Clear();
                foreach (object obj in this.originalList)
                {
                    InnerList.Add(obj);
                }

                this.isSorted = false;
                this.originalList = null; // destroy
                // TODO: what about this event implementation ????
                this.OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, 0));
            }
        }

        /// <summary>
        /// Adds a new item to the list.
        /// </summary>
        /// <returns>The item added to the list.</returns>
        object IBindingList.AddNew()
        {
            // TODO: the implementation of this method should be deferred to the sub class that will create the object of apprpriate type ie. Territory.
            return 0;
        }

        /// <summary>
        /// Sorts the list based on a <see cref="PropertyDescriptor"/> and a <see cref="ListSortDirection"/>.
        /// </summary>
        /// <param name="property">The <see cref="PropertyDescriptor"/> to sort by.</param>
        /// <param name="direction">One of the <see cref="ListSortDirection"/> values.</param>
        void IBindingList.ApplySort(PropertyDescriptor property, ListSortDirection direction)
        {
            this.sortBy = property;
            this.sortDirection = direction;

            // TODO: add the implementation to do IBindingList.ApplySort in this abstract class.
            this.SortList();
        }

        /// <summary>
        /// Returns the index of the row that has the given <see cref="PropertyDescriptor"/>.
        /// </summary>
        /// <param name="propertyDescriptor">The <see cref="PropertyDescriptor"/> to search on. </param>
        /// <param name="key">The value of the property parameter to search for.</param>
        /// <returns>The index of the row that has the given <see cref="PropertyDescriptor"/>.</returns>
        int IBindingList.Find(PropertyDescriptor propertyDescriptor, object key)
        {
            // TODO: change the enumeartion from foreach to for in IBindingList.Find
            foreach (EntityType entityType in this.List)
            {
                if (propertyDescriptor.GetValue(entityType).Equals(key))
                {
                    return this.List.IndexOf(entityType);
                }
            }

            return -1;
        }

        /// <summary> Removes the from the indexes used for searching.
        /// </summary>
        /// <param name="property">The property</param>
        void IBindingList.RemoveIndex(PropertyDescriptor property)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Add propert index.
        /// </summary>
        /// <param name="property">The property.</param>
        void IBindingList.AddIndex(PropertyDescriptor property)
        {
            // TODO: IBindingList.AddIndex method will not be supported .???
            throw new NotSupportedException();
        }

        /// <summary>
        /// Conver the object into XML.
        /// </summary>
        /// <returns>The object as an XML string</returns>
        public string ToXML()
        {
            StringBuilder output = new StringBuilder();
            output.Append("<Objects>");
            Type typeToReflect = null;
            System.Reflection.MemberInfo[] membersInfo = null;
            if (this.List.Count >= 0)
            {
                typeToReflect = this.List[0].GetType();
                membersInfo = typeToReflect.GetMembers();
            }

            for (int i = 0; i < this.List.Count; i++)
            {
                object objTypeToReflect = Activator.CreateInstance(typeToReflect);
                objTypeToReflect = this[i];
                output.Append((string)typeToReflect.InvokeMember(
                    "ToXML",
                    BindingFlags.Default | BindingFlags.InvokeMethod,
                    null,
                    objTypeToReflect,
                    null));
            }

            output.Append("</Objects>");
            return output.ToString();
        }

        /// <summary>
        /// converts object to string.
        /// </summary>
        /// <returns>XML string object.</returns>
        public string ConvertToXML()
        {
            StringBuilder output = new StringBuilder();
            StringBuilder refOutput = new StringBuilder();
            ArrayList referenceObjects = new ArrayList();
            output.Append("<Objects>");

            for (int i = 0; i <= this.Count - 1; i++)
            {
                PropertyInfo[] objFields = this.List[i].GetType().GetProperties();

                output.Append("<" + objFields[0].ReflectedType.Name.ToString() + ">");

                output.Append("<" + objFields[0].ReflectedType.Name.ToString());

                foreach (PropertyInfo prop in objFields)
                {
                    if (prop.CanRead)
                    {
                        if ((!prop.PropertyType.IsValueType) && (prop.PropertyType != typeof(string)) && (prop.PropertyType != typeof(Error)))
                        {
                            object customObject = prop.GetValue(this[i] as object, null);
                            if (null != customObject)
                            {
                                refOutput.Append(this.ConvertToXML(customObject));
                            }
                        }
                        else
                        {
                            object strObjectString = prop.GetValue(this.List[i] as object, null);
                            if (null != strObjectString)
                            {
                                output.Append(" " + prop.Name.ToString() + "='");
                                strObjectString = strObjectString.ToString().Replace("&", "&amp;");
                                strObjectString = strObjectString.ToString().Replace("<", "&lt;");
                                strObjectString = strObjectString.ToString().Replace(">", "&gt;");
                                output.Append(strObjectString.ToString() + "'");
                            }
                        }
                    }
                }

                output.Append("/>");
                output.Append(refOutput.ToString());
                refOutput.Remove(0, refOutput.Length);
                output.Append("</" + objFields[0].ReflectedType.Name.ToString() + ">");
            }

            output.Append("</Objects>");
            return output.ToString();
        }

        /// <summary>
        /// Function for Shallow Clonning 
        /// </summary>
        /// <returns>The IEntityCollectionBase</returns>
        public IEntityCollectionBase Clone()
        {
            return this.MemberwiseClone() as IEntityCollectionBase;
        }

        /// <summary>
        /// Function for Deep clonning
        /// </summary>
        /// <param name="isDeepCopy">bool isDeepCopy.</param>
        /// <returns>The IEntityCollectionBase</returns>
        public IEntityCollectionBase Clone(bool isDeepCopy)
        {
            if (isDeepCopy)
            {
                MemoryStream memoryStream = new MemoryStream();
                BinaryFormatter binaryFormater = new BinaryFormatter();

                if (this.sortedList == null)
                {
                    this.sortedList = new ArrayList();
                }
                else
                {
                    this.sortedList.Clear();
                }

                binaryFormater.Serialize(memoryStream, this);
                memoryStream.Position = 0;
                return binaryFormater.Deserialize(memoryStream) as IEntityCollectionBase;
            }
            else
            {
                return this.Clone();
            }
        }

        /// <summary>
        /// Gets the concatenated list from entity collection.
        /// </summary>
        /// <param name="seperator">The seperator.</param>
        /// <param name="columnName">The column name.</param>
        /// <returns>concatenated list.</returns>
        public string GetConcatenatedList(char seperator, string columnName)
        {
            try
            {
                string value = string.Empty;
                if (this.List.Count == 0)
                {
                    return value;
                }
                else
                {
                    PropertyInfo pi = List[0].GetType().GetProperty(columnName);
                    for (int count = 0; count <= this.List.Count - 1; count++)
                    {
                        if (value == string.Empty)
                        {
                            value = pi.GetValue(List[count], null).ToString();
                        }
                        else
                        {
                            value += seperator + pi.GetValue(List[count], null).ToString();
                        }
                    }
                }

                return value;
            }
            catch 
            {
                throw;
            }
        }

        /// <summary>
        /// It is used to fetch entity(s) based on criteria.
        /// Given a search column and search value. This method returns entity(s)
        /// </summary>
        /// <param name="searchColumn">The column on which search to be performed.</param>
        /// <param name="searchValue">The search value to be used match.</param>
        /// <returns>A collection of type EntityCollectionBase which will have results.</returns>
        IEntityBase IEntityCollectionBase.Fetch(string searchColumn, object searchValue)
        {
            return this.Fetch(searchColumn, searchValue) as IEntityBase;
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Raises the ListChanged event.
        /// </summary>
        /// <param name="ev">A <see cref="ListChangedEventArgs"/> that contains the event data.</param>
        protected virtual void OnListChanged(ListChangedEventArgs ev)
        {
            if (this.onListChangedEventHandler != null)
            {
                this.onListChangedEventHandler(this, ev);
            }
        }

        /// <summary>
        /// Raises the ListChanged event.
        /// </summary>
        protected override void OnClearComplete()
        {
            if (this.isSorted)
            {
                this.originalList.Clear();
            }

            this.OnListChanged(this.resetEvent);
        }

        /// <summary>
        /// Raises the InsertComplete event.
        /// Performs additional custom processes after inserting a new element into the EntityCollectionBase instance.
        /// </summary>
        /// <param name="index">The zero-based index at which to insert value.</param>
        /// <param name="value">The new value of the element at index.</param>
        protected override void OnInsertComplete(int index, object value)
        {
            if (this.isSorted)
            {
                this.originalList.Add(value);
            }

            this.OnListChanged(new ListChangedEventArgs(ListChangedType.ItemAdded, index));
        }

        /// <summary>
        /// Raises the RemoveComplete event.
        /// Performs additional custom processes after removing a new element into the EntityCollectionBase instance.
        /// </summary>
        /// <param name="index">The zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove from index.</param>
        protected override void OnRemoveComplete(int index, object value)
        {
            if (this.isSorted)
            {
                this.originalList.Remove(value);
            }

            this.OnListChanged(new ListChangedEventArgs(ListChangedType.ItemDeleted, index));
        }

        /// <summary>
        /// Raises the SetComplete event.
        /// Performs additional custom processes after setting a value in the EntityCollectionBase instance.
        /// </summary>
        /// <param name="index">The zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">The value to replace with newValue.</param>
        /// <param name="newValue">The new value of the element at index.</param>
        protected override void OnSetComplete(int index, object oldValue, object newValue)
        {
            if (oldValue != newValue)
            {
                this.OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, index));
            }
        }
        #endregion "Typed Event Handlers"

        #region Private Methods
        /// <summary>
        /// Conver the object into XML.
        /// </summary>
        /// <param name="userObject">The object to be converted to XML</param>
        /// <returns>The object as an XML string</returns>
        private string ConvertToXML(object userObject)
        {
            StringBuilder output = new StringBuilder();
            StringBuilder refOutput = new StringBuilder();
            Type objectType = userObject.GetType().BaseType;
            int count = objectType == typeof(object) ? 1 : (userObject as IEntityCollectionBase).Count;
            output.Append(objectType == typeof(object) ? "<" + userObject.GetType().Name + ">" : "<Objects>");
            for (int i = 0; i <= count - 1; i++)
            {
                PropertyInfo[] objectFields = objectType == typeof(object) ? userObject.GetType().GetProperties() : (userObject as IEntityCollectionBase)[i].GetType().GetProperties();
                object tempObject = objectType == typeof(object) ? userObject : (userObject as IEntityCollectionBase)[i] as object;
                output.Append("<" + objectFields[0].ReflectedType.Name.ToString() + ">");
                output.Append("<" + objectFields[0].ReflectedType.Name.ToString());
                foreach (PropertyInfo propertyInfo in objectFields)
                {
                    if (propertyInfo.CanRead)
                    {
                        if ((!propertyInfo.PropertyType.IsValueType) && (propertyInfo.PropertyType != typeof(string)) && (propertyInfo.PropertyType != typeof(Error)))
                        {
                            object customObject = propertyInfo.GetValue(tempObject as object, null) as object;
                            if (null != customObject)
                            {
                                refOutput.Append(this.ConvertToXML(customObject));
                            }
                        }
                        else
                        {
                            object strObjectString = propertyInfo.GetValue(tempObject, null);
                            if (null != strObjectString)
                            {
                                output.Append(" " + propertyInfo.Name.ToString() + "='");
                                strObjectString = strObjectString.ToString().Replace("&", "&amp;");
                                strObjectString = strObjectString.ToString().Replace("<", "&lt;");
                                strObjectString = strObjectString.ToString().Replace(">", "&gt;");
                                output.Append(strObjectString.ToString() + "'");
                            }
                        }
                    }
                }

                output.Append("/>");
                output.Append(refOutput.ToString());
                refOutput.Remove(0, refOutput.Length);
                output.Append("</" + objectFields[0].ReflectedType.Name.ToString() + ">");
                output = output.Replace("Key", "ID");
            }

            output.Append(objectType == typeof(object) ? "</" + userObject.GetType().Name + ">" : "</Objects>");
            return output.ToString();
        }

        /// <summary>
        /// Sorts the elements in the EntityCollectionBase .
        /// </summary>
        private void SortList()
        {
            ArrayList list = new ArrayList();
            list.AddRange(this.InnerList);
            try
            {
                this.sortedList.Clear();

                // Load List to sort.
                if (this.sortBy == null)
                {
                    foreach (object obj in this.InnerList)
                    {
                        this.sortedList.Add(new ListItem(obj, obj));
                    }
                }
                else
                {
                    foreach (object obj in this.InnerList)
                    {
                        this.sortedList.Add(new ListItem(this.sortBy.GetValue(obj), obj));
                    }
                }

                // if not already sorted, create a backup of original
                if (!this.isSorted)
                {
                    this.originalList = new ArrayList(List);
                }

                // Sort List
                this.sortedList.Sort();

                // Clear real list.
                InnerList.Clear();

                // re-add item in sorted order.
                if (this.sortDirection == ListSortDirection.Ascending)
                {
                    for (int x = 0; x < this.sortedList.Count; x++)
                    {
                        InnerList.Add(((ListItem)this.sortedList[x]).Item);
                    }
                }
                else
                {
                    for (int x = this.sortedList.Count - 1; x != -1; x--)
                    {
                        InnerList.Add(((ListItem)this.sortedList[x]).Item);
                    }
                }

                this.isSorted = true;
                this.OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, 0));
                this.sortedList = new ArrayList();
            }
            catch (Exception)
            {
                this.InnerList.Clear();
                this.InnerList.AddRange(list);
            }
        }
        #endregion

        #region Nested Class
        /// <summary>
        /// <see cref="ListItem"/>class that sorts items of EntityCollectionBase.
        /// </summary>
        private class ListItem : IComparable
        {
            /// <summary>
            /// Initializes a new instance of the ListItem class.
            /// </summary>
            /// <param name="key">object key.</param>
            /// <param name="item">object Item.</param>
            public ListItem(object key, object item)
            {
                this.Key = key;
                this.Item = item;
            }

            /// <summary>
            /// Gets or sets the Key of the List Item.
            /// </summary>
            public object Key { get; set; }

            /// <summary>
            /// Gets or sets the Item associated with the key.
            /// </summary>
            public object Item { get; set; }

            /// <summary>
            /// Compares the current instance with another object of the same type.
            /// </summary>
            /// <param name="obj">An object to compare with this instance.</param>
            /// <returns>
            /// A 32-bit signed integer that indicates the relative order of the comparands. The return value has these meanings:
            /// <list type="table">
            /// <listheader>
            /// <term>Value</term>
            /// <description>Meaning</description>
            /// </listheader>
            /// <item>
            /// <term>Less than zero</term>
            /// <description>This instance is less than obj.</description>
            /// </item>
            /// <item>
            /// <term>Zero</term>
            /// <description>This instance is equal to obj.</description>
            /// </item>
            /// <item>
            /// <term>Greater than zero</term>
            /// <description>This instance is greater than obj.</description>
            /// </item>
            /// </list>
            /// </returns>
            int IComparable.CompareTo(object obj)
            {
                object target = ((ListItem)obj).Key;

                if (this.Key is IComparable)
                {
                    return ((IComparable)this.Key).CompareTo(target);
                }
                else
                {
                    // Debug.WriteLine("No Comparable");
                    if (target == null)
                    {
                        return 1;
                    }

                    if (this.Key == null)
                    {
                        return -1;
                    }

                    if (this.Key.Equals(target))
                    {
                        return 0;
                    }
                    else
                    {
                        return this.Key.ToString().CompareTo(target.ToString());
                    }
                }
            }

            /// <summary>
            /// Obtains the <see cref="string"/> representation of this instance.
            /// </summary>
            /// <returns>The key of the item.</returns>
            public override string ToString()
            {
                return this.Key.ToString();
            }
        }
        #endregion
    }
}

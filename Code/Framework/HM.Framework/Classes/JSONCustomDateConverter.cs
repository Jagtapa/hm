﻿// <copyright file="JSONCustomDateConverter.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>18-07-2013</date>
// <summary>The custom JSON date formatter.
// </summary>

namespace HM.Framework
{
    using System;
    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    /// <summary>
    /// Custom datetime formatter for json objects
    /// </summary>
    public class JSONCustomDateConverter : DateTimeConverterBase
    {
        /// <summary>
        /// The date format.
        /// </summary>
        private string dateFormat;

        /// <summary>
        /// Initializes a new instance of the JSONCustomDateConverter class.
        /// </summary>
        /// <param name="dateFormat">The date format.</param>
        public JSONCustomDateConverter(string dateFormat)
        {
            this.dateFormat = dateFormat;
        }

        /// <summary>
        /// Can convert method
        /// </summary>
        /// <param name="objectType">Type objectType</param>
        /// <returns>The boolean result.</returns>
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime) || objectType == typeof(Nullable<DateTime>);
        }

        /// <summary>
        /// Read the json.
        /// </summary>
        /// <param name="reader">JsonReader reader</param>
        /// <param name="objectType">Type objectType</param>
        /// <param name="existingValue">object existingValue</param>
        /// <param name="serializer">JsonSerializer serializer</param>
        /// <returns>The object converted.</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value != null && reader.Value.ObjectToString() != string.Empty)
            {
                string dateToConvert = reader.Value.ObjectToString();
                string[] formats = new string[] { "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd hh:mm:ss", "yyyy-MM-dd", "yyyyMMdd", "d-M-yyyy", "d-M-yy", "d-MM-yy", "d-MM-yyyy", "d-MMM-yy", "dd-M-yyyy", "dd-M-yy", "dd-MMM-yy", "dd-MM-yy", "dd-MM-yyyy", "d-MMM-yyyy", "dd-MMM-yyyy", "dMyyyy", "dMyy", "dMMyy", "dMMyyyy", "dMMMyy", "ddMyyyy", "ddMyy", "ddMMMyy", "ddMMyy", "ddMMyyyy", "dMMMyyyy", "ddMMMyyyy", "dd/MM/yy", "dd/MM/yyyy" };
                DateTimeFormatInfo dfi = new DateTimeFormatInfo();
                DateTime dateTime = DateTime.ParseExact(dateToConvert, formats, dfi, DateTimeStyles.None);
                DateTime? date = dateTime;
                return date;
            }

            return reader.Value;
        }

        /// <summary>
        /// Write to json object.
        /// </summary>
        /// <param name="writer">JsonWriter writer</param>
        /// <param name="value">object value</param>
        /// <param name="serializer">JsonSerializer serializer</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(Convert.ToDateTime(value).ToString(this.dateFormat, System.Globalization.CultureInfo.InvariantCulture));
            writer.Flush();
        }
    }
}

// <copyright file="ErrorParams.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class used to maintain error parameters durring execution of code.
// </summary>
namespace HM.Framework
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// This class is used for to create parameter for error messages.
    /// The instance of this class is used to create the parameter defination for error messages.
    /// Create object collection if more than one parameter is passed to error messages.
    /// The Following example demonstrates creation of parameter for error message.
    /// CError oError = new CError() This Error doesen't have any parameter.
    /// oError.Add("ERRORID")
    /// This error has one parameter.
    /// oError.Add("ERRORID",new ErrorParams("lblCourseGroup",true))
    /// this error has more than one parameter.
    /// ErrorParams[] param = new ErrorParams[2];
    /// param[0] = new ErrorParams("lblCourseGroup",true));
    /// param[1] = new ErrorParams("lblCourseTitle",true));
    /// oError.Add("ERRORID",param);
    /// </summary>
    [DataContract]
    [Serializable]
    public class ErrorParams
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ErrorParams class.
        /// </summary>
        public ErrorParams()
        {
            this.Param = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the ErrorParams class.
        /// </summary>
        /// <param name="parameter">String which accepts the token id or plain text.</param>
        /// <param name="fromResource">Bool which states to pick from resource file or not.</param>
        public ErrorParams(string parameter, bool fromResource)
        {
            this.Param = parameter;
            this.FromResource = fromResource;
        } 
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the string value for parameter. It can be token id also for resource and turn the flag on of FromResource.
        /// </summary>
        [DataMember]
        public string Param
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether token id from resource file.
        /// </summary>
        [DataMember]
        public bool FromResource
        {
            get;
            set;
        } 
        #endregion
    }
}

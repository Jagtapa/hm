﻿// <copyright file="CreateExcelDoc.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>03-01-2011</date>
// <summary> This class is used to create the excel file using open xml doc sdk.
// </summary>

namespace HM.Framework
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    using System.Web;
    using DocumentFormat.OpenXml;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;

    /*
//
//  November 2013
//  http://www.mikesknowledgebase.com
//
//  Note: if you plan to use this in an ASP.Net application, remember to add a reference to "System.Web", and to uncomment
//  the "INCLUDE_WEB_FUNCTIONS" definition at the top of this file.
//
//  Release history
//   - Nov 2013:
//        Changed "CreateExcelDocument(DataTable dt, string xlsxFilePath)" to remove the DataTable from the DataSet after creating the Excel file.
//        You can now create an Excel file via a Stream (making it more ASP.Net friendly)
//   - Jan 2013: Fix: Couldn't open .xlsx files using OLEDB  (was missing "WorkbookStylesPart" part)
//   - Nov 2012:
//        List<>s with Nullable columns weren't be handled properly.
//        If a value in a numeric column doesn't have any data, don't write anything to the Excel file (previously, it'd write a '0')
//   - Jul 2012: Fix: Some worksheets weren't exporting their numeric data properly, causing "Excel found unreadable content in '___.xslx'" errors.
//   - Mar 2012: Fixed issue, where Microsoft.ACE.OLEDB.12.0 wasn't able to connect to the Excel files created using this class.
//
*/

    /// <summary>
    /// This class is used to create the excel file using open xml doc sdk.
    /// </summary>
    public class CreateExcelDoc
    {
        /// <summary>
        /// Create Excel Document
        /// </summary>
        /// <typeparam name="T">The type of entity</typeparam>
        /// <param name="list">the collection of records</param>
        /// <param name="xlsxFilePath">the file path</param>
        /// <returns>the result of operation.</returns>
        public static bool CreateExcelDocument<T>(List<T> list, string xlsxFilePath)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(ListToDataTable(list));
            return CreateExcelDocument(ds, xlsxFilePath);
        }

        /// <summary>
        /// The data table created from list
        /// </summary>
        /// <typeparam name="T">The generic type</typeparam>
        /// <param name="list">The list to translate.</param>
        /// <returns>The data table</returns>
        public static DataTable ListToDataTable<T>(List<T> list)
        {
            DataTable dataTable = new DataTable();

            foreach (PropertyInfo info in typeof(T).GetProperties())
            {
                dataTable.Columns.Add(new DataColumn(info.Name, GetNullableType(info.PropertyType)));
            }

            foreach (T t in list)
            {
                DataRow row = dataTable.NewRow();
                foreach (PropertyInfo info in typeof(T).GetProperties())
                {
                    if (!IsNullableType(info.PropertyType))
                    {
                        row[info.Name] = info.GetValue(t, null);
                    }
                    else
                    {
                        row[info.Name] = info.GetValue(t, null) ?? DBNull.Value;
                    }
                }

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }

        /// <summary>
        /// create the excel document.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <param name="xlsxFilePath">The file path</param>
        /// <returns>The result of operation.</returns>
        public static bool CreateExcelDocument(DataTable dataTable, string xlsxFilePath)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(dataTable);
            bool result = CreateExcelDocument(ds, xlsxFilePath);
            ds.Tables.Remove(dataTable);
            return result;
        }

        /// <summary>
        /// Create an Excel file, and write it out to a MemoryStream (rather than directly to a file)
        /// </summary>
        /// <param name="dt">DataTable containing the data to be written to the Excel.</param>
        /// <param name="filename">The filename (without a path) to call the new Excel file.</param>
        /// <param name="response">HttpResponse of the current page.</param>
        /// <returns>True if it was created succesfully, otherwise false.</returns>
        public static bool CreateExcelDocument(DataTable dt, string filename, HttpResponse response)
        {
            try
            {
                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                CreateExcelDocumentAsStream(ds, filename, response);
                ds.Tables.Remove(dt);
                return true;
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Failed, exception thrown: " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Create an Excel file, and write it out to a MemoryStream (rather than directly to a file)
        /// </summary>
        /// <param name="list">list containing the data to be written to the Excel.</param>
        /// <typeparam name="T">the generic type.</typeparam>
        /// <param name="filename">The filename (without a path) to call the new Excel file.</param>
        /// <param name="response">HttpResponse of the current page.</param>
        /// <returns>True if it was created succesfully, otherwise false.</returns>
        public static bool CreateExcelDocument<T>(List<T> list, string filename, HttpResponse response)
        {
            try
            {
                DataSet ds = new DataSet();
                ds.Tables.Add(ListToDataTable(list));
                CreateExcelDocumentAsStream(ds, filename, response);
                return true;
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Failed, exception thrown: " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Create an Excel file, and write it out to a MemoryStream (rather than directly to a file)
        /// </summary>
        /// <param name="dataSet">DataSet containing the data to be written to the Excel.</param>
        /// <param name="filename">The filename (without a path) to call the new Excel file.</param>
        /// <param name="response">HttpResponse of the current page.</param>
        /// <returns>Either a MemoryStream, or NULL if something goes wrong.</returns>
        public static bool CreateExcelDocumentAsStream(DataSet dataSet, string filename, HttpResponse response)
        {
            try
            {
                MemoryStream stream = new MemoryStream();
                using (SpreadsheetDocument document = SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook, true))
                {
                    WriteExcelFile(dataSet, document);
                }

                stream.Flush();
                stream.Position = 0;

                /*response.Clear();
                response.ClearContent();
                response.Buffer = true;
                response.Charset = string.Empty;*/

                /*//  NOTE: If you get an "HttpCacheability does not exist" error on the following line, make sure you have
                //  manually added System.Web to this project's References.*/

                //// response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                //// response.AddHeader("content-disposition", "attachment; filename=" + filename);
                //// response.ContentType = "application/force-download";
                response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //// response.ContentType = "application/vnd.ms-excel";
                response.AddHeader("content-disposition", "filename=" + filename);

                byte[] data1 = new byte[stream.Length];
                stream.Read(data1, 0, data1.Length);
                stream.Close();
                response.BinaryWrite(data1);

                /*response.Flush();
                  response.End();
                */

                return true;
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Failed, exception thrown: " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Create an Excel file, and write it to a file.
        /// </summary>
        /// <param name="dataSet">DataSet containing the data to be written to the Excel.</param>
        /// <param name="excelFilename">Name of file to be written.</param>
        /// <returns>True if successful, false if something went wrong.</returns>
        public static bool CreateExcelDocument(DataSet dataSet, string excelFilename)
        {
            try
            {
                using (SpreadsheetDocument document = SpreadsheetDocument.Create(excelFilename, SpreadsheetDocumentType.Workbook))
                {
                    WriteExcelFile(dataSet, document);
                }

                Trace.WriteLine("Successfully created: " + excelFilename);
                return true;
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Failed, exception thrown: " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Write the dataset to excel file.
        /// </summary>
        /// <param name="dataSet">The data set</param>
        /// <param name="spreadsheet">The spread sheet documemnt.</param>
        private static void WriteExcelFile(DataSet dataSet, SpreadsheetDocument spreadsheet)
        {
            ////  Create the Excel file contents.  This function is used when creating an Excel file either writing 
            ////  to a file, or writing to a MemoryStream.
            spreadsheet.AddWorkbookPart();
            spreadsheet.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

            ////  My thanks to James Miera for the following line of code (which prevents crashes in Excel 2010)
            spreadsheet.WorkbookPart.Workbook.Append(new BookViews(new WorkbookView()));

            ////  If we don't add a "WorkbookStylesPart", OLEDB will refuse to connect to this .xlsx file !
            WorkbookStylesPart workbookStylesPart = spreadsheet.WorkbookPart.AddNewPart<WorkbookStylesPart>("rIdStyles");
            Stylesheet stylesheet = new Stylesheet();
            workbookStylesPart.Stylesheet = stylesheet;

            ////  Loop through each of the DataTables in our DataSet, and create a new Excel Worksheet for each.
            uint worksheetNumber = 1;
            foreach (DataTable dt in dataSet.Tables)
            {
                ////  For each worksheet you want to create
                string workSheetID = "rId" + worksheetNumber.ToString();
                string worksheetName = dt.TableName;

                WorksheetPart newWorksheetPart = spreadsheet.WorkbookPart.AddNewPart<WorksheetPart>();
                newWorksheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet();

                //// create sheet data
                newWorksheetPart.Worksheet.AppendChild(new DocumentFormat.OpenXml.Spreadsheet.SheetData());

                //// save worksheet
                WriteDataTableToExcelWorksheet(dt, newWorksheetPart);
                newWorksheetPart.Worksheet.Save();

                //// create the worksheet to workbook relation
                if (worksheetNumber == 1)
                {
                    spreadsheet.WorkbookPart.Workbook.AppendChild(new DocumentFormat.OpenXml.Spreadsheet.Sheets());
                }

                spreadsheet.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>().AppendChild(new DocumentFormat.OpenXml.Spreadsheet.Sheet()
                {
                    Id = spreadsheet.WorkbookPart.GetIdOfPart(newWorksheetPart),
                    SheetId = (uint)worksheetNumber,
                    Name = dt.TableName
                });

                worksheetNumber++;
            }

            spreadsheet.WorkbookPart.Workbook.Save();
        }

        /// <summary>
        /// Write Data Table To Excel Worksheet
        /// </summary>
        /// <param name="dataTable">the data table</param>
        /// <param name="worksheetPart">the worksheetPart</param>
        private static void WriteDataTableToExcelWorksheet(DataTable dataTable, WorksheetPart worksheetPart)
        {
            var worksheet = worksheetPart.Worksheet;
            var sheetData = worksheet.GetFirstChild<SheetData>();

            string cellValue = string.Empty;

            /*//  Create a Header Row in our Excel file, containing one header for each Column of data in our DataTable.
            //
            //  We'll also create an array, showing which type each column of data is (Text or Numeric), so when we come to write the actual
            //  cells of data, we'll know if to write Text values or Numeric cell values.*/
            int numberOfColumns = dataTable.Columns.Count;
            bool[] isNumericColumn = new bool[numberOfColumns];

            string[] excelColumnNames = new string[numberOfColumns];
            for (int n = 0; n < numberOfColumns; n++)
            {
                excelColumnNames[n] = GetExcelColumnName(n);
            }

            ////  Create the Header row in our Excel Worksheet

            uint rowIndex = 1;
            var headerRow = new Row { RowIndex = rowIndex };
            //// add a row at the top of spreadsheet

            sheetData.Append(headerRow);

            for (int colInx = 0; colInx < numberOfColumns; colInx++)
            {
                DataColumn col = dataTable.Columns[colInx];
                AppendTextCell(excelColumnNames[colInx] + "1", col.ColumnName, headerRow);
                isNumericColumn[colInx] = (col.DataType.FullName == "System.Decimal") || (col.DataType.FullName == "System.Int32");
            }

            ////  Now, step through each row of data in our DataTable...

            double cellNumericValue = 0;
            foreach (DataRow dr in dataTable.Rows)
            {
                //// ...create a new row, and append a set of this row's data to it.
                ++rowIndex;
                var newExcelRow = new Row { RowIndex = rowIndex };  // add a row at the top of spreadsheet
                sheetData.Append(newExcelRow);

                for (int colInx = 0; colInx < numberOfColumns; colInx++)
                {
                    cellValue = dr.ItemArray[colInx].ToString();

                    //// Create cell with data
                    if (isNumericColumn[colInx])
                    {
                        ////  For numeric cells, make sure our input data IS a number, then write it out to the Excel file.
                        ////  If this numeric value is NULL, then don't write anything to the Excel file.
                        cellNumericValue = 0;
                        if (double.TryParse(cellValue, out cellNumericValue))
                        {
                            cellValue = cellNumericValue.ToString();
                            AppendNumericCell(excelColumnNames[colInx] + rowIndex.ToString(), cellValue, newExcelRow);
                        }
                    }
                    else
                    {
                        ////  For text cells, just write the input data straight out to the Excel file.
                        AppendTextCell(excelColumnNames[colInx] + rowIndex.ToString(), cellValue, newExcelRow);
                    }
                }
            }
        }

        /// <summary>
        /// Append Text Cell
        /// </summary>
        /// <param name="cellReference">the cellReference</param>
        /// <param name="cellStringValue">the cellStringValue</param>
        /// <param name="excelRow">the excelRow</param>
        private static void AppendTextCell(string cellReference, string cellStringValue, Row excelRow)
        {
            ////  Add a new Excel Cell to our Row 
            Cell cell = new Cell() { CellReference = cellReference, DataType = CellValues.String };
            CellValue cellValue = new CellValue();
            cellValue.Text = cellStringValue;
            cell.Append(cellValue);
            excelRow.Append(cell);
        }

        /// <summary>
        /// Append Numeric Cell
        /// </summary>
        /// <param name="cellReference">the cellReference</param>
        /// <param name="cellStringValue">the cellStringValue</param>
        /// <param name="excelRow">the excelRow</param>
        private static void AppendNumericCell(string cellReference, string cellStringValue, Row excelRow)
        {
            ////  Add a new Excel Cell to our Row 
            Cell cell = new Cell() { CellReference = cellReference };
            CellValue cellValue = new CellValue();
            cellValue.Text = cellStringValue;
            cell.Append(cellValue);
            excelRow.Append(cell);
        }

        /// <summary>
        /// Get Excel Column Name
        /// </summary>
        /// <param name="columnIndex">the column Index</param>
        /// <returns>the Excel Column Name</returns>
        private static string GetExcelColumnName(int columnIndex)
        {
            /*//  Convert a zero-based column index into an Excel column reference  (A, B, C.. Y, Y, AA, AB, AC... AY, AZ, B1, B2..)
            //
            //  eg  GetExcelColumnName(0) should return "A"
            //      GetExcelColumnName(1) should return "B"
            //      GetExcelColumnName(25) should return "Z"
            //      GetExcelColumnName(26) should return "AA"
            //      GetExcelColumnName(27) should return "AB"
            //      ..etc..
            //*/
            if (columnIndex < 26)
            {
                return ((char)('A' + columnIndex)).ToString();
            }

            char firstChar = (char)('A' + (columnIndex / 26) - 1);
            char secondChar = (char)('A' + (columnIndex % 26));
            return string.Format("{0}{1}", firstChar, secondChar);
        }

        /// <summary>
        /// Check whether the type is nullable.
        /// </summary>
        /// <param name="type">The type to check</param>
        /// <returns>the result of check</returns>
        private static bool IsNullableType(Type type)
        {
            return type == typeof(string) || type.IsArray || (type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>)));
        }

        /// <summary>
        /// Gets the nullable type
        /// </summary>
        /// <param name="t">The type to get.</param>
        /// <returns>the nullable type</returns>
        private static Type GetNullableType(Type t)
        {
            Type returnType = t;
            if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                returnType = Nullable.GetUnderlyingType(t);
            }

            return returnType;
        }
    }
}
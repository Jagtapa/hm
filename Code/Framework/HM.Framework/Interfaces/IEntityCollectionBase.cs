// <copyright file="IEntityCollectionBase.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This is an interface used to handle the collections.
// </summary>
namespace HM.Framework
{
    using System.Collections;
    using System.ComponentModel;

    /// <summary>
    /// IEntityCollectionBase class.
    /// </summary>
    public interface IEntityCollectionBase : ICollection, IEnumerable, IList, IBindingList
    {
        #region Methods
        /// <summary>
        /// Add entity in list.
        /// </summary>
        /// <param name="entityBase">IEntityBase entityBase</param>
        /// <returns>success failure</returns>
        int Add(IEntityBase entityBase);

        /// <summary>
        /// Converts to xml.
        /// </summary>
        /// <returns>The XML string.</returns>
        string ConvertToXML();

        /// <summary>
        /// Get all the matching records.
        /// </summary>
        /// <param name="searchColumn">The search column name.</param>
        /// <param name="searchValue">The search column value.</param>
        /// <returns>filtered collection.</returns>
        IEntityCollectionBase FetchAll(string searchColumn, string searchValue);

        /// <summary>
        /// Get all the matching records.
        /// </summary>
        /// <param name="searchColumn">The search column name.</param>
        /// <param name="searchValue">The search column object.</param>
        /// <returns>filtered collection.</returns>
        IEntityCollectionBase FetchAll(string searchColumn, object searchValue);

        /// <summary>
        /// Get all the matching records.
        /// </summary>
        /// <param name="searchColumn">The search column name.</param>
        /// <param name="searchValues">The search column values.</param>
        /// <returns>filtered collection.</returns>
        IEntityCollectionBase FetchAll(string searchColumn, object[] searchValues);

        /// <summary>
        /// Get all the matching records using bitwise operator.
        /// </summary>
        /// <param name="searchColumn">The search column name.</param>
        /// <param name="searchValue">The search value.</param>
        /// <returns>filtered collection.</returns>
        IEntityCollectionBase FetchAllBitwise(string searchColumn, int searchValue);

        /// <summary>
        /// Get all the matching records between range.
        /// </summary>
        /// <param name="searchColumn">The search column name.</param>
        /// <param name="fromSearchValue">The from search value.</param>
        /// <param name="toSearchValue">The to search value.</param>
        /// <returns>filtered collection.</returns>
        IEntityCollectionBase FetchAllBetween(string searchColumn, object fromSearchValue, object toSearchValue);

        /// <summary>
        /// Update the collection.
        /// </summary>
        /// <param name="searchColumn">string searchColumn</param>
        /// <param name="searchValue">object searchValue</param>
        /// <param name="refreshColumn">string refreshColumn</param>
        /// <param name="refreshValue">object refreshValue</param>
        void Update(string searchColumn, object searchValue, string refreshColumn, object refreshValue);

        /// <summary>
        /// Update the collection.
        /// </summary>
        /// <param name="searchColumn">string searchColumn</param>
        /// <param name="searchValue">object searchValue</param>
        /// <param name="newEntity">IEntityBase newEntity</param>
        void Update(string searchColumn, object searchValue, IEntityBase newEntity);

        /// <summary>
        /// Serach for record in list.
        /// </summary>
        /// <param name="searchColumn">The search column name.</param>
        /// <param name="searchValue">The search object.</param>
        /// <returns>The entity.</returns>
        IEntityBase Fetch(string searchColumn, object searchValue);

        /// <summary>
        /// Gets the index of record from list.
        /// </summary>
        /// <param name="searchColumn">The search column name.</param>
        /// <param name="searchValue">The search column value.</param>
        /// <returns>An index of entity.</returns>
        int GetIndex(string searchColumn, string searchValue);

        /// <summary>
        /// Gets the index of record from list.
        /// </summary>
        /// <param name="searchColumn">The search column name.</param>
        /// <param name="searchValue">The search column object.</param>
        /// <returns>An index of entity.</returns>
        int GetIndex(string searchColumn, object searchValue);

        /// <summary>
        /// Sorts the collection.
        /// </summary>
        /// <param name="sortDirection">The sort direction.</param>
        /// <param name="sortColumn">the column to sort.</param>
        void Sort(SortOrder sortDirection, string sortColumn);

        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <returns>The string object.</returns>
        string ToString();

        /// <summary>
        /// Converts to xml string.
        /// </summary>
        /// <returns>The xml string object.</returns>
        string ToXML();

        /// <summary>
        /// Creates the dummey object.
        /// </summary>
        /// <returns>Dummey object of collection.</returns>
        IEntityCollectionBase Clone();

        /// <summary>
        /// Creates the dummey object.
        /// </summary>
        /// <param name="isDeepCopy">is deep copy or shallow copy indicator.</param>
        /// <returns>The dummy object.</returns>
        IEntityCollectionBase Clone(bool isDeepCopy);

        /// <summary>
        /// Gets the concatenated list of columns and values of an entity.
        /// </summary>
        /// <param name="seperator">The seperator.</param>
        /// <param name="columnName">The name of column.</param>
        /// <returns>The concatenated list.</returns>
        string GetConcatenatedList(char seperator, string columnName); 
        #endregion
    }
}

// <copyright file="IController.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This is an interface have to implement in controller classes.
// </summary>
namespace HM.Framework
{
    /// <summary>
    /// Interface for Controllers
    /// </summary>
    public interface IController
    {
        #region Methods
        /// <summary>
        /// Method to delete records
        /// </summary>
        /// <param name="deleteCollection">Records to be deleted</param>
        /// <returns>A Result object</returns>
        Result Delete(IEntityCollectionBase deleteCollection);

        /// <summary>
        /// Method to Save an entity
        /// </summary>
        /// <param name="entityBase">Entity to be saved</param>
        /// <returns>The saved entity in a Result object</returns>
        Result Save(IEntityBase entityBase);

        /// <summary>
        /// Method to search for records
        /// </summary>
        /// <param name="entityBase">The values to be searched for</param>
        /// <returns>The found records in a Result object</returns>
        Result Search(IEntityBase entityBase);

        /// <summary>
        /// Method to Get an entity based on an ID.
        /// </summary>
        /// <param name="key">The ID of the entity to be retrieved</param>
        /// <returns>The found entity in a Result object</returns>
        Result Get(int key);

        /// <summary>
        /// Method to Get an entity based on some provided values.
        /// </summary>
        /// <param name="entityBase">The ID of the entity to be retrieved</param>
        /// <returns>The found entity in a Result object</returns>
        Result Get(IEntityBase entityBase);

        /// <summary>
        /// Method to Get a list of entities.
        /// </summary>
        /// <returns>The entity collection in a Result object</returns>
        Result GetList(); 
        #endregion
    }
}

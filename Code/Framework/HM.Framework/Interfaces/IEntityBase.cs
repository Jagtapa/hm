// <copyright file="IEntityBase.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This is an interface which is base interface to all the entities.
// </summary>
namespace HM.Framework
{
    using System;

    /// <summary>
    /// This interface is the base interface for all specific Entity
    /// Interfaces and classes.
    /// Entity classes are classes that define the main object types in the
    /// application viz. Member, Element, etc. This is the base interface for all
    /// Entities.
    /// </summary>
    public interface IEntityBase
    {
        #region Properties
        /// <summary>
        /// Gets or sets the error object.
        /// If there are any errors with in the entity, use this property.
        /// </summary>
        Error Error
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether is valid set it to true.
        /// </summary>
        bool IsValid
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets an unique identifier for an entity. It�s just like the database ID.
        /// </summary>
        int Key
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the created by user id.
        /// </summary>
        int CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the created on date and time.
        /// </summary>
        DateTime? CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the updated by user id.
        /// </summary>
        int UpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets the updated on date and time.
        /// </summary>
        DateTime? UpdatedOn { get; set; }

        /// <summary>
        /// Gets or sets the deleted status. 0-Not deleted 1-deleted
        /// </summary>
        int Deleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Converts the entity into XML format.
        /// </summary>
        /// <returns>The entity as an XML string</returns>
        string ToXML();
        #endregion
    }
}

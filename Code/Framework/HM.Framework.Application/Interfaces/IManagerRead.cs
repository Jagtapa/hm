// <copyright file="IManagerRead.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This interface is the base interface for all specific Entity Manager Read Interfaces and classes.
// </summary>

namespace HM.Framework.Application
{
    using System;
    using HM.Framework;

    /// <summary>
    /// This interface is the base interface for all specific Entity Manager Read
    /// Interfaces and classes.
    /// </summary>
    /// <remarks>
    /// This is the base interface for all Entity Managers. Entity Managers Read are classes
    /// that manage Entity (see <see cref="IEntityBase"/>) objects. Their common functions
    /// include GetList, GetDetails, Search etc. These managers communicate
    /// with the Data Stores (see <see cref="IDataStore"/>) to fetch/put data in
    /// the database.
    /// </remarks>
    public interface IManagerRead
    {
        #region Methods
        /// <summary>
        /// This function retrives collection list information 
        /// It calls the respective Data Store's GetList (see <see cref="IDataStore.GetList()"/>) function to fetch data from the
        /// database. Information specific to only List Pages are returned here rather
        /// than the entire Entity objects being returned.
        /// <seealso cref="GetList()"/>
        /// </summary>
        /// <returns>A DataSet (see <see cref="System.Data.DataSet"/>) object containig
        /// the required information (multiple records) sent by the Data Store (see <see cref="IDataStore"/>).</returns>
        Result GetList();

        /// <summary>
        /// This function fetches information about an Entity (see <see cref="IEntityBase"/>)
        /// based on the Entity ID that uniquely identifies it. It calls the respective Data Store
        /// GetDetails (see <see cref="IDataStore.GetDetails"/>) function to fetch data from the
        /// database.
        /// </summary>
        /// <param name="entityBase">The Object ID that uniquely identifies the required Entity (see <see cref="IEntityBase"/>).</param>
        /// <returns>The populated Entity (see <see cref="Result"/>) object.</returns>
        Result GetDetails(IEntityBase entityBase);

        /// <summary>
        /// This function retrives collection list information based on Search Criteria
        /// based on the Entity ID that uniquely identifies it. It calls the respective Data Store
        /// GetDetails (see <see cref="IDataStore.Search"/>) function to fetch data from the
        /// database.
        /// </summary>
        /// <param name="entityBase">The Object ID that uniquely identifies the required Entity (see <see cref="CSearchCriteria"/>).</param>
        /// <returns>The populated Entity (see <see cref="Result"/>) object.</returns>
        Result Search(IEntityBase entityBase); 
        #endregion
    }
}

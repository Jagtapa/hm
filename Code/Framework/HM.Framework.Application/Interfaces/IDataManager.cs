// <copyright file="IDataManager.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This interface is the base interface for all specific Data Store Interfaces and classes.
// </summary>

namespace HM.Framework.Application
{
    using HM.Framework;

    /// <summary>
    /// This interface is the base interface for all specific Data Store
    /// Interfaces and classes.
    /// </summary>
    /// <remarks>
    /// This is the base interface for all Data Store classes. Data Stores
    /// are classes that manage all database activities on Entities (see <see cref="IEntityBase"/>).
    /// All SQLs are embeded in their respective functions to carry
    /// out the desired tasks.
    /// </remarks>
    public interface IDataManager
    {
        #region Methods
        /// <summary>
        /// This function executes the SQL to add a record to the database.
        /// </summary>
        /// <param name="entityBase">The Entity object that needs to be saved to the database.</param>
        /// <returns> An integer representing success or failure.</returns>
        int Add(IEntityBase entityBase);

        /// <summary>
        /// This function executes SQLs that are required to fetch information
        /// about an Entity (see <see cref="IEntityBase"/>) based on a Record ID.
        /// </summary>
        /// <param name="entityBase">The Record ID for which the information is to be fetched.</param>
        /// <returns> The entity details.</returns>
        IEntityBase GetDetails(IEntityBase entityBase);

        /// <summary>
        /// This function executes SQLs that are required to fetch a list of
        /// records.
        /// Information specific to only List Pages are returned here rather than the
        /// entire information about an Entity being returned.
        /// </summary>
        /// <returns> object containig the required information (multiple records).</returns>
        IEntityCollectionBase GetList();

        /// <summary>
        /// This function executes the SQL to delete a record from the database based
        /// on the Record ID.
        /// </summary>
        /// <param name="entityBase">The Record ID for the record to be deleted.</param>
        /// <returns> An integer representing success or failure.</returns>
        int Remove(IEntityBase entityBase);

        /// <summary>
        /// This function executes the SQL to delete records from the database based
        /// on the Record IDs.
        /// </summary>
        /// <param name="entityCollection">The Record List for the records to be deleted.</param>
        /// <returns> An integer representing success or failure.</returns>
        int RemoveList(IEntityCollectionBase entityCollection);

        /// <summary>
        /// This function executes the SQL to update a record in the database.
        /// </summary>
        /// <param name="entityBase">The Entity object that needs to be updated.</param>
        /// <returns> An integer representing success or failure.</returns>
        int Update(IEntityBase entityBase);

        /// <summary>
        /// This function executes the SQL to search records in the database based on the search criteria
        /// </summary>
        /// <param name="entityBase">The Entity object that needs to be updated.</param>
        /// <returns> object containig the required information (multiple records).</returns>
        IEntityCollectionBase Search(IEntityBase entityBase);
        #endregion
    }
}

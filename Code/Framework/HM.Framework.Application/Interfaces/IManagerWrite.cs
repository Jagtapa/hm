// <copyright file="IManagerWrite.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This interface is the base interface for all specific Entity Manager Write Interfaces and classes.
// </summary>

namespace HM.Framework.Application
{
    using System;
    using HM.Framework;

    /// <summary>
    /// This interface is the base interface for all specific Entity Manager Write
    /// Interfaces and classes.
    /// </summary>
    /// <remarks>
    /// This is the base interface for all Entity Managers Write. Entity Managers Write are classes
    /// that manage Entity (see <see cref="IEntityBase"/>) objects. Their common functions
    /// include Instantiation, Delete, Update, Add, etc. These managers communicate
    /// with the Data Stores (see <see cref="IDataStore"/>) to fetch/put data in
    /// the database.
    /// </remarks>
    public interface IManagerWrite
    {
        /// <summary>
        /// This function adds an Entity. It calls the respective Add (see <see cref="IDataStore.Add"/>)
        /// function on the Data Store (see <see cref="IDataStore"/>) class to add a record to the database. Before
        /// calling operations on the Data Store the function calls the Validate (see <see cref="Validate"/>)
        /// function. This is to ensure that the data in the Entity (see <see cref="IEntityBase"/>) object
        /// is valid. Incase of invalid data the Error (see <see cref="CError"/>) object
        /// is populated with the respective errors and returned.
        /// </summary>
        /// <param name="entityBase">The Entity (see <see cref="IEntityBase"/>) object
        /// that needs to be added.</param>
        /// <returns>The Result (see <see cref="Result"/>) object.</returns>
        Result Add(IEntityBase entityBase);

        /// <summary>
        /// This function deletes an entry based on the Object ID that uniquely
        /// identifies an Entity (see <see cref="IEntityBase"/>). It calls the respective Remove (see <see cref="IDataStore.Remove"/>)
        /// function on the Data Store (see <see cref="IDataStore"/>) class to delete a record from the database. Any
        /// invalid deletions are validated here before making calls to the Data Store.
        /// Any errorneous delete is returned as an Error (see <see cref="CError"/>) object
        /// having the respective error messages.
        /// </summary>
        /// <param name="entityBase">The Entry ID for the entry to be deleted.</param>
        /// <returns>The Result (see <see cref="Result"/>) object.</returns>
        Result Remove(IEntityBase entityBase);

        /// <summary>
        /// This function updates an Entity. It calls the respective Update (see <see cref="IDataStore.Update"/>)
        /// function on the Data Store (see <see cref="IDataStore"/>) class to update a record in the database. Before
        /// calling operations on the Data Store the function calls the Validate (see <see cref="Validate"/>)
        /// function. This is to ensure that the data in the Entity (see <see cref="IEntityBase"/>) object
        /// is valid. Incase of invalid data the Error (see <see cref="CError"/>) object
        /// is populated with the respective errors and returned.
        /// </summary>
        /// <param name="entityBase">The Entity (see <see cref="IEntityBase"/>) object
        /// that needs to be updated.</param>
        /// <returns>The Result (see <see cref="Result"/>) object.</returns>
        Result Update(IEntityBase entityBase);

        /// <summary>
        /// This function validates an Entity for its completeness and validity.
        /// The function first validates data within the Entity (see <see cref="IEntityBase"/>) object by calling the
        /// Validate (see <see cref="IEntityBase.Validate"/> function on the Entity. Then,
        /// it validates the Entity w.r.t. to other business rules that apply for it.
        /// </summary>
        /// <param name="entityBase">The Entity (see <see cref="IEntityBase"/>) object
        /// that needs to be validated.</param>
        /// <returns>The Result (see <see cref="Result"/>) object.</returns>
        Result Validate(IEntityBase entityBase);

        /// <summary>
        /// This function remoeves list of Entity. It calls the respective RemoveList (see <see cref="IDataStore.RemoveList"/>)
        /// function on the Data Store (see <see cref="IDataStore"/>) class to delete records from the database. Before
        /// calling operations on the Data Store the function calls the Validate (see <see cref="Validate"/>)
        /// function. This is to ensure that the data in the Entity (see <see cref="IEntityBase"/>) object
        /// is valid. Incase of invalid data the Error (see <see cref="CError"/>) object
        /// is populated with the respective errors and returned.
        /// </summary>
        /// <param name="entityCollection">The Entity Collection (see <see cref="EntityCollectionBase"/>) objects
        /// that needs to be deleted.</param>
        /// <returns>The Result (see <see cref="Result"/>) object.</returns>
        Result RemoveList(IEntityCollectionBase entityCollection);
    }
}

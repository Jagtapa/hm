// <copyright file="CEmailEventArgs.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class is used to pass the event arguments for Send Email event handlers.
// </summary>

namespace HM.Framework.Application
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// This class is used to hold methods and properties email event arguments.
    /// </summary>
    [DataContract]
    [Serializable]
    public class CEmailEventArgs : EventArgs
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the CEmailEventArgs class.
        /// </summary>
        public CEmailEventArgs()
        {
            this.EmailEventType = EmailTypes.None;
        } 
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Id of the entity for which email is to be sent.
        /// </summary>
        [DataMember]
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Type of email to be sent.
        /// </summary>
        [DataMember]
        public EmailTypes EmailEventType
        {
            get;
            set;
        }
        #endregion
    }
}

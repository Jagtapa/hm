// <copyright file="CommonDataAccess.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright � 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>26-06-2013</date>
// <summary>This class is used to perform the database operations.
// </summary>

namespace HM.Framework.Application
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Xml;
    using Microsoft.Practices.EnterpriseLibrary.Data;
    //// using EntLibContrib.Data.MySql;
    using HM.Framework;

    /// <summary>
    /// The common data access class for database operations
    /// </summary>
    public class CommonDataAccess
    {
        #region Private Variables
        /// <summary>
        /// The database object.
        /// </summary>
        [ThreadStatic]
        private static Database database;

        /// <summary>
        /// DbCommand object to wrapp the sql commands.
        /// </summary>
        [ThreadStatic]
        private static DbCommand commandWrapper;

        /// <summary>
        /// The name of the connection string.
        /// </summary>
        [ThreadStatic]
        private static string connectionStringName;

        /// <summary>
        /// The connection string.
        /// </summary>
        [ThreadStatic]
        private static string connectionString;
        #endregion

        /// <summary>
        /// Prevents a default instance of the CommonDataAccess class from being created.
        /// </summary>
        private CommonDataAccess()
        {
        }

        /// <summary>
        /// Gets the ReaderWriterLock Timeout Value.
        /// </summary>
        public static int LockTimeOutValue
        {
            get
            {
                // initialize with default value.
                int timeout = 0;
                if (null != ConfigManager.ReadAppSettings("READER_LOCK_TIMEOUT_VALUE"))
                {
                    // check if config value is a valid integer
                    if (!int.TryParse(ConfigManager.ReadAppSettings("READER_LOCK_TIMEOUT_VALUE"), out timeout))
                    {
                        // if not a valid integer, set the default value.
                        timeout = 0;
                    }
                }

                return timeout;
            }
        }

        /// <summary>
        /// Sets the database connection string name.
        /// </summary>
        /// <param name="connectionStringNameValue">The connection string name.</param>
        public static void SetConnectionStringName(string connectionStringNameValue)
        {
            connectionStringName = connectionStringNameValue;
        }

        /// <summary>
        /// Sets the database connection string value.
        /// </summary>
        /// <param name="connectionStringValue">The connection string value.</param>
        public static void SetConnectionString(string connectionStringValue)
        {
            connectionString = connectionStringValue;
        }

        /// <summary>
        /// Creates sql command with stored procedure name and command timeout.
        /// </summary>
        /// <param name="storedProcedureName">The name of stored procedure.</param>
        /// <param name="commandTimeout">The integer value for command timeout.</param>
        public static void CreateStoredProcCommandWrapper(string storedProcedureName, int commandTimeout)
        {
            CreateStoredProcCommandWrapper(storedProcedureName);
            commandWrapper.CommandTimeout = commandTimeout;
        }

        /// <summary>
        /// Creates sql command with stored procedure name.
        /// </summary>
        /// <param name="storedProcedureName">The name of stored procedure.</param>
        public static void CreateStoredProcCommandWrapper(string storedProcedureName)
        {
            if (null == database)
            {
                try
                {
                    if (string.IsNullOrEmpty(connectionStringName))
                    {
                        database = DatabaseFactory.CreateDatabase();
                    }
                    else
                    {
                        database = DatabaseFactory.CreateDatabase(connectionStringName);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            commandWrapper = database.GetStoredProcCommand(storedProcedureName);
            commandWrapper.Parameters.Clear();

            // Since many long-running queries were giving a timeout, this was added. 
            int commandTimeout = Convert.ToInt32(ConfigManager.ReadAppSettings(Global.CONSTSTRCOMMANDTIMEOUT));
            commandWrapper.CommandTimeout = (commandTimeout == 0) ? 120 : commandTimeout;
        }

        /// <summary>
        /// Creates a command wrapper for the given query.
        /// </summary>
        /// <param name="query">The sql query string.</param>
        public static void CreateSqlStringCommandWrapper(string query)
        {
            if (null == database)
            {
                try
                {
                    if (string.IsNullOrEmpty(connectionStringName))
                    {
                        database = DatabaseFactory.CreateDatabase();
                    }
                    else
                    {
                        database = DatabaseFactory.CreateDatabase(connectionStringName);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            commandWrapper = database.GetSqlStringCommand(query);

            // Since many long-running queries were giving a timeout, this was added. 
            int commandTimeout = Convert.ToInt32(ConfigManager.ReadAppSettings(Global.CONSTSTRCOMMANDTIMEOUT));
            commandWrapper.CommandTimeout = (commandTimeout == 0) ? 120 : commandTimeout;
        }

        /// <summary>
        /// Creates a command wrapper for the given sql query.
        /// </summary>
        /// <param name="query">The sql query string.</param>
        /// <param name="commandTimeout">Command Timeout in seconds.</param>
        public static void CreateSqlStringCommandWrapper(string query, int commandTimeout)
        {
            CreateSqlStringCommandWrapper(query);
            commandWrapper.CommandTimeout = commandTimeout;
        }

        /// <summary>
        /// Adds parameters to the command wrapper
        /// </summary>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterType">Type of the parameter.</param>
        /// <param name="direction">Direction of the parameter.</param>
        /// <param name="sourceColumn">Column name of the parameter in the table.</param>
        /// <param name="sourceRowVersion">Version of the row in the source data.</param>
        /// <param name="objectValue">Value of the parameter.</param>
        public static void AddParameter(
            string parameterName,
            DbType parameterType,
            ParameterDirection direction,
            string sourceColumn,
            DataRowVersion sourceRowVersion,
            object objectValue)
        {
            database.AddParameter(
                commandWrapper,
                parameterName,
                parameterType,
                direction,
                sourceColumn,
                sourceRowVersion,
                objectValue);
        }

        /// <summary>
        /// Adds an "IN" parameter to the command wrapper
        /// </summary>
        /// <param name="name">Name of the parameter</param>
        /// <param name="parameterType">Type of the parameter</param>
        /// <param name="objectValue">Value of the paramater</param>
        public static void AddInParameter(string name, DbType parameterType, object objectValue)
        {
            database.AddParameter(
                commandWrapper,
                name,
                parameterType,
                ParameterDirection.Input,
                string.Empty,
                DataRowVersion.Default,
                objectValue);
        }

        /// <summary>
        /// Adds an "OUT" parameter to the command wrapper
        /// </summary>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterType">Type of the parameter</param>
        /// <param name="size">Size of the parameter.</param>
        /// <param name="isNullable">Is nullable parametere property.</param>
        /// <param name="bytPrecision">The precision byte.</param>
        /// <param name="bytScale">The scale byte.</param>
        public static void AddOutParameter(string parameterName, DbType parameterType, int size, bool isNullable, byte bytPrecision, byte bytScale)
        {
            database.AddParameter(
                commandWrapper,
                parameterName,
                parameterType,
                size,
                ParameterDirection.Output,
                isNullable,
                bytPrecision,
                bytScale,
                string.Empty,
                DataRowVersion.Default,
                null);
        }

        /// <summary>
        /// Executes the command on the database
        /// </summary>
        /// <returns>The results as a DataReader object</returns>
        public static IDataReader ExecuteReader()
        {
            return database.ExecuteReader(commandWrapper);
        }

        /// <summary>
        /// Executes the command on the database
        /// </summary>
        /// <returns>An integer value indicates success or failure.</returns>
        public static int ExceuteNonQuery()
        {
            return database.ExecuteNonQuery(commandWrapper);
        }

        /// <summary>
        /// Executes the command on the database and returns a dataset
        /// </summary>
        /// <returns>The dataset object.</returns>
        public static object ExecuteDataSet()
        {
            return database.ExecuteDataSet(commandWrapper);
        }

        /// <summary>
        /// Executes the command on the database as a scalar
        /// </summary>
        /// <returns>The first column of the first row of the result</returns>
        public static object ExceuteScalar()
        {
            return database.ExecuteScalar(commandWrapper);
        }

        /// <summary>
        /// Executes the query and returns XML
        /// </summary>
        /// <returns>The result as an XmlReader object</returns>
        public static System.Xml.XmlReader ExecuteXmlReader()
        {
            /*SqlDatabase sqlDatabase = database as SqlDatabase;
            return sqlDatabase.ExecuteXmlReader(commandWrapper);*/
            return null;
        }

        /// <summary>
        /// Gets the value of a parameter
        /// </summary>
        /// <param name="strParameterName">Name of the paramter</param>
        /// <returns>Value of the parameter</returns>
        public static object GetParameterValue(string strParameterName)
        {
            return database.GetParameterValue(commandWrapper, strParameterName);
        }

        /// <summary>
        /// Creates XML document object.
        /// </summary>
        /// <returns>The instance of XML document.</returns>
        public static XmlDocument CreateXmlDocumentDataCollectionWrapper()
        {
            XmlDocument xmlDocument;
            try
            {
                xmlDocument = new XmlDocument();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return xmlDocument;
        }

        /// <summary>
        /// Creates XML child element.
        /// </summary>
        /// <param name="xmlDocument">The xml document.</param>
        /// <returns>The xml element.</returns>
        public static XmlElement CreateXmlDataElementWrapper(XmlDocument xmlDocument)
        {
            XmlElement xmlElementObject;
            try
            {
                xmlElementObject = xmlDocument.CreateElement("DATA");
                xmlDocument.AppendChild(xmlElementObject);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return xmlElementObject;
        }

        /// <summary>
        /// Creates XMl element for given xml root node.
        /// </summary>
        /// <param name="xmlDocument">The XML Document.</param>
        /// <param name="rootNode">The root node of xml document.</param>
        /// <param name="elementName">The name of xml element to be created inside root node.</param>
        /// <returns>The xml element.</returns>
        public static XmlElement CreateXmlElementDataRowWrapper(XmlDocument xmlDocument, XmlElement rootNode, string elementName)
        {
            XmlElement xmlElement;
            try
            {
                xmlElement = xmlDocument.CreateElement(elementName);
                rootNode.AppendChild(xmlElement);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return xmlElement;
        }
    }
}

﻿// <copyright file="ExcelDataAccess.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email>anand.jagtap@quantumsystems.co.uk</email>
// <date>02-09-2013</date>
// <summary>The common excel data access class for excel operations.
// </summary>

namespace HM.Framework.Application
{
    using System;
    using System.Data;
    using System.Data.OleDb;

    /// <summary>
    /// The common excel data access class for excel operations.
    /// </summary>
    public class ExcelDataAccess
    {
        #region Private Variables
        /// <summary>
        /// DbCommand object to wrapp the sql commands.
        /// </summary>
        private OleDbCommand commandWrapper;

        /// <summary>
        /// The ledb connection object.
        /// </summary>
        private OleDbConnection oledbConnection;
        #endregion

        /// <summary>
        /// Initializes a new instance of the ExcelDataAccess class.
        /// </summary>
        public ExcelDataAccess()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ExcelDataAccess class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public ExcelDataAccess(string connectionString)
        {
            this.oledbConnection = new OleDbConnection(connectionString);
            this.oledbConnection.Open();
        }

        /// <summary>
        /// Executes the command on the database
        /// </summary>
        /// <returns>The results as a DataReader object</returns>
        public IDataReader ExecuteReader()
        {
            return this.commandWrapper.ExecuteReader();
        }

        /// <summary>
        /// Executes the command on the database
        /// </summary>
        /// <returns>An integer value indicates success or failure.</returns>
        public int ExceuteNonQuery()
        {
            return this.commandWrapper.ExecuteNonQuery();
        }

        /// <summary>
        /// Executes the command on the database
        /// </summary>
        /// <returns>Object contains scalar value.</returns>
        public object ExecuteScalar()
        {
            return this.commandWrapper.ExecuteScalar();
        }

        /// <summary>
        /// Executes the command on the database and returns a dataset
        /// </summary>
        /// <returns>The dataset object.</returns>
        public object ExecuteDataSet()
        {
            OleDbDataAdapter oledbDataAdapter = new OleDbDataAdapter(this.commandWrapper.CommandText, this.oledbConnection);
            DataSet dataSet = new DataSet();
            oledbDataAdapter.Fill(dataSet);
            return dataSet;
        }

        /// <summary>
        /// Get the first sheet name of excel file.
        /// </summary>
        /// <returns>The first sheet name of excel file.</returns>
        public string GetSheetName()
        {
            DataTable dataTable = this.oledbConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            if (dataTable == null || dataTable.Rows.Count < 1)
            {
                throw new Exception("Error: Could not determine the name of the first worksheet.");
            }

            string firstSheetName = dataTable.Rows[0]["TABLE_NAME"].ToString();
            return firstSheetName.Trim();
        }

        /// <summary>
        /// Creates a command wrapper for the given query.
        /// </summary>
        /// <param name="query">The sql query string.</param>
        public void CreateOledbStringCommandWrapper(string query)
        {
            this.commandWrapper = new OleDbCommand(query, this.oledbConnection);

            // Since many long-running queries were giving a timeout, this was added. 
            int commandTimeout = Convert.ToInt32(ConfigManager.ReadAppSettings(Global.CONSTSTRCOMMANDTIMEOUT));
            this.commandWrapper.CommandTimeout = (commandTimeout == 0) ? 120 : commandTimeout;
        }

        /// <summary>
        /// Close the oledb connection.
        /// </summary>
        public void CloseConnection()
        {
            try
            {
                if (this.oledbConnection != null && this.oledbConnection.State == ConnectionState.Open)
                {
                    this.oledbConnection.Close();
                }
            }
            catch
            {
                this.oledbConnection.Dispose();
            }
        }
    }
}

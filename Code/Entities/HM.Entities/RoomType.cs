﻿// <copyright file="RoomType.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>22-07-2015</date>
// <summary> The RoomType entity class.
// </summary>

namespace HM.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The RoomType entity.
    /// </summary>
    [DataContract]
    [Serializable]
    public class RoomType : BaseEntity
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the RoomType class.
        /// </summary>
        public RoomType()
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        [DataMember]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the Type.
        /// </summary>
        [DataMember]
        public string Type { get; set; }
        #endregion
    }
}

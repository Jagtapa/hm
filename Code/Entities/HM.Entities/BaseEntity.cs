﻿// <copyright file="BaseEntity.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>22-07-2015</date>
// <summary> The BaseEntity entity class.
// </summary>

namespace HM.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The BaseEntity entity.
    /// </summary>
    [DataContract]
    [Serializable]
    public class BaseEntity
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the BaseEntity class.
        /// </summary>
        public BaseEntity()
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the created by user id.
        /// </summary>
        [DataMember]
        public long CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the created on date and time.
        /// </summary>
        [DataMember]
        public DateTime? CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the updated by user id.
        /// </summary>
        [DataMember]
        public long UpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets the updated on date and time.
        /// </summary>
        [DataMember]
        public DateTime? UpdatedOn { get; set; }

        /// <summary>
        /// Gets or sets the deleted status. 0-Not deleted 1-deleted
        /// </summary>
        [DataMember]
        public int Deleted { get; set; }
        #endregion
    }
}

﻿// <copyright file="HomeUser.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>22-07-2015</date>
// <summary> The HomeUser entity class.
// </summary>

namespace HM.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The HomeUser entity.
    /// </summary>
    [DataContract]
    [Serializable]
    public class HomeUser : BaseEntity
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the HomeUser class.
        /// </summary>
        public HomeUser()
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        [DataMember]
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the Home Id.
        /// </summary>
        [DataMember]
        public long HomeId { get; set; }

        /// <summary>
        /// Gets or sets the User Id.
        /// </summary>
        [DataMember]
        public long UserId { get; set; }

        /// <summary>
        /// Gets or sets the HouseName.
        /// </summary>
        [DataMember]
        public string HouseName { get; set; }
        #endregion
    }
}

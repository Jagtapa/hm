﻿// <copyright file="RoomItemType.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>22-07-2015</date>
// <summary> The RoomItemType entity class.
// </summary>

namespace HM.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The RoomItemType entity.
    /// </summary>
    [DataContract]
    [Serializable]
    public class RoomItemType : BaseEntity
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the RoomItemType class.
        /// </summary>
        public RoomItemType()
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        [DataMember]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the Item.
        /// </summary>
        [DataMember]
        public string Item { get; set; }
        #endregion
    }
}

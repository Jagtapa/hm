﻿// <copyright file="Home.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>21-07-2015</date>
// <summary> The Home entity class.
// </summary>

namespace HM.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The Home entity.
    /// </summary>
    [DataContract]
    [Serializable]
    public class Home : BaseEntity
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the Home class.
        /// </summary>
        public Home()
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        [DataMember]
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the User Id.
        /// </summary>
        [DataMember]
        public long UserId { get; set; }

        /// <summary>
        /// Gets or sets the HOUSENAME.
        /// </summary>
        [DataMember]
        public string HouseName { get; set; }

        /// <summary>
        /// Gets or sets the HOUSENO.
        /// </summary>
        [DataMember]
        public string HouseNo { get; set; }

        /// <summary>
        /// Gets or sets the STREET.
        /// </summary>
        [DataMember]
        public string Street { get; set; }

        /// <summary>
        /// Gets or sets the COUNTRY.
        /// </summary>
        [DataMember]
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the CITY.
        /// </summary>
        [DataMember]
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the TOWN.
        /// </summary>
        [DataMember]
        public string Town { get; set; }

        /// <summary>
        /// Gets or sets the LOCALITY.
        /// </summary>
        [DataMember]
        public string Locality { get; set; }

        /// <summary>
        /// Gets or sets the LOCALITY.
        /// </summary>
        [DataMember]
        public int PostCode { get; set; }
        #endregion
    }
}

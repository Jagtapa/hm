﻿// <copyright file="RoomDetails.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>22-07-2015</date>
// <summary> The RoomDetails entity class.
// </summary>

namespace HM.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The RoomDetails entity.
    /// </summary>
    [DataContract]
    [Serializable]
    public class RoomDetails : BaseEntity
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the RoomDetails class.
        /// </summary>
        public RoomDetails()
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        [DataMember]
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the Room Id.
        /// </summary>
        [DataMember]
        public long RoomId { get; set; }

        /// <summary>
        /// Gets or sets the room Item type Id.
        /// </summary>
        [DataMember]
        public int RoomItemTypeId { get; set; }

        /// <summary>
        /// Gets or sets the YearOfInstalled.
        /// </summary>
        [DataMember]
        public int YearOfInstalled { get; set; }

        /// <summary>
        /// Gets or sets the Quantity.
        /// </summary>
        [DataMember]
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the Brand.
        /// </summary>
        [DataMember]
        public string Brand { get; set; }

        /// <summary>
        /// Gets or sets the Type.
        /// </summary>
        [DataMember]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the Item.
        /// </summary>
        [DataMember]
        public string Item { get; set; }
        #endregion
    }
}

﻿// <copyright file="Room.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>22-07-2015</date>
// <summary> The Room entity class.
// </summary>

namespace HM.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The Room entity.
    /// </summary>
    [DataContract]
    [Serializable]
    public class Room : BaseEntity
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the Room class.
        /// </summary>
        public Room()
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        [DataMember]
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the Home Id.
        /// </summary>
        [DataMember]
        public long HomeId { get; set; }

        /// <summary>
        /// Gets or sets the room type Id.
        /// </summary>
        [DataMember]
        public int TypeId { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the Type.
        /// </summary>
        [DataMember]
        public string Type { get; set; }
        #endregion
    }
}

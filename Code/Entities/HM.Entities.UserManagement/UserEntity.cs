﻿// <copyright file="UserEntity.cs" company="Quantum Software Solutions Pvt Ltd">
// Copyright © 2013 All Right Reserved
// </copyright>
// <author>Anand Jagtap</author>
// <email></email>
// <date>14-05-2014</date>
// <summary>This class represents user entity of user details.
// </summary>

namespace HM.Entities.UserManagement
{
    using System;
    using System.Runtime.Serialization;
    using HM.Framework;

    /// <summary>
    /// The Entity class for User, implements IUserEntity.
    /// </summary>
    [DataContract]
    [Serializable]
    public class UserEntity : EntityBase
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the UserEntity class.
        /// </summary>
        public UserEntity()
        {
           
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets Unique Id for the row
        /// </summary>
        [DataMember]
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets First Name of the user.
        /// </summary>
        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Last Name of the user.
        /// </summary>
        [DataMember]
        public string LastName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Password for user login.
        /// </summary>
        [DataMember]
        public string Password
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets EmailId for user login.
        /// </summary>
        [DataMember]
        public string EmailId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether user is deleted.
        /// </summary>
        [DataMember]
        public bool Deleted { get; set; }

        /// <summary>
        /// Gets or sets default home Id for the user
        /// </summary>
        [DataMember]
        public long HomeId
        {
            get;
            set;
        }

        #endregion
    }
}
